package com.marico.fsp.activity;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Calendar;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.method.KeyListener;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.marico.fsp.R;
import com.marico.fsp.helper.FSPEntryConfirmHelper;

public class FSPConfirmActivity extends Activity{
	private String harvested,previousmonth,previousnuts,nutstock,nextmonth,expectednuts,outlook,round,fertilizer,secondLastP;
	private	String avgmatureNuts,avgmatureBunches,avgbunchesCount,avgbuttonNuts;
	private Button buttonSaveConfirm,buttonBackConfirm;
	private int mYear;    
	private int mMonth;  
	private int mDay;
	private String lastPlucking,nextPlucking,numOfNuts,expNuts;
	private TextView textViewFarmerNameFSPEntryConfirm;
	DatePickerDialog dpd =null ;
	private FSPEntryConfirmHelper fspEntryConfirmHelper;
	String farmerId;
	String farmerName;
	ArrayAdapter<String> adapteroutlook,adapterFertilizer ;
	private ButtonClick buttonClick;
	int tempId;
	String harv;
	private ToggleButton toggleButtonCharvested;
	private TextView textViewCpreviousmonth,textViewCnextmonth,textViewCsecondLP;
	private EditText editTextCRound;
	private EditText editTextCpreviousnuts,editTextCnutstock,editTextCexpectednuts;
	private Spinner spinneroutlook,spinnerfertilizer;
	private int flag;
	private String[] strLastHarvestedDetails;
	private KeyListener listener;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lyt_entry_confirm);
		buttonBackConfirm = (Button) findViewById(R.id.buttonBackConfirm);
		buttonSaveConfirm = (Button) findViewById(R.id.buttonSaveConfirm);
		savedInstanceState = getIntent().getExtras();
		flag = savedInstanceState.getInt("Flag");
		textViewFarmerNameFSPEntryConfirm = (TextView) findViewById(R.id.textViewFarmerNameFSPEntryConfirm);
		savedInstanceState = getIntent().getExtras();
		farmerId = savedInstanceState.getString("FarmerId");
		farmerName = savedInstanceState.getString("FarmerName");
		textViewFarmerNameFSPEntryConfirm.setText(farmerName);
		fspEntryConfirmHelper = new FSPEntryConfirmHelper(FSPConfirmActivity.this);
		String [] temp =fspEntryConfirmHelper.getEntryAvgDate(farmerId);


		avgmatureNuts= temp[0];
		avgmatureBunches= temp[1];
		avgbunchesCount= temp[2];
		avgbuttonNuts= temp[3];


		toggleButtonCharvested = (ToggleButton) findViewById(R.id.toggleButtonCharvested);
		textViewCpreviousmonth = (TextView) findViewById(R.id.textViewCpreviousmonth);
		textViewCnextmonth = (TextView) findViewById(R.id.textViewCnextmonth);
		textViewCsecondLP = (TextView) findViewById(R.id.textViewCsecondLP);
		spinneroutlook = (Spinner) findViewById(R.id.spinneroutlook);
		spinnerfertilizer = (Spinner) findViewById(R.id.spinnerfertilizer);
		editTextCpreviousnuts = (EditText) findViewById(R.id.editTextCpreviousnuts);
		editTextCnutstock = (EditText) findViewById(R.id.editTextCnutstock);
		editTextCexpectednuts = (EditText) findViewById(R.id.editTextCexpectednuts);
		editTextCRound = (EditText) findViewById(R.id.editTextCRound);

		strLastHarvestedDetails = fspEntryConfirmHelper.getLastHarvestedData(farmerId);

		lastPlucking = strLastHarvestedDetails[0];
		nextPlucking = strLastHarvestedDetails[1];
		numOfNuts = strLastHarvestedDetails[2];
		expNuts = strLastHarvestedDetails[3];
		harv = toggleButtonCharvested.getText().toString().trim();



		if(harv.equalsIgnoreCase("No"))
		{
			textViewCpreviousmonth.setEnabled(false);
			textViewCnextmonth.setEnabled(false);
			editTextCpreviousnuts.setEnabled(false);
			editTextCexpectednuts.setEnabled(false);

			listener = editTextCpreviousnuts.getKeyListener();
			editTextCpreviousnuts.setKeyListener(null);
			editTextCexpectednuts.setKeyListener(null);

			if(lastPlucking.equals("/"))
			{
				textViewCpreviousmonth.setText("NA");
				textViewCnextmonth.setText("NA");
				editTextCpreviousnuts.setText("0");
				editTextCexpectednuts.setText("0");
			}
			else
			{
				textViewCpreviousmonth.setText(lastPlucking);
				textViewCnextmonth.setText(nextPlucking);
				editTextCpreviousnuts.setText(numOfNuts);
				editTextCexpectednuts.setText(expNuts);
			}			
		}
		ArrayList<String> output = new ArrayList<String>();
		output.add("Good");
		output.add("Ok");
		output.add("Bad");


		ArrayList<String> fertilizer = new ArrayList<String>();
		fertilizer.add("Organic");
		fertilizer.add("Inorganic");
		fertilizer.add("Nil");


		buttonClick = new ButtonClick(); 
		buttonBackConfirm.setOnClickListener(buttonClick);
		buttonSaveConfirm.setOnClickListener(buttonClick);

		adapteroutlook = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, output);
		adapteroutlook.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinneroutlook.setAdapter(adapteroutlook);

		adapterFertilizer = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, fertilizer);
		adapterFertilizer.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerfertilizer.setAdapter(adapterFertilizer);

		final Calendar c = Calendar.getInstance();        
		mYear = c.get(Calendar.YEAR);        
		mMonth = c.get(Calendar.MONTH);        
		mDay = c.get(Calendar.DAY_OF_MONTH);

		toggleButtonCharvested.setOnClickListener(buttonClick);

		textViewCpreviousmonth.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				tempId=1;
				createDialogWithoutDateField().show();
			}
		});

		textViewCnextmonth.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				tempId=2;
				createDialogWithoutDateField().show();
			}
		});


		textViewCsecondLP.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				tempId=3;
				createDialogWithoutDateField().show();
			}
		});

		if(flag==2)
		{
			String [] temp2 =fspEntryConfirmHelper.getdataFromAvg_data(farmerId);

			toggleButtonCharvested.setText(temp2[0]);
			textViewCpreviousmonth.setText(temp2[1]);
			editTextCpreviousnuts.setText(temp2[2]);
			editTextCnutstock.setText(temp2[3]);
			textViewCnextmonth.setText(temp2[4]);
			editTextCexpectednuts.setText(temp2[5]);

			int i =output.indexOf(temp2[6]);
			spinneroutlook.setSelection(i);
			editTextCRound.setText(temp2[7]);
			
			
			textViewCsecondLP.setText(temp2[8]);
			int i2 =fertilizer.indexOf(temp2[9]);
			spinnerfertilizer.setSelection(i2);
		}				
	}
	@Override
	public void onBackPressed() {
	}	
	private class ButtonClick implements OnClickListener
	{

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.buttonBackConfirm:

				finish();
				break;
			case R.id.toggleButtonCharvested:

				harv=toggleButtonCharvested.getText().toString();
				//Toast.makeText(getApplicationContext(), "Harvested  "+harv, Toast.LENGTH_SHORT).show();

				if(harv.equalsIgnoreCase("No"))
				{
					textViewCpreviousmonth.setEnabled(false);
					textViewCnextmonth.setEnabled(false);
					editTextCpreviousnuts.setEnabled(false);
					editTextCexpectednuts.setEnabled(false);

					listener = editTextCpreviousnuts.getKeyListener();
					editTextCpreviousnuts.setKeyListener(null);
					editTextCexpectednuts.setKeyListener(null);



					if(lastPlucking.equals("/"))
					{
						textViewCpreviousmonth.setText("NA");
						textViewCnextmonth.setText("NA");
						editTextCpreviousnuts.setText("0");
						editTextCexpectednuts.setText("0");
					}
					else
					{
						textViewCpreviousmonth.setText(lastPlucking);
						textViewCnextmonth.setText(nextPlucking);
						editTextCpreviousnuts.setText(numOfNuts);
						editTextCexpectednuts.setText(expNuts);
					}					
				}
				else if(harv.equalsIgnoreCase("Yes"))
				{
					textViewCpreviousmonth.setEnabled(true);
					textViewCnextmonth.setEnabled(true);
					editTextCpreviousnuts.setEnabled(true);
					editTextCexpectednuts.setEnabled(true);

					editTextCpreviousnuts.setKeyListener(listener);
					editTextCexpectednuts.setKeyListener(listener);

					textViewCpreviousmonth.setText("");
					textViewCnextmonth.setText("");
					editTextCpreviousnuts.setText("");
					editTextCexpectednuts.setText("");
				}
				break;
			case R.id.buttonSaveConfirm:

				harvested=toggleButtonCharvested.getText().toString();
				previousmonth=textViewCpreviousmonth.getText().toString();				
				previousnuts=editTextCpreviousnuts.getText().toString();
				nutstock=editTextCnutstock.getText().toString();
				nextmonth=textViewCnextmonth.getText().toString();
				expectednuts=editTextCexpectednuts.getText().toString();
				round=editTextCRound.getText().toString();
				outlook=spinneroutlook.getSelectedItem().toString();
				fertilizer=spinnerfertilizer.getSelectedItem().toString();
				secondLastP=textViewCsecondLP.getText().toString();
				if(harvested.equalsIgnoreCase("No") )
				{
					if(previousmonth.equals("/")|| previousmonth.equals(null))
					{
						Log.i("Log", "Previous month is: "+previousmonth);
						previousmonth = "NA";
						nextmonth = "NA";
						previousnuts = "0";
						expectednuts = "0";
					}															
				}
				if(harvested.equalsIgnoreCase("")
						||previousmonth.equalsIgnoreCase("")
						||previousnuts.equalsIgnoreCase("")
						||nutstock.equalsIgnoreCase("")
						||nextmonth.equalsIgnoreCase("")
						||expectednuts.equalsIgnoreCase("")
						||outlook.equalsIgnoreCase("")
						|| round.equalsIgnoreCase("")
						|| fertilizer.equalsIgnoreCase("")
						|| secondLastP.equalsIgnoreCase("")
						)
				{
					Toast.makeText(getApplicationContext(), "Pls Enter All Data", Toast.LENGTH_LONG).show();
					break;	
				}
				Log.i("log", "avgmatureNuts"+avgmatureNuts);
				Log.i("log", "avgmatureBunches"+avgmatureBunches);
				Log.i("log", "avgbunchesCount"+avgbunchesCount);
				Log.i("log", "avgbuttonNuts"+avgbuttonNuts);
				Log.i("log", "harvested"+harvested);
				Log.i("log", "previousmonth"+previousmonth);
				Log.i("log", "previousnuts"+previousnuts);
				Log.i("log", "nutstock"+nutstock);
				Log.i("log", "nextmonth"+nextmonth);
				Log.i("log", "expectednuts"+expectednuts);
				Log.i("log", "outlook"+outlook);
				Log.i("log", "round"+round);


				AlertDialog.Builder alertbox = new AlertDialog.Builder(FSPConfirmActivity.this);
				alertbox.setMessage(" Are you sure want to save? ");
				alertbox.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface arg0, int arg1) {					
						new InsertIntoConfirm().execute("");

					}
				});
				alertbox.setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface arg0, int arg1) {
					}
				});	
				alertbox.show();							
				break;
			default:
				break;
			}
		}		
	}
	class InsertIntoConfirm extends AsyncTask<String, String, String>
	{
		ProgressDialog progressDialog ;
		TextView textViewmsg;
		@Override
		protected void onPreExecute() {
			progressDialog= new ProgressDialog(FSPConfirmActivity.this);
			progressDialog.setTitle("Please Wait..");
			progressDialog.setMessage("Saving");
			progressDialog.setCancelable(false);
			progressDialog.show();
			super.onPreExecute();
		}
		@Override
		protected String doInBackground(String... params) {

			Log.i("Log", "For Location :  latti : "+FSPEntryActivity.lattitude+" longi "+FSPEntryActivity.longitude+" Accuracy "+FSPEntryActivity.accuracy+" Provider "+FSPEntryActivity.provider);

			fspEntryConfirmHelper.saveToAvgTable(farmerId,avgmatureBunches,avgmatureNuts,avgbunchesCount,avgbuttonNuts,
					harvested,previousmonth,previousnuts,nutstock,nextmonth,expectednuts,outlook,round,fertilizer,secondLastP);
			fspEntryConfirmHelper.saveToFarmData(farmerId);
			fspEntryConfirmHelper.saveLocation(farmerId, FSPEntryActivity.lattitude, FSPEntryActivity.longitude, FSPEntryActivity.accuracy, FSPEntryActivity.provider);
			fspEntryConfirmHelper.delete_FarmDataTemp();
			return null;
		}
		@Override
		protected void onPostExecute(String result) {
			if(progressDialog.isShowing())
			{
				progressDialog.dismiss();
			}			

			super.onPostExecute(result);
			Intent intent = new Intent(FSPConfirmActivity.this, FarmerSelectionActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
		}
	}

	public DatePickerDialog.OnDateSetListener  mDateSetListener =            
			new DatePickerDialog.OnDateSetListener() {                
		public void onDateSet(DatePicker view, int year,int monthOfYear, int dayOfMonth) {                    
			mYear = year;                    
			mMonth = monthOfYear;                    

			String date = (mMonth + 1)+"/"+mYear; 

			if(tempId==1)
			{
				textViewCpreviousmonth.setText(date);	
			}
			if(tempId==2)
			{
				textViewCnextmonth.setText(date);	
			}
			if(tempId==3)
			{
				textViewCsecondLP.setText(date);	
			}



		}            
	};



	private DatePickerDialog createDialogWithoutDateField(){

		if(dpd!=null)
		{
			return dpd;
		}
		dpd = new DatePickerDialog(FSPConfirmActivity.this,mDateSetListener,mYear, mMonth,mDay );
		dpd.setTitle("");
		try{
			Field[] datePickerDialogFields = dpd.getClass().getDeclaredFields();
			for (Field datePickerDialogField : datePickerDialogFields) { 
				if (datePickerDialogField.getName().equals("mDatePicker")) {
					datePickerDialogField.setAccessible(true);
					DatePicker datePicker = (DatePicker) datePickerDialogField.get(dpd);
					Field datePickerFields[] = datePickerDialogField.getType().getDeclaredFields();
					for (Field datePickerField : datePickerFields) {
						if ("mDayPicker".equals(datePickerField.getName())) {
							datePickerField.setAccessible(true);
							Object dayPicker = new Object();
							dayPicker = datePickerField.get(datePicker);
							((View) dayPicker).setVisibility(View.GONE);
						}
					}
				}
			}
		}catch(Exception ex){
		}
		return dpd;
	}

}