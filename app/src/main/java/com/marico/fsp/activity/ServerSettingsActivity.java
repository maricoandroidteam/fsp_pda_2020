package com.marico.fsp.activity;



import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.marico.fsp.R;
import com.marico.fsp.helper.ServerSettingHelper;

public class ServerSettingsActivity extends Activity {
	private Button buttonSSSave=null,buttonSSCancel=null; 

	private ServerSettingHelper mercHelper;


	private EditText editTextSSUrl = null;
	protected void onCreate(Bundle savedInstanceState) {

		mercHelper = new ServerSettingHelper(ServerSettingsActivity.this);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lyt_serversettings_main);

		editTextSSUrl = (EditText) findViewById(R.id.editTextSSUrl);
		buttonSSSave = (Button) findViewById(R.id.buttonSSSave);
		buttonSSCancel	= (Button) findViewById(R.id.buttonSSCancel);

		MyOnClickListener myOnClickListener = new MyOnClickListener();
		buttonSSSave.setOnClickListener(myOnClickListener);
		buttonSSCancel.setOnClickListener(myOnClickListener);
	}//onCreate Ends


	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
				
		editTextSSUrl.setText(mercHelper.getUrl());

	}//onStart() ends


	private class MyOnClickListener implements View.OnClickListener{

		@Override
		public void onClick(View v) {
			int i=v.getId();
			switch (i) {
			case R.id.buttonSSCancel:{
				finish();
			}
			break;
			case R.id.buttonSSSave:{
				alertBoxSettings();
			}
			break;
			default:
				break;
			}
		}

	}//MyOnClickListener ends

	private void alertBoxSettings(){

		AlertDialog.Builder builder = new AlertDialog.Builder(ServerSettingsActivity.this);
		builder.setCancelable(true);
		builder.setMessage("Do You Really Want to Change Url??");

		builder.setPositiveButton("Yes", new DialogInterface.OnClickListener(){

			@Override
			public void onClick(DialogInterface dialog, int id) {

				String url = editTextSSUrl.getText().toString().trim();

				mercHelper.setUrl(url);
				

				Toast.makeText(ServerSettingsActivity.this, "URL Changed ", Toast.LENGTH_LONG).show();
				finish(); 

			}
		});

		builder.setNegativeButton("No", new DialogInterface.OnClickListener(){

			@Override
			public void onClick(DialogInterface dialog, int id) {

			}
		});

		builder.show();
	}//alertBoxSettings ends

	//creating the Back button
	/*@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.back_button, menu);
		return true;
	}

	//putting the Onclick Event on the Menu

	@Override
	public boolean onOptionsItemSelected(MenuItem menuItem)
	{
		int menuItemId = menuItem.getItemId();

		if(menuItemId == R.id.backbutton)
		{
			finish();
		}
		return false;

	}*/

	//Back Press Down
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub

	}
}//Activity Ends
