package com.marico.fsp.activity;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.SQLException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.marico.fsp.R;
import com.marico.fsp.helper.DownloadHelper;
import com.marico.fsp.helper.RouteDownloadHelper;
import com.marico.fsp.util.GetJSONArray;


public class RouteDownloadActivity extends Activity
{

	private ListView listViewRouteDownload;
	private Button buttonRDBack,buttonRDNext;

	private String[] beatName, beatId;
	private RouteDownloadHelper routeDownloadHelper;
	private ArrayAdapter<String> adapter;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lyt_route_download);
		buttonIdSetter();
		new BeatDownloadAsyncTask().execute("");
		//adapter = new ArrayAdapter<String>(RouteDownloadActivity.this, android.R.layout.simple_list_item_multiple_choice, beatName);
	}

	void buttonIdSetter()
	{
		ButtonClickListener buttonClickListener = new ButtonClickListener();

		routeDownloadHelper = new RouteDownloadHelper(RouteDownloadActivity.this);
		listViewRouteDownload = (ListView) findViewById(R.id.listViewRouteDownload);
		buttonRDBack = (Button) findViewById(R.id.buttonRDBack);
		buttonRDNext = (Button) findViewById(R.id.buttonRDNext);
		buttonRDBack.setOnClickListener(buttonClickListener);
		buttonRDNext.setOnClickListener(buttonClickListener);
	}

	@Override
	protected void onStart() 
	{
		super.onStart();
	}
	
	@Override
	public void onBackPressed() {	}


	class ButtonClickListener implements OnClickListener
	{

		@Override
		public void onClick(View v)
		{
			switch (v.getId()) {
			case R.id.buttonRDBack:
				finish();
				break;

			case R.id.buttonRDNext:
				String route  = getRouteItems();
				if(route.length()>0)
				{
					Intent i = new Intent(RouteDownloadActivity.this, DownloadActivity.class);

					i.putExtra("route", route);
					startActivity(i);
				}
				else
				{
					Toast.makeText(getApplicationContext(), "Select Route", Toast.LENGTH_SHORT).show();
				}
				break;
			}
		}
	}


	class BeatDownloadAsyncTask extends AsyncTask<String, String, String>
	{
		ProgressDialog progressDialog ;
		TextView textViewmsg;
		DownloadHelper downloadHelper= new DownloadHelper(RouteDownloadActivity.this);

		String downloadURL_MstRoute="";
		@Override
		protected void onPreExecute() {
			String userId = downloadHelper.getUserId();
			String URL=downloadHelper.getUrl();
			downloadURL_MstRoute=URL+"/Download.aspx?TableName=MstRoute&userid="+userId;
			Log.v("log","downloadURL_MstRoute"+downloadURL_MstRoute);

			progressDialog= new ProgressDialog(RouteDownloadActivity.this);
			progressDialog.setTitle("Please Wait..");
			progressDialog.setMessage("Loading");
			progressDialog.setCancelable(false);
			progressDialog.show();
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... params) 
		{
			try
			{
				downloadHelper.deleteALLtableForDownload();
			}
			catch (SQLException e) {
				return "SQLException";
			}
			catch (Exception e) {
				return "Exception";
			}
			downloadHelper.inserIntoMstRouteFromDownload(GetJSONArray.getfromURL(downloadURL_MstRoute));
			return null;
		}

		@Override
		protected void onPostExecute(String result)
		{
			super.onPostExecute(result);
			progressDialog.dismiss();
			Object obj[]=routeDownloadHelper.routeInfo();
			beatId = (String[]) obj[0];
			beatName = (String[]) obj[1];
			adapter = new ArrayAdapter<String>(RouteDownloadActivity.this, android.R.layout.simple_list_item_single_choice, beatName);					
			listViewRouteDownload.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
			listViewRouteDownload.setItemsCanFocus(false);
			listViewRouteDownload.setAdapter(adapter);
			//adapter.notifyDataSetChanged();
		}
	}

	private String getRouteItems() 
	{
		String savedItems = "";
		int count = listViewRouteDownload.getAdapter().getCount();
		for (int i = 0; i < count; i++) {
			if (listViewRouteDownload.isItemChecked(i)) {

				/*if (savedItems.length() > 0) {
					savedItems += "," + listViewRouteDownload.getItemAtPosition(i);
				} else {
					savedItems += listViewRouteDownload.getItemAtPosition(i);
				}*/
				savedItems = (String) listViewRouteDownload.getItemAtPosition(i);
			}
		}
		return savedItems;
	}
}