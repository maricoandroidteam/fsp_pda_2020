package com.marico.fsp.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.marico.fsp.R;
import com.marico.fsp.helper.UserReportInfoHelper;


public class UserReportInfoAtivity extends Activity{

	private Button buttonInfoDetails,buttonInfoBack;
	private UserReportInfoHelper helper;
	private TextView textViewInfoName;
	private TextView textViewInfoharvested,textViewInfoRound,textViewInfopreviousmonth,textViewInfoPreviousnuts;
	private TextView textViewInfoNextmonth,textViewInfoExpectednuts,textViewInfoNutstock,textViewInfoOutlook;

	private TextView textViewInfofertilizer,textViewInfosecondLastP;
	DatePickerDialog dpd =null ;

	String farmerId;
	String farmerName;
	ArrayAdapter<String> aa ;
	private ButtonClick buttonClick;
	int tempId;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lyt_user_report_info);

		farmerId = getIntent().getStringExtra("farmid");
		farmerName = getIntent().getStringExtra("farmname");

		textViewInfoName = (TextView) findViewById(R.id.textViewInfoName);

		textViewInfoharvested = (TextView) findViewById(R.id.textViewInfoharvested);
		textViewInfoRound = (TextView) findViewById(R.id.textViewInfoRound);
		textViewInfopreviousmonth = (TextView) findViewById(R.id.textViewInfopreviousmonth);
		textViewInfoPreviousnuts = (TextView) findViewById(R.id.textViewInfoPreviousnuts);
		textViewInfoNextmonth = (TextView) findViewById(R.id.textViewInfoNextmonth);
		textViewInfoExpectednuts = (TextView) findViewById(R.id.textViewInfoExpectednuts);
		textViewInfoNutstock = (TextView) findViewById(R.id.textViewInfoNutstock);
		textViewInfoOutlook = (TextView) findViewById(R.id.textViewInfoOutlook);

		textViewInfofertilizer = (TextView) findViewById(R.id.textViewInfofertilizer);
		textViewInfosecondLastP = (TextView) findViewById(R.id.textViewInfosecondLastP);

		buttonInfoDetails = (Button) findViewById(R.id.buttonInfoDetails);
		buttonInfoBack = (Button) findViewById(R.id.buttonInfoBack);

		helper = new UserReportInfoHelper(UserReportInfoAtivity.this);

		buttonClick = new ButtonClick();
		buttonInfoDetails.setOnClickListener(buttonClick);
		buttonInfoBack.setOnClickListener(buttonClick);

		textViewInfoName.setText(farmerName);



		String [] temp2 =helper.getdataFromAvg_data(farmerId);

		textViewInfoharvested.setText(temp2[0]);
		textViewInfopreviousmonth.setText(temp2[1]);
		textViewInfoPreviousnuts.setText(temp2[2]);
		textViewInfoNutstock.setText(temp2[3]);
		textViewInfoNextmonth.setText(temp2[4]);
		textViewInfoExpectednuts.setText(temp2[5]);

		textViewInfoOutlook.setText(temp2[6]);
		textViewInfoRound.setText(temp2[7]);
		
		textViewInfosecondLastP.setText(temp2[8]);
		textViewInfofertilizer.setText(temp2[9]);
	}


	@Override
	public void onBackPressed() {


	}
	private class ButtonClick implements OnClickListener
	{

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.buttonInfoBack:

				finish();
				break;
			case R.id.buttonInfoDetails:
				Intent i = new Intent(UserReportInfoAtivity.this, UserReportDetailActivity.class);
				i.putExtra("farmid", farmerId);
				i.putExtra("farmname", farmerName);
				startActivity(i);

				break;
			default:
				break;
			}
		}		
	}





}