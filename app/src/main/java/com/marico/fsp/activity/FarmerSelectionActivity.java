package com.marico.fsp.activity;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.marico.fsp.R;
import com.marico.fsp.helper.FarmerSelectionHelper;


public class FarmerSelectionActivity extends Activity
{
	private Button buttonFSBack;
	private ListView listViewFarmerlist;
	private Spinner spinnerRoute;
	private ArrayList<String> routeid;
	private ArrayList<String> routeName;
	private SpinnerSelected spinnerSelected;
	private MySpinnerAdapter mySpinnerAdapter;
	private FarmerSelectionHelper farmerSelectionHelper;
	private ArrayList<String> arrayListFarmerId;
	private ArrayList<String> arrayListFarmerName;
	private ArrayList<String> arrayListColorCode;
	private BeatSelBaseAdapter beatSelBaseAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lyt_farmer_selection);

		mySpinnerAdapter = new MySpinnerAdapter();
		spinnerSelected = new SpinnerSelected();
		farmerSelectionHelper = new FarmerSelectionHelper(FarmerSelectionActivity.this);
		routeid = new ArrayList<String>();
		routeName = new ArrayList<String>();
		arrayListFarmerName = new ArrayList<String>();
		arrayListColorCode = new ArrayList<String>();
		arrayListFarmerId = new ArrayList<String>();
		beatSelBaseAdapter = new BeatSelBaseAdapter();
	}


	@SuppressWarnings("unchecked")
	@Override
	protected void onStart() {
		super.onStart();
		buttonIdSeter();
		Object[] object = farmerSelectionHelper.getRouteInfo();
		routeid = (ArrayList<String>) object[0]; 
		routeName = (ArrayList<String>) object[0];

		spinnerRoute.setAdapter(mySpinnerAdapter);
		mySpinnerAdapter.notifyDataSetChanged();
		spinnerRoute.setOnItemSelectedListener(spinnerSelected);

		listViewFarmerlist.setAdapter(beatSelBaseAdapter);
		beatSelBaseAdapter.notifyDataSetChanged();
		listViewFarmerlist.setOnItemClickListener(new ListViewClickListener());
	}

	@Override
	public void onBackPressed() {}
	void buttonIdSeter()
	{

		ButtonOnClickListener buttonOnClickListener= new ButtonOnClickListener();

		buttonFSBack = (Button) findViewById(R.id.buttonFSBack);

		spinnerRoute = (Spinner) findViewById(R.id.spinnerRoute);

		listViewFarmerlist = (ListView) findViewById(R.id.listViewFarmerlist);
		buttonFSBack.setOnClickListener(buttonOnClickListener);


	}

	private class MySpinnerAdapter extends BaseAdapter
	{
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return routeid.size();
		}
		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}
		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}
		@Override
		public View getView(int position, View view, ViewGroup parent) {
			// TODO Auto-generated method stub
			final ViewHolderForSpinner viewHolder; 
			View rowView=view;
			if(rowView == null)
			{
				LayoutInflater layoutInflater = LayoutInflater.from(FarmerSelectionActivity.this);
				rowView = layoutInflater.inflate(R.layout.lyt_farmer_selection_spinner,parent,false);
				
				viewHolder = new ViewHolderForSpinner();
				viewHolder.textViewFSSRouteName = (TextView)rowView.findViewById(R.id.textViewFSSRouteName);

				rowView.setTag(viewHolder);	
			}
			else
			{
				viewHolder = (ViewHolderForSpinner) rowView.getTag();
			}
			//	viewHolder.textViewtextViewBS_Spinner.setTypeface(typeface);
			viewHolder.textViewFSSRouteName.setText(routeid.get(position));
			//viewHolder.icon.setImageResource(R.drawable.ic_launcher);
			return rowView;
		}
	}


	private class ViewHolderForSpinner
	{
		TextView textViewFSSRouteName = null;
	}


	private class SpinnerSelected implements OnItemSelectedListener
	{
		@SuppressWarnings("unchecked")
		@Override
		public void onItemSelected(AdapterView<?> parent, View view, int position, long id) 
		{
			//String dsrN  = routeid.get(position);
			String dstI = routeName.get(position);
			//	Log.i("Log", "Spinner click at:"+brandName.get(position));
			Object[] object = farmerSelectionHelper.getFarmerNameFromMstFarm(dstI);
			arrayListFarmerName = (ArrayList<String>) object[1];
			arrayListFarmerId = (ArrayList<String>) object[0];
			arrayListColorCode = (ArrayList<String>) object[2];
			beatSelBaseAdapter.notifyDataSetChanged();


		}
		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub

		}

	}

	class BeatSelBaseAdapter extends BaseAdapter
	{
		View row ;
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return arrayListFarmerName.size();
		}
		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return arrayListFarmerName.get(arg0);
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return arg0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup arg2) {

			final ViewHolder viewHolder;
			row = convertView;
			if (row == null)
			{
				viewHolder = new ViewHolder();
				LayoutInflater vi = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				row = vi.inflate(R.layout.lyt_farmer_selection_listview,null);
				viewHolder.textViewFSLFarmerName = (TextView)row.findViewById(R.id.textViewFSLFarmerName);
				viewHolder.textViewFSLFarmerCode = (TextView)row.findViewById(R.id.textViewFSLFarmerCode);


				row.setTag(viewHolder);
			}
			else
			{
				viewHolder = (ViewHolder)row.getTag();
			}

			viewHolder.textViewFSLFarmerName.setText(arrayListFarmerName.get(position));
			viewHolder.textViewFSLFarmerCode.setText(arrayListFarmerId.get(position));

			if(arrayListColorCode.get(position).equalsIgnoreCase("1"))
			{
				row.setBackgroundColor(Color.WHITE);
			}
			else if(arrayListColorCode.get(position).equalsIgnoreCase("2"))
			{
				row.setBackgroundColor(Color.GREEN);
			}
			else if(arrayListColorCode.get(position).equalsIgnoreCase("3"))
			{
				row.setBackgroundColor(Color.GRAY);
			}
			return row;
		}
	}

	class ListViewClickListener implements OnItemClickListener
	{
		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {

			if(arrayListColorCode.get(arg2).equalsIgnoreCase("3"))
			{
				Toast.makeText(getApplicationContext(), "Data is Uploaded Can't Edited", Toast.LENGTH_LONG).show();
				return ;
			}

			String farmerId=arrayListFarmerId.get(arg2);
			Log.i("log", "farmerId is "+farmerId);
			Intent intent = new Intent(FarmerSelectionActivity.this, FarmerInfoActivity.class);
			intent.putExtra("FarmerId", farmerId);
			intent.putExtra("FarmerName", arrayListFarmerName.get(arg2));
			startActivity(intent);

		}
	}


	class ButtonOnClickListener implements OnClickListener
	{
		@Override
		public void onClick(View v)
		{
			switch (v.getId())
			{
			case R.id.buttonFSBack:
				finish();
				break;

			}
		}
	}
	private class ViewHolder{

		TextView textViewFSLFarmerName = null;
		TextView textViewFSLFarmerCode = null;



	}
}
