package com.marico.fsp.activity;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.ParseException;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.marico.fsp.R;
import com.marico.fsp.helper.DatabaseHelper;
import com.marico.fsp.helper.ServerSettingHelper;

@SuppressLint("SimpleDateFormat")
public class SettingsActivity extends Activity {
	private String dbname = "FSP.db";

	private Button btnBackUp=null,btnServerSettings=null,btnBack=null,btnDBCleanup=null,buttonBack=null; 
	ProgressDialog progDialog = null;
	private ProgressDialog progDiag; 

	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.lyt_settings_main);

		btnBackUp = (Button)findViewById(R.id.btnBackUp);
		btnServerSettings = (Button)findViewById(R.id.btnServerSettings);
		btnBack = (Button)findViewById(R.id.btnBack);
		btnDBCleanup = (Button)findViewById(R.id.btnDBCleanup);
		buttonBack = (Button)findViewById(R.id.buttonBack);//this button is used for checking updates

		MyOnClickListener myOnClickListener = new MyOnClickListener();
		btnBackUp.setOnClickListener(myOnClickListener);
		btnServerSettings.setOnClickListener(myOnClickListener);
		btnBack.setOnClickListener(myOnClickListener);
		//btnDBCleanup.setOnClickListener(myOnClickListener);
		buttonBack.setOnClickListener(myOnClickListener);
	}

	private class MyOnClickListener implements View.OnClickListener{

		@Override
		public void onClick(View v) {
			int i=v.getId();
			switch(i){
			case R.id.btnBackUp:
			{
				AlertDialog.Builder myDialog = new AlertDialog.Builder(SettingsActivity.this);
				myDialog.setTitle("Back Up?");
				myDialog.setMessage("Are You Sure You want to take Back up Database on SD card: "+dbname);
				myDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {

						//RestoreDatabase(dbname);
						
						BackUpDatabase();
					}
				});
				myDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface arg0, int arg1) {
					}
				});
				myDialog.show();
			}
			break;

			case R.id.btnServerSettings:
			{
				Intent intent = new Intent(SettingsActivity.this, ServerSettingsActivity.class);
				startActivity(intent);
			}
			break;

			case R.id.btnDBCleanup:
			{
				AlertDialog.Builder myDialog = new AlertDialog.Builder(SettingsActivity.this);
				myDialog.setTitle("DB Cleanup?");
				myDialog.setMessage("Are You Sure You want Clean Database ? ");
				myDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						progDialog = new ProgressDialog(SettingsActivity.this);
						progDialog.setTitle("DB Deleting");
						progDialog.setMessage("Please Wait...");
						progDialog.show();

						/*MerchandisingHelper mercHelper = new MerchandisingHelper(Settings.this);
						mercHelper.dropAllTables();
						mercHelper.close();*/
						File sd = Environment.getExternalStorageDirectory(); 
						File currentDB = new File(sd, dbname); 
						boolean deleted = currentDB.delete();
						if(progDialog.isShowing()){
							progDialog.dismiss();
						}
						//	if(deleted) MerchandisingProperty.isDatainDb = false;
						Toast.makeText(SettingsActivity.this, "DB Cleaned Successfully ", Toast.LENGTH_LONG).show();
						Log.i("Log","Settings:dbclenup:deleted: "+ deleted);
						//Intent intent = new Intent(Settings.this, Login.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						Intent intent = new Intent(SettingsActivity.this, LoginActivity.class);
						intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(intent);
					}
				});
				myDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface arg0, int arg1) {
					}
				});
				myDialog.show();
			}
			break;

			case R.id.btnBack://though this buttons name is back it is used for Checking Updates
			{
				//finish();
				if(isInternetOn() == false)
				{
					Toast.makeText(SettingsActivity.this, "Please Check the Internet Connection", Toast.LENGTH_LONG).show();
				}else{

					progDiag = ProgressDialog.show(SettingsActivity.this, "", "Checking Updates Please Wait...");
					progDiag.setCancelable(false);
					Thread updateThread = new Thread(){
						public void run(){
							Message msg = Message.obtain();

							ServerSettingHelper gtDatabaseHelper = new ServerSettingHelper(SettingsActivity.this);
							gtDatabaseHelper.deleteMstVersionControl();

							String URL=gtDatabaseHelper.getUrl();

							String result = null;
							InputStream is = null;
							StringBuilder stringBuilder = null;
							int jArrayLength;
							String table = "MstVersionControl";
							String criteria = "";

							try {

								String url = URL+"/download.aspx?TableName="+table+criteria;
								HttpClient httpClient = new DefaultHttpClient();

								HttpPost httpPost = new HttpPost(url);

								HttpResponse response = httpClient.execute(httpPost);
								HttpEntity entity = response.getEntity();

								StatusLine statusLine = response.getStatusLine();
								int statuscode = statusLine.getStatusCode();

								if (statuscode == 200) {
									Log.i("Log", "Response Is got from the Server for "+ table);
								}

								is = entity.getContent();
							} catch (Exception e) {
								Log.e("Log", "Error In The HttpConnection" + e.toString());
							}

							// Reading The InputStream

							try {
								BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is, "UTF8"), 16);
								stringBuilder = new StringBuilder();
								stringBuilder.append(bufferedReader.readLine() + "\n");
								String line = "0";

								while ((line = bufferedReader.readLine()) != null) {
									stringBuilder.append(line + "\n");
								}

								is.close();
								result = stringBuilder.toString();

								Log.i("Log", "Result::"+result);

							} catch (Exception e) {
								Log.e("Log",
										"Error While Converting Result Into the Result"
										+ e.toString());
							}

							try{if(table.equals("MstVersionControl"))
							{
								JSONArray jArray = new JSONArray(result);
								JSONObject json_data = null;
								jArrayLength = jArray.length();

								String[] id   = new String[jArrayLength];
								String[] versionNumber = new String[jArrayLength];
								String[] apkUrl = new String[jArrayLength];
								String[] apkName = new String[jArrayLength];

								for(int i=0;i<jArrayLength;i++){
									json_data = jArray.getJSONObject(i);

									id[i] = json_data.getString("id");
									versionNumber[i]  = json_data.getString("VersionNumber");
									apkUrl[i]  = json_data.getString("ApkUrl");
									apkName[i]  = json_data.getString("ApkName");
								}

								Log.v("Log", "Downloading MstVersionControl finished with "+ jArrayLength +" records");
								Log.i("Log", "Downloader.downloadServer inserted data in MstVersionControl");

								gtDatabaseHelper.saveMstVersionControl(id,versionNumber,apkUrl,apkName);
							}

							msg.what = 111;
							handler.sendMessage(msg);

							}
							catch (JSONException e1) {
								Log.e("Log", "Error in the Json",e1.fillInStackTrace());
								msg.what = 222;
								handler.sendMessage(msg);
							} catch (ParseException e1) {
								Log.e("Log",e1.getMessage(),e1.fillInStackTrace());
								msg.what = 222;
								handler.sendMessage(msg);
							}
							catch (Exception ex) {
								Log.e("Log",ex.getMessage(),ex.fillInStackTrace());
								msg.what = 222;
								handler.sendMessage(msg);
							}
						}
					};
					updateThread.start();
				}
			}
			break;
			case R.id.buttonBack:{

				finish();
			}

			break;
			default:
				break;
			}
		}
	}

	public void BackUpDatabase()
	{
		String DBName = DatabaseHelper.DATABASE_NAME.toString();
		try
		{
			File sd = Environment.getExternalStorageDirectory(); 
			//File data = Environment.getDataDirectory(); 
			
			final String STORAGE_PATH = DatabaseHelper.BD_BackUp_PATH; //"FSPDB_BackUp/";
			
			File cnxDir = new File(Environment.getExternalStorageDirectory(), STORAGE_PATH);
            if(!cnxDir.exists())
            {
                cnxDir.mkdir();
            }

			if (sd.canWrite())
			{ 
				Calendar nowDate = Calendar.getInstance();
				SimpleDateFormat dfnowDate = new SimpleDateFormat("dd-MM-yyyy_kk.mm");
				String strnowDate = dfnowDate.format(nowDate.getTime());
				
				String currentDBPath = DBName;
				String backupDBPath = strnowDate+"_BackUp_"+DatabaseHelper.DATABASE_NAME; 
				File currentDB = new File(sd, currentDBPath); 
				File backupDB = new File(cnxDir, backupDBPath); 
				Log.i("Log", "The value of database path is "+currentDBPath);
				if (currentDB.exists())
				{ 
					FileChannel src = new FileInputStream(currentDB).getChannel(); 
					FileChannel dst = new FileOutputStream(backupDB).getChannel(); 
					dst.transferFrom(src, 0, src.size()); 
					src.close(); 
					dst.close(); 
					Toast.makeText(SettingsActivity.this, "Back Up Created with name: "+backupDBPath, Toast.LENGTH_SHORT).show();
				}
				else
				{
					Toast.makeText(SettingsActivity.this, "No Database Available of Name: "+DBName, Toast.LENGTH_SHORT).show();
				}
			} 
			else
			{
				Log.i("Log", "No SD card available");
			}

		} catch (Exception e) { 

				Log.e("Log", "msg if no db present "+e.toString());
		} 
	}
	/*public void RestoreDatabase(String DBName)
	{
		try
		{
			File sd = Environment.getExternalStorageDirectory(); 

			if (sd.canWrite()) 
			{ 
				//String currentDBPath = "\\data\\com.ruralsmart4_1\\databases\\"+DBName;
				//String currentDBPath = sd.getAbsolutePath()+File.separator+DBName;
				String currentDBPath =DBName;
				String backupDBPath =DBName.subSequence(0, DBName.length()-3)+"_BackUp"+".db";
				//String backupDBPath = DBName+"_BackUp"; 
				File currentDB = new File(sd, currentDBPath); 
				File backupDB = new File(sd, backupDBPath); 
				System.out.println("The value of database path is "+currentDBPath);
				if(currentDB.exists()) 
				{ 
					FileChannel src = new FileInputStream(currentDB).getChannel(); 
					FileChannel dst = new FileOutputStream(backupDB).getChannel(); 
					dst.transferFrom(src, 0, src.size()); 
					src.close(); 
					dst.close(); 
					Toast.makeText(this, "Back Up Created with name: "+backupDBPath, Toast.LENGTH_SHORT).show();
				}
				else
				{
					Toast.makeText(this, "No Database Available of Name: "+DBName, Toast.LENGTH_SHORT).show();
				}
			} 
			else
			{
				Log.i("Log", "No SD card available");
			}

		} catch (Exception e) { 

			Log.i("Log", "msg if no db present "+e.toString());
		} 
	}*/

	//creating the Back button
	/*@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.back_button, menu);
		return true;
	}

	//putting the Onclick Event on the Menu

	@Override
	public boolean onOptionsItemSelected(MenuItem menuItem)
	{
		int menuItemId = menuItem.getItemId();

		if(menuItemId == R.id.backbutton)
		{
			finish();
		}
		return false;

	}*/

	//Back Press Down
	@Override
	public void onBackPressed() {

	}

	Handler handler = new Handler(){

		@Override
		public void handleMessage(Message msg) 
		{
			switch(msg.what){
			case 111:{

				if(progDiag!=null && progDiag.isShowing()){
					progDiag.dismiss();
				}

				Intent intent = new Intent (SettingsActivity.this, InstallApkActivity.class);
				startActivity(intent);
			}
			break;

			case 222:{
				if(progDiag!=null && progDiag.isShowing()){
					progDiag.dismiss();
				}
			}
			break;
			}
			super.handleMessage(msg);
		}
	};

	/**
	 * 
	 * Checking the InternetConnection
	 * 
	 */

	public final boolean isInternetOn() {

		ConnectivityManager connec =  (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

		if ( connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED ||
				connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTING ||
				connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTING ||
				connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED ) 
		{
			return true;
		} else if ( connec.getNetworkInfo(0).getState() == NetworkInfo.State.DISCONNECTED ||  
				connec.getNetworkInfo(1).getState() == NetworkInfo.State.DISCONNECTED  ) 
		{

			return false;
		}
		return false;
	}//end of the Internet Connection Checking
}

