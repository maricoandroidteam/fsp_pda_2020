package com.marico.fsp.activity;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.marico.fsp.R;
import com.marico.fsp.adminactivity.AdminReportMenuActivity;
import com.marico.fsp.adminactivity.AdminUserSelectionActivity;
import com.marico.fsp.helper.DatabaseHelper;
import com.marico.fsp.helper.MainMenuHelper;
import com.marico.fsp.helper.VersionControlHelper;
import com.marico.fsp.util.GetJSONArray;


public class MainMenuActivity extends Activity {
	private Button buttonMMDownload,buttonMMEntry,buttonMMUpload,buttonMMReport,buttonMMLogout,buttonMM_Tracking;
	private TextView textViewMMUserName;

	private ImageView imageViewMMSetting;

	private MainMenuHelper mainMenuHelper;
	private Boolean userIsAdmin = false;

	//List<String> arryListVersionControl = new ArrayList<String>();

	VersionControlHelper versionControlHelper; // = new VersionControlHelper(MainMenuActivity.this);
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lyt_main_menu);

		versionControlHelper = new VersionControlHelper(MainMenuActivity.this);

		buttonIdSetter();
	}

	@Override
	public void onBackPressed() {}
	void buttonIdSetter()
	{

		ButtonClickListener buttonClickListener = new ButtonClickListener();
		buttonMMDownload = (Button) findViewById(R.id.buttonMMDownload);
		buttonMMEntry = (Button) findViewById(R.id.buttonMMEntry);
		buttonMMUpload = (Button) findViewById(R.id.buttonMMUpload);
		buttonMMReport = (Button) findViewById(R.id.buttonMMReport);
		imageViewMMSetting =  (ImageView) findViewById(R.id.imageViewMMSetting);
		buttonMMLogout = (Button) findViewById(R.id.buttonMMLogout);
		buttonMM_Tracking = (Button) findViewById(R.id.buttonMM_Tracking);
		textViewMMUserName = (TextView) findViewById(R.id.textViewMMUserName);

		buttonMMDownload.setOnClickListener(buttonClickListener);
		buttonMMEntry.setOnClickListener(buttonClickListener);
		buttonMMUpload.setOnClickListener(buttonClickListener);
		buttonMMReport.setOnClickListener(buttonClickListener);
		imageViewMMSetting.setOnClickListener(buttonClickListener);
		buttonMMLogout.setOnClickListener(buttonClickListener);
		buttonMM_Tracking.setOnClickListener(buttonClickListener);
		mainMenuHelper = new MainMenuHelper(MainMenuActivity.this);

		textViewMMUserName.setText(mainMenuHelper.getUserName());

		userIsAdmin= mainMenuHelper.userisAdmin();
		if(userIsAdmin)
		{
			Toast.makeText(getApplicationContext(), "Welcome Admin", Toast.LENGTH_LONG).show();
			buttonMMUpload.setEnabled(false);
			//buttonMMEntry.setVisibility(View.INVISIBLE);
			buttonMMEntry.setEnabled(false);
		}
		else
		{


		}

	}
	class ButtonClickListener implements OnClickListener
	{
		@Override
		public void onClick(View v) 
		{
			int i = 0;
			int j = 0;
			int countForTrnAvg = 0;
			int countOF_TotalFarmers = 0;
			int closing = 0;
			String[] nutPrice = null;
			float copraPrice = 0,coconutPrice = 0;
			if(!userIsAdmin)
			{
				closing = mainMenuHelper.checkForClosing();
				i = mainMenuHelper.getCountOfTrnFarm_Data();
				j = mainMenuHelper.getUploadCountWithClosing();
				countForTrnAvg = mainMenuHelper.getCoutOfTrnAvg_Data();
				countOF_TotalFarmers = mainMenuHelper.getCountOF_Farmers();
				nutPrice = mainMenuHelper.checkForCopraPrice();
				copraPrice = Float.parseFloat(nutPrice[0]);
				coconutPrice = Float.parseFloat(nutPrice[1]);
			}

			switch (v.getId()) 
			{
			case R.id.buttonMMDownload:

				/*AlertDialog.Builder builder;
				builder = new AlertDialog.Builder(MainMenuActivity.this);

				boolean downloadDataFlag = true;*/

				//new AsynkTaskForDownloadMstVersionControl().execute("");

				if(i == 0 || userIsAdmin)
				{
					if(isInternetOn() == false )
					{
						Log.i("log", "Internet is : false ");
						Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
						return;
					}

					new AsynkTaskForDownloadMstVersionControl().execute("");
				}
				else
				{
					Toast.makeText(getApplicationContext(), getResources().getString(R.string.toast_msg_up_pending), Toast.LENGTH_SHORT).show();
				}
				break;

			case R.id.buttonMMEntry:
				/*boolean checkForOpening = mainMenuHelper.checkForOpening();
				if(checkForOpening == true)
				{
					Intent goBeatSelectionActivity = new Intent(MainMenuActivity.this, FarmerSelectionActivity.class);
					startActivity(goBeatSelectionActivity);
				}
				else
				{
					Toast.makeText(MainMenuActivity.this, "First enter the Opening to start...", Toast.LENGTH_LONG).show();
					Intent intentOpening = new Intent(MainMenuActivity.this,OpeningActivity.class);
					startActivity(intentOpening);
				}*/
				Intent goBeatSelectionActivity = new Intent(MainMenuActivity.this, FarmerSelectionActivity.class);
				startActivity(goBeatSelectionActivity);
				break;

			case R.id.buttonMMUpload:

				if(i==0)
				{
					Toast.makeText(getApplicationContext(), getResources().getString(R.string.No_upload), Toast.LENGTH_SHORT).show();
				}
				else
				{
					if(countForTrnAvg == countOF_TotalFarmers)
					{
						if(closing == 0)
						{
							alertBox();
							return;
							//Toast.makeText(MainMenuActivity.this, "This is your last Upload. \n Please enter the Closing KM before Uploading", Toast.LENGTH_LONG).show();							
						}
						else
						{
							if(copraPrice==0 || coconutPrice == 0)
							{
								alertBoxForNutEntry();
								return;
							}
						}
					}
					else if(closing>0)
					{
						if(copraPrice==0 || coconutPrice == 0)
						{
							alertBoxForNutEntry();
							return;
						}
					}
					if(isInternetOn() == false )
					{
						Log.i("log", "Internet is : false ");

						Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
						return;
					}
					Intent goUploadActivity = new Intent(MainMenuActivity.this, UploadActivity.class);
					startActivity(goUploadActivity);
				}
				break;

			case R.id.buttonMMReport:

				if(userIsAdmin)
				{
					Intent admin= new Intent(MainMenuActivity.this, AdminReportMenuActivity.class);
					startActivity(admin);

				}
				else
				{
					Intent i1 = new Intent(MainMenuActivity.this, UserReportActivity.class);
					startActivity(i1);
				}
				break;
			case R.id.imageViewMMSetting:

				Toast.makeText(getApplicationContext(),getResources().getString(R.string.it_msg), Toast.LENGTH_LONG).show();
				Intent goSettingsActivity = new Intent(MainMenuActivity.this, SettingsActivity.class);
				startActivity(goSettingsActivity);
				break;

			case R.id.buttonMM_Tracking:

				//Toast.makeText(getApplicationContext(),"Km clicked", Toast.LENGTH_LONG).show();

				Intent intentTracking = new Intent(MainMenuActivity.this,TrackingActivity.class);
				startActivity(intentTracking);
				/*boolean checkMstFarm = mainMenuHelper.checkForMstFarm();
				if(checkMstFarm == false)
				{
					Toast.makeText(MainMenuActivity.this, "Please download first..", Toast.LENGTH_LONG).show();
					return;
				}
				if(closing > 0)
				{
					Toast.makeText(MainMenuActivity.this, "Closing is already entered..", Toast.LENGTH_LONG).show();
					return;
				}
				Intent kmIntent = new Intent(MainMenuActivity.this, KilometerActivity.class);
				startActivity(kmIntent);*/

				break;

			case R.id.buttonMMLogout:
				Toast.makeText(getApplicationContext(),getResources().getString(R.string.toast_msg_appclose), Toast.LENGTH_LONG).show();
				finish();
				break;


			}
		}
	}
	public final boolean isInternetOn() 
	{
		ConnectivityManager connec =  (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

		if ( connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED ||
				connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTING ||
				connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTING ||
				connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED ) 
		{
			return true;
		} else if ( connec.getNetworkInfo(0).getState() == NetworkInfo.State.DISCONNECTED ||  
				connec.getNetworkInfo(1).getState() == NetworkInfo.State.DISCONNECTED  ) 
		{

			return false;
		}
		return false;
	}//end of the Internet Connection Checking


	private void alertDownloadData()
	{
		AlertDialog.Builder alertbox = new AlertDialog.Builder(MainMenuActivity.this);
		alertbox.setMessage("Are You Sure To Download");
		alertbox.setCancelable(false);
		alertbox.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface arg0, int arg1) 
			{
				Intent intent;
				if(userIsAdmin)
				{
					intent = new Intent(MainMenuActivity.this, AdminUserSelectionActivity.class);
				}
				else
				{
					mainMenuHelper.deleteTrnKilometer();
					mainMenuHelper.deleteNutsPrice();
					mainMenuHelper.deleteOilPrice();
					intent = new Intent(MainMenuActivity.this, RouteDownloadActivity.class);
				}
				startActivity(intent);
			}
		});

		alertbox.setNegativeButton("No", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface arg0, int arg1) {

			}
		});
		alertbox.setCancelable(false);
		alertbox.show();

	}

	private void alertBox()
	{
		AlertDialog.Builder alertbox = new AlertDialog.Builder(MainMenuActivity.this);
		alertbox.setMessage("This is your last upload \n Please enter the closing KM before Uploading ??");
		alertbox.setCancelable(false);
		alertbox.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface arg0, int arg1) {
				Intent intentKilometer = new Intent(MainMenuActivity.this,KilometerActivity.class);
				startActivity(intentKilometer);				
			}
		});
		alertbox.show();
	}

	private void alertBoxForNutEntry(){
		AlertDialog.Builder alertbox = new AlertDialog.Builder(MainMenuActivity.this);
		alertbox.setMessage("This is your last upload \n Please enter the Coconut and Copra Price before Uploading ??");
		alertbox.setCancelable(false);
		alertbox.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface arg0, int arg1) {
				Intent intentNuts = new Intent(MainMenuActivity.this,NutsTrackingActivity.class);
				startActivity(intentNuts);				
			}
		});
		alertbox.show();
	}


	class AsynkTaskForDownloadMstVersionControl extends AsyncTask<String, String, String>
	{
		ProgressDialog progressDialog;
		int records = 0;
		AlertDialog.Builder builder = new AlertDialog.Builder(MainMenuActivity.this);
		List<String> arryListVersionControl = new ArrayList<String>();
		int appCount=0;
		//boolean downloadDataFlag = true;

		@Override
		protected void onPreExecute() 
		{
			progressDialog = new ProgressDialog(MainMenuActivity.this);
			progressDialog.setCancelable(false);
			progressDialog.setMessage("Please Wait....");
			progressDialog.show();
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... params) 
		{
			try 
			{
				Date date = new Date();
				SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

				// Get Last Update Time from Preferences 
				SharedPreferences prefs = getPreferences(0);  // activity.MainMenuActivity
				String strMandatory = prefs.getString("Mandatory", "N");
				appCount = prefs.getInt("appCount", 0);
				String strlastDate = prefs.getString("lastDate", "01/01/2001");
				String strcurrentDate = dateFormat.format(date);

				//strlastDate = "30/08/2013";
				Log.i("Log", "lastDate : "+strlastDate);
				Log.i("Log", "strcurrentDate : "+strcurrentDate);

				Date currentDate = dateFormat.parse(strcurrentDate);
				Date lastDate = dateFormat.parse(strlastDate);

				/*if(lastDate.after(currentDate))
	        	{
	        		 SharedPreferences.Editor editor = getPreferences(0).edit();
			         editor.putString("lastDate", strcurrentDate);
			         editor.commit();   
	        	}*/

				// Should Activity Check for Updates Now? 
				if(lastDate.before(currentDate) || lastDate.after(currentDate)) 
				{
					// Save current Date for next Check
					SharedPreferences.Editor editor = getPreferences(0).edit();
					editor.putString("lastDate", strcurrentDate);
					editor.commit();        

					// Start Update       
					String URL = versionControlHelper.getUrl();
					String downloadURL_MstVersionControl = URL + "/Download.aspx?TableName=MstVersionControl";

					Log.i("Log", "downloadURL_MstVersionControl URL : "+downloadURL_MstVersionControl);
					records = versionControlHelper.inserIntoMstVersionControlFromDownload(GetJSONArray.getfromURL(downloadURL_MstVersionControl));

					Log.i("Log", "Download records : "+records);

					if(records > 0)
					{
						arryListVersionControl = versionControlHelper.getUpdateVersion();
					}

					return ""+records;
				}
				else if(strMandatory.equalsIgnoreCase("Y"))
				{
					arryListVersionControl = versionControlHelper.getUpdateVersion();
				}


				/* 	
			// Start Update       
            String URL = versionControlHelper.getUrl();
			String downloadURL_MstVersionControl = URL + "/Download.aspx?TableName=MstVersionControl";

			//Log.i("Log", "downloadURL_MstVersionControl URL : "+downloadURL_MstVersionControl);
			records = versionControlHelper.inserIntoMstVersionControlFromDownload(GetJSONArray.getfromURL(downloadURL_MstVersionControl));

			//Log.i("Log", "Download records : "+records);


			if(records > 0)
			{
				arryListVersionControl = versionControlHelper.getUpdateVersion();
			}*/

				Log.i("Log", "ArrayList Size : "+arryListVersionControl.size());

			} catch (Exception e) {
				Log.e("Log","Exception : "+e.getMessage()+""+e.getLocalizedMessage());
				e.printStackTrace();
			}

			return "";
		}

		@Override
		protected void onPostExecute(String result) 
		{
			if(progressDialog.isShowing())
			{
				progressDialog.dismiss();
			}


			if(result.equals("-101"))
			{
				//Toast.makeText(MainMenuActivity.this, "Please restart the app and download your data again..", Toast.LENGTH_LONG).show();

				alertRestart();
				return;
			}

			if (arryListVersionControl.size() > 0) 
			{
				//downloadDataFlag = false;
				final String version = arryListVersionControl.get(0).toString();
				final String apkName =  arryListVersionControl.get(1).toString();
				final String apkUrl =  arryListVersionControl.get(2).toString();
				String mandatory =  arryListVersionControl.get(3).toString();

				String strMandatoryMessage = "";

				SharedPreferences.Editor editor2 = getPreferences(0).edit();
				editor2.putInt("appCount", ++appCount);
				editor2.commit();


				if(appCount>=DatabaseHelper.APPCOUNT)
				{
					mandatory="Y";
				}

				if(mandatory.equalsIgnoreCase("Y"))
				{
					strMandatoryMessage = "Must Install New Version.";
					// Get Last Update Time from Preferences 
					SharedPreferences.Editor editor = getPreferences(0).edit();
					editor.putString("Mandatory", mandatory);
					editor.commit();
				}else {
					strMandatoryMessage = "Install New Version.";
				}

				HandlerClass.handlerClass = new HandlerClass(MainMenuActivity.this, DatabaseHelper.PACKAGENAME, apkName);

				// Do You want to Install New Version
				builder.setMessage(strMandatoryMessage+"\nVersion No. : "+version+"\nApkName : "+apkName)
				.setPositiveButton("YES", new DialogInterface.OnClickListener() 
				{
					public void onClick(DialogInterface dialog, int id) 
					{

						Intent intent = new Intent (MainMenuActivity.this,InstallApkActivity.class);
						startActivity(intent);
					}
				});

				if(mandatory.equals("N"))
				{
					builder.setNegativeButton("NO", new DialogInterface.OnClickListener() 
					{
						public void onClick(DialogInterface dialog, int id) 
						{

							alertDownloadData();

						}
					});
				}
				AlertDialog alert = builder.create();
				alert.show();
				alert.setCancelable(false);
			}
			else
			{
				alertDownloadData();
			}


			/*if(mandatory.equalsIgnoreCase("N"))
			{
				alertDownloadData();
			}*/
			super.onPostExecute(result);
		}
	}


	private void alertRestart()
	{
		AlertDialog.Builder alertbox = new AlertDialog.Builder(MainMenuActivity.this);
		alertbox.setMessage("Please restart the app and download your data again."); // Please Restart Application // "Please restart the app and download your purchase again".
		alertbox.setPositiveButton("OK", new DialogInterface.OnClickListener() 
		{
			public void onClick(DialogInterface arg0, int arg1) 
			{
				HandlerClass handlerClass = new HandlerClass(MainMenuActivity.this);
				handlerClass.BackUpDatabase();
				handlerClass.DeleteDBFile();
				finish();	
			}
		});
		alertbox.show();
		alertbox.setCancelable(false);
	}

	public void hiturl(String apkName, String apkurl)
	{
		DownloadHandler dh = new DownloadHandler();
		//dh.downloadFile(getApplicationContext(), apkurl, apkName);	  
		dh.downloadFile(getApplicationContext(), apkurl, apkName, DatabaseHelper.PATH);	
	}//hitURL Ends

}
