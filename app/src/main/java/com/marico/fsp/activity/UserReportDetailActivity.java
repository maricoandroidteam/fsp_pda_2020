package com.marico.fsp.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.marico.fsp.R;
import com.marico.fsp.helper.UserReportDetailHelper;

public class UserReportDetailActivity extends Activity
{
	private Button buttonRDBack;

	private ListView listViewRDdetail;
	private TextView textViewRDFarmName;
	private UserReportDetailHelper userReportDetailHelper;
	String[] farmerName;
	String[] farmerId;
	ListView listViewDDownload;
	String[]  arrayTreeName,arrayMatureNuts, arrayMatureBunches, arrayLastTwoBunchesCount, arrayButtonNuts;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.lyt_user_report_detail);
		String farmid = getIntent().getStringExtra("farmid");
		String farmName = getIntent().getStringExtra("farmname");


		buttonRDBack = (Button) findViewById(R.id.buttonRDBack);
		textViewRDFarmName = (TextView) findViewById(R.id.textViewRDFarmName);
		listViewRDdetail  = (ListView) findViewById(R.id.listViewRDdetail);

		textViewRDFarmName.setText(farmName);

		buttonRDBack.setOnClickListener(new ButtonOnClickListener());
		userReportDetailHelper = new UserReportDetailHelper(UserReportDetailActivity.this);

		Object obj [] =	userReportDetailHelper.getEntryDate(farmid);
		arrayTreeName =(String[]) obj[0];
		arrayMatureNuts=(String[]) obj[1];
		arrayMatureBunches=(String[]) obj[2];
		arrayLastTwoBunchesCount=(String[]) obj[3];
		arrayButtonNuts=(String[]) obj[4];

		listViewRDdetail.setAdapter(new EntryCustomListView());
	}
	
	@Override
	public void onBackPressed() {}

	class ButtonOnClickListener implements OnClickListener
	{
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.buttonRDBack:
				finish();
				break;

			default:
				break;
			}
		}
	}

	class EntryCustomListView extends BaseAdapter //custom list with help of base adapter for Order Modify Delete Activity
	{
		TextView textViewRDLFrmName, textViewRDLMatureNuts, textViewRDLMatureBunches, textViewRDLLastTwoBunchesCount, textViewRDLButtonNuts;
		TextView textViewRDLHarvested, textViewRDLPHCount, textViewRDLPHMonth, textViewRDLNutsStock, textViewRDLOutlook;

		@Override
		public int getCount() {

			return arrayMatureNuts.length;
		}
		@Override
		public Object getItem(int position) {

			return arrayMatureNuts[position];
		}
		@Override
		public long getItemId(int position) {

			return position;
		}
		@Override
		public View getView(int position, View convertView, ViewGroup parent)
		{
			View row=convertView;
			if(row==null)
			{
				LayoutInflater vi = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				row = vi.inflate(R.layout.lyt_user_report_detail_list,null);
			}
			textViewRDLFrmName = (TextView) row.findViewById(R.id.textViewRDLFrmName);
			textViewRDLMatureNuts = (TextView) row.findViewById(R.id.textViewRDLMatureNuts);
			textViewRDLMatureBunches = (TextView) row.findViewById(R.id.textViewRDLMatureBunches);
			textViewRDLLastTwoBunchesCount = (TextView) row.findViewById(R.id.textViewRDLLastTwoBunchesCount);
			textViewRDLButtonNuts = (TextView) row.findViewById(R.id.textViewRDLButtonNuts);

			textViewRDLFrmName.setText(arrayTreeName[position]);
			textViewRDLMatureNuts.setText(arrayMatureNuts[position]);
			textViewRDLMatureBunches.setText(arrayMatureBunches[position]);
			textViewRDLLastTwoBunchesCount.setText(arrayLastTwoBunchesCount[position]);
			textViewRDLButtonNuts.setText(arrayButtonNuts[position]);

			return row;
		}
	}
}