package com.marico.fsp.activity;

import com.marico.fsp.R;
import com.marico.fsp.helper.OpeningHelper;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class OpeningActivity extends Activity{

	private EditText editTextOpening_OK;
	private TextView textViewRoute_OK;
	private Button buttonCancel_OK,buttonSave_OK;
	private String route_code;
	private OpeningHelper openingHelper;
	private String open;
	private ButtonClicked buttonClicked;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lyt_opening);
		editTextOpening_OK = (EditText) findViewById(R.id.editTextOpening_OK);
		textViewRoute_OK = (TextView) findViewById(R.id.textViewRoute_OK);		
		buttonCancel_OK = (Button) findViewById(R.id.buttonCancel_OK);
		buttonSave_OK = (Button) findViewById(R.id.buttonSave_OK);
		
		openingHelper = new OpeningHelper(OpeningActivity.this);
		buttonClicked = new ButtonClicked();
		route_code = openingHelper.getRouteCode();
		textViewRoute_OK.setText(route_code);
		buttonCancel_OK.setOnClickListener(buttonClicked);
		buttonSave_OK.setOnClickListener(buttonClicked);
	}
	private class ButtonClicked implements OnClickListener
	{
		@Override
		public void onClick(View view) 
		{
			switch (view.getId()) {
			case R.id.buttonCancel_OK:
				finish();
				break;
			case R.id.buttonSave_OK:
				open = editTextOpening_OK.getText().toString();
				if(open.equals("") || open.equals("0"))
				{
					Toast.makeText(OpeningActivity.this, "Please Enter the Opening KM ", Toast.LENGTH_LONG).show();
					return;
				}				
				openingHelper.saveFromOpening(route_code,open);
				finish();
				break;

			default:
				break;
			}
		}
		
	}
}
