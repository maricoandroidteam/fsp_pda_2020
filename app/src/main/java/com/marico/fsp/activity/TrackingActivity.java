package com.marico.fsp.activity;

import com.marico.fsp.R;
import com.marico.fsp.helper.MainMenuHelper;
import com.marico.fsp.helper.TrackingHelper;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class TrackingActivity extends Activity{

	private Button buttonOilTracking,buttonNutTracking,buttonKM_Tracking,buttonKM_Back;
	private ButtonClicked buttonClicked;
	private TrackingHelper trackingHelper;
	Boolean userIsAdmin = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lyt_tracking_main);
		buttonKM_Tracking = (Button) findViewById(R.id.buttonKM_Tracking);
		buttonNutTracking = (Button) findViewById(R.id.buttonNutTracking);
		buttonOilTracking = (Button) findViewById(R.id.buttonOilTracking);
		buttonKM_Back = (Button) findViewById(R.id.buttonKM_Back);
		buttonClicked = new ButtonClicked();
		trackingHelper = new TrackingHelper(TrackingActivity.this);
		buttonKM_Tracking.setOnClickListener(buttonClicked);
		buttonOilTracking.setOnClickListener(buttonClicked);
		buttonNutTracking.setOnClickListener(buttonClicked);
		buttonKM_Back.setOnClickListener(buttonClicked);
		
		
		userIsAdmin = new MainMenuHelper(this).userisAdmin(); 
		
	}
	private class ButtonClicked implements OnClickListener
	{
		@Override
		public void onClick(View v) {
			
			int closing = 0;
				closing = trackingHelper.checkForClosing();
			
			switch (v.getId()) {
			case R.id.buttonKM_Tracking:
				
				boolean checkMstFarm = trackingHelper.checkForMstFarm();
				
				/*if(userIsAdmin)
				{
					checkMstFarm = true;			
				}*/
				
				if(checkMstFarm == false)
				{
					Toast.makeText(TrackingActivity.this, "Please download first..", Toast.LENGTH_LONG).show();
					return;
				}
				
				if(closing > 0)
				{
					if(!userIsAdmin)
					{
						Toast.makeText(TrackingActivity.this, "Closing is already entered..", Toast.LENGTH_LONG).show();
						return;
					}
					
				}
				Intent kmIntent = new Intent(TrackingActivity.this, KilometerActivity.class);
				startActivity(kmIntent);
				break;
			case R.id.buttonNutTracking:
				boolean checkForNutUploaded = trackingHelper.checkForNutUploaded();
				if(userIsAdmin)
				{
					checkForNutUploaded = false;				
				}
				
				if(checkForNutUploaded == true)
				{
					Toast.makeText(TrackingActivity.this, "Already uploaded ", Toast.LENGTH_LONG).show();
				}
				else
				{
					Intent intentNuts = new Intent(TrackingActivity.this, NutsTrackingActivity.class);
					startActivity(intentNuts);
				}				
				break;				
			case R.id.buttonOilTracking:
				
				boolean checkUploadFlag = trackingHelper.checkOilTrackingUploadFlag();
				if(userIsAdmin)
				{
					checkUploadFlag = false;				
				}
				
				if(checkUploadFlag == false)
				{
					Intent intentOil = new Intent(TrackingActivity.this, OilTrackingActivity.class);
					startActivity(intentOil);
				}
				else
				{
					Toast.makeText(TrackingActivity.this, "Oil price already uploaded for today", Toast.LENGTH_LONG).show();
				}
				break;			
			case R.id.buttonKM_Back:
				finish();
				break;
			default:
				break;
			}
		}
		
	}
	@Override
	public void onBackPressed() {
	}
}
