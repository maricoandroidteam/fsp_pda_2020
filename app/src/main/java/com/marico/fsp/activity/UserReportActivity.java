package com.marico.fsp.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;


import com.marico.fsp.R;
import com.marico.fsp.helper.UserReportHelper;

public class UserReportActivity extends Activity
{
	private Button buttonRBack;
	private Spinner	spinnerRRoute;
	private ListView listViewREntry;
	private UserReportHelper userReportHelper;

	String[] route;
	ListView listViewDDownload;
	String[]  arrayFrmId,arrayFrmName,arrayMatureNuts, arrayMatureBunches, arrayLastTwoBunchesCount, arrayButtonNuts;

	String routeid;
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.lyt_user_report);

		buttonRBack = (Button) findViewById(R.id.buttonRBack);
		spinnerRRoute = (Spinner) findViewById(R.id.spinnerRRoute);
		listViewREntry  = (ListView) findViewById(R.id.listViewREntry);



		listViewREntry.setOnItemClickListener(new ListViewClickListener());



		buttonRBack.setOnClickListener(new ButtonOnClickListener());
		userReportHelper = new UserReportHelper(UserReportActivity.this);

		Object []obj = userReportHelper.getRouteInfo();
		route = (String[]) obj[0];

		spinnerRRoute.setAdapter(new FarmerInfoCustomSpinner());
		spinnerRRoute.setOnItemSelectedListener(new SpinnerFarmIdListener());
		}
	@Override
	public void onBackPressed() {}


	class ButtonOnClickListener implements OnClickListener
	{
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.buttonRBack:
				finish();
				break;

			default:
				break;
			}
		}
	}
	class FarmerInfoCustomSpinner extends BaseAdapter //custom spinner with help of base adapter for spinnerOMBeatName
	{
		TextView textViewRSFarmerName;
		@Override
		public int getCount() {

			return route.length;
		}
		@Override
		public Object getItem(int position) {

			return route[position];
		}
		@Override
		public long getItemId(int position) {

			return position;
		}
		@Override
		public View getView(int position, View convertView, ViewGroup parent)
		{
			View row=convertView;
			if(row==null)
			{
				LayoutInflater vi = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				row = vi.inflate(R.layout.lyt_user_report_spinner,null);
			}
			textViewRSFarmerName = (TextView) row.findViewById(R.id.textViewRSFarmerName);
			textViewRSFarmerName.setText(route[position]);
			return row;
		}
	}


	class EntryCustomListView extends BaseAdapter //custom list with help of base adapter for Order Modify Delete Activity
	{


		TextView textViewRLTreeName,textViewRLMatureNuts,textViewRLMatureBunches,textViewRLLastTwoBunchesCount,textViewRLButtonNuts;

		@Override
		public int getCount() {

			return arrayMatureNuts.length;
		}
		@Override
		public Object getItem(int position) {

			return arrayMatureNuts[position];
		}
		@Override
		public long getItemId(int position) {

			return position;
		}
		@Override
		public View getView(int position, View convertView, ViewGroup parent)
		{
			View row=convertView;
			if(row==null)
			{
				LayoutInflater vi = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				row = vi.inflate(R.layout.lyt_user_report_list,null);
			}
			textViewRLTreeName = (TextView) row.findViewById(R.id.textViewRLTreeName);
			textViewRLMatureNuts = (TextView) row.findViewById(R.id.textViewRLMatureNuts);
			textViewRLMatureBunches = (TextView) row.findViewById(R.id.textViewRLMatureBunches);
			textViewRLLastTwoBunchesCount = (TextView) row.findViewById(R.id.textViewRLLastTwoBunchesCount);
			textViewRLButtonNuts = (TextView) row.findViewById(R.id.textViewRLButtonNuts);




			textViewRLTreeName.setText(arrayFrmName[position]);
			textViewRLMatureNuts.setText(arrayMatureNuts[position]);
			textViewRLMatureBunches.setText(arrayMatureBunches[position]);
			textViewRLLastTwoBunchesCount.setText(arrayLastTwoBunchesCount[position]);
			textViewRLButtonNuts.setText(arrayButtonNuts[position]);


			return row;
		}
	}
	class SpinnerFarmIdListener implements OnItemSelectedListener  // Listener class  for SpinnerOMBeatName
	{
		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) 
		{
			routeid=route[arg2];

			Object obj [] =	userReportHelper.getEntryDate(routeid);
			arrayFrmId =(String[]) obj[0];
			arrayFrmName =(String[]) obj[1];
			arrayMatureNuts=(String[]) obj[2];
			arrayMatureBunches=(String[]) obj[3];
			arrayLastTwoBunchesCount=(String[]) obj[4];
			arrayButtonNuts=(String[]) obj[5];

			listViewREntry.setAdapter(new EntryCustomListView());


		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0)
		{			
		}
	}



	class ListViewClickListener implements OnItemClickListener
	{


		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {

			String FrmName = arrayFrmName[arg2];
			String FrmId = arrayFrmId[arg2];
			Log.i("log", "clicked list item");
			Intent intent = new Intent(UserReportActivity.this, UserReportInfoAtivity.class);
			intent.putExtra("farmid", FrmId);
			intent.putExtra("farmname", FrmName);

			startActivity(intent); 

		}
	}

}