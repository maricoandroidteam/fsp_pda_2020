package com.marico.fsp.activity;

import java.util.ArrayList;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.marico.fsp.R;
import com.marico.fsp.helper.DownloadHelper;
import com.marico.fsp.helper.UserTrnLogHelper;
import com.marico.fsp.util.GetJSONArray;

public class DownloadActivity extends Activity
{
	private Button buttonDDwonload, buttonDBack;
	ProgressBar progressBarDDownload;
	ListView listViewDDownload;
	private ArrayList<String> downloadMsgList = new ArrayList<String>();
	CustomListView customListView;
	String route="";
	String userId="";
	private DownloadHelper downloadHelper;
	Boolean downloadfalgdone=false;
	private DownloadDataAsyncTask downloadDataAsyncTask;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lyt_download);
		savedInstanceState = getIntent().getExtras();
		route = getIntent().getStringExtra("route");
		ButtonIdSetter();
	}
	
	void ButtonIdSetter()
	{
		downloadHelper = new DownloadHelper(DownloadActivity.this);
		customListView = new CustomListView();  
		ButtonClickListener buttonClickListener = new ButtonClickListener();
		buttonDDwonload = (Button) findViewById(R.id.buttonDDownload);
		buttonDBack = (Button) findViewById(R.id.buttonDBackNew);

		listViewDDownload = (ListView) findViewById(R.id.listViewDDownload);
		progressBarDDownload = (ProgressBar) findViewById(R.id.progressBarDDownload);
		progressBarDDownload.setVisibility(View.INVISIBLE);
		buttonDDwonload.setOnClickListener(buttonClickListener);
		buttonDBack.setOnClickListener(buttonClickListener);
		listViewDDownload.setAdapter(customListView);
		downloadDataAsyncTask = new DownloadDataAsyncTask();
	}
	
	class ButtonClickListener implements OnClickListener
	{
		@Override
		public void onClick(View v) 
		{
			switch (v.getId()) 
			{
			case R.id.buttonDDownload:
				Log.i("log", "buttonDDownload Clicked");
				buttonDDwonload.setEnabled(false);
				downloadDataAsyncTask.execute("");
				break;

			case R.id.buttonDBackNew:
				Log.i("log", "buttonDBackNew Clicked");
				alertBack();
				break;
			}
		}
	}
	
	class DownloadDataAsyncTask extends AsyncTask<String, String, String> 
	{
		String uid = downloadHelper.getUserId();
		String URL = downloadHelper.getUrl();
		String downloadURL_MstFarm = URL+"/Download.aspx?TableName=ML_Farm_Master&UserId="+uid+"&Routecode="+route;
		String downloadURL_MstLastHarvested = URL+"/Download.aspx?TableName=MstHarvested&UserId="+uid+"&CRITERIA="+route;
		//String downloadURL_CheckLastOilPriceUpload = URL+"/Download.aspx?TableName=CheckLastOilPriceUpload&UserId="+uid;
		
		@Override
		protected String doInBackground(String... params) 
		{
			//create object of InsertDbFromJSONArray 

			int records; // count for total recode
			Log.i("log", "Url is :"+ downloadURL_MstFarm);
			//Log.i("Log", "URL for last upload is: "+downloadURL_CheckLastOilPriceUpload);
			Log.i("log", "route is :"+ route);

			//table MstItem  Downloading
			publishProgress("Downloading Started","1");
			publishProgress(" MstFarm Downloading","1");

			records = downloadHelper.inserIntoMstFarmFromDownload(GetJSONArray.getfromURL(downloadURL_MstFarm));
			if(records<0)
			{
				publishProgress("Downloaded fail try Agin "+records, "1");
				return "";
			}
			
			publishProgress("MstFarm Downloaded records "+records, "2");

			if (isCancelled())
			{
				Log.i("log", "isCancelled");
				return ""; 
			}
			
			
			// The Second table download
			publishProgress(" MstLastHarvested Downloading", "1");

			records=downloadHelper.inserIntoMstLastHarvestedFromDownload(GetJSONArray.getfromURL(downloadURL_MstLastHarvested));
			if(records<0)
			{
				publishProgress("Downloaded fail try Agin "+records, "1");
				return "";
			}
			publishProgress("MstLastHarvested Downloaded records "+records,"2");

			if (isCancelled())
			{
				Log.i("log", "isCancelled");
				return ""; 
			}
			
			
			// Upload User information on server  in UserTrnLog Table
			String UserInfo_URL = URL + "/upload.aspx?TableName=UserTrnLog";
			UserTrnLogHelper userTrnLogHelper = new UserTrnLogHelper(DownloadActivity.this);
			int result = userTrnLogHelper.UserTrnLogUpload("D", UserInfo_URL);
			if(result < 0)
			{
				publishProgress("Error in Downloading  "+result, "1");
				return "";
			}
			
			/*publishProgress(" MstCheckLastOilPrice Downloading","1");
			records = downloadHelper.inserIntoMstCheckLastOilPrice(GetJSONArray.getfromURL(downloadURL_CheckLastOilPriceUpload));
			if(records<0)
			{
				publishProgress("Downloaded fail try Agin "+records,"1");
				return "";
			}
			publishProgress("MstCheckLastOilPrice Downloaded records "+records,"2");
			if (isCancelled())
			{
				Log.i("log", "isCancelled");
				return ""; 
			}
			 */
			
			publishProgress("Downloading complete","1");
			return null;
		}
		@Override
		protected void onPreExecute() 
		{
			progressBarDDownload.setVisibility(View.VISIBLE);
			super.onPreExecute();
		}
		@Override
		protected void onPostExecute(String result) 
		{
			super.onPostExecute(result);
			progressBarDDownload.setVisibility(View.INVISIBLE);
			downloadfalgdone=true;
		}
		@Override
		protected void onProgressUpdate(String... values) {
			super.onProgressUpdate(values);
			if(values[1] == "1")
			{
				downloadMsgList.add(values[0]);
			}
			else
			{
				downloadMsgList.set((downloadMsgList.size()-1), values[0]);
			}
			customListView.notifyDataSetChanged();
		}
	}
	
	class  CustomListView extends BaseAdapter // Custom base adapter for downloadMsgList
	{
		TextView tetViewDStatus;
		@Override
		public int getCount() {
			return downloadMsgList.size();
		}
		@Override
		public Object getItem(int arg0) {
			return downloadMsgList.get(arg0);
		}
		@Override
		public long getItemId(int arg0) {
			return arg0;
		}
		@Override
		public View getView(int arg0, View arg1, ViewGroup arg2) 
		{
			View row=arg1;
			if(row==null)
			{
				LayoutInflater vi = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				row = vi.inflate(R.layout.lyt_download_listview, null);
			}

			tetViewDStatus = (TextView)row.findViewById(R.id.tetViewDStatus);

			tetViewDStatus.setText(downloadMsgList.get(arg0));
			return row;
		}
	}
	
	private void alertBack()
	{
		AlertDialog.Builder alertbox = new AlertDialog.Builder(DownloadActivity.this);
		alertbox.setMessage("Exit Download");
		alertbox.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface arg0, int arg1) {
				if(downloadfalgdone)
				{
					Intent intent = new Intent(DownloadActivity.this, MainMenuActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);
				}				
				else
				{
					finish();
				}
			}
		});
		alertbox.setNegativeButton("No", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface arg0, int arg1) {
			}
		});
		alertbox.show();
	}

	@Override
	public void onBackPressed() {}
}