package com.marico.fsp.activity;

import com.marico.fsp.R;
import com.marico.fsp.helper.MainMenuHelper;
import com.marico.fsp.helper.OilTrackingHelper;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class OilTrackingActivity extends Activity{

	private Button buttonCancel_OT,buttonSave_OT;
	private EditText editTextPalmOil_OT,editTextCoconutOil_OT;
	private ButtonClicked buttonClicked;
	int cnoRate,palmRate;
	private OilTrackingHelper oilTrackingHelper;
	boolean check = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lyt_oil_tracking);
		buttonCancel_OT = (Button) findViewById(R.id.buttonCancel_OT);
		buttonSave_OT = (Button) findViewById(R.id.buttonSave_OT);
		editTextCoconutOil_OT = (EditText) findViewById(R.id.editTextCoconutOil_OT);
		editTextPalmOil_OT = (EditText) findViewById(R.id.editTextPalmOil_OT);	
		
		Boolean userIsAdmin = false;
		userIsAdmin = new MainMenuHelper(this).userisAdmin(); 
		if(userIsAdmin)
		{
			editTextCoconutOil_OT.setKeyListener(null);
			editTextCoconutOil_OT.setEnabled(false);
			
			editTextPalmOil_OT.setKeyListener(null);
			editTextPalmOil_OT.setEnabled(false);
			
			buttonSave_OT.setVisibility(View.GONE);
			buttonCancel_OT.setText("Back");
			buttonCancel_OT.setCompoundDrawablesWithIntrinsicBounds(R.drawable.back_32, 0, 0, 0);
		}
		
		buttonClicked = new ButtonClicked();
		buttonCancel_OT.setOnClickListener(buttonClicked);
		buttonSave_OT.setOnClickListener(buttonClicked);		
		oilTrackingHelper = new OilTrackingHelper(OilTrackingActivity.this);
		String[] strArray = null;
		check = oilTrackingHelper.chechforTrnOils();
		if(check == true)
		{
			strArray = oilTrackingHelper.getTrnOils_Data();
			cnoRate = Integer.parseInt(strArray[0]);
			palmRate = Integer.parseInt(strArray[1]);
			editTextCoconutOil_OT.setText(String.valueOf(cnoRate));
			editTextPalmOil_OT.setText(String.valueOf(palmRate));
		}
		else
		{
			editTextCoconutOil_OT.setText("0");
			editTextPalmOil_OT.setText("0");
		}
	}
	
	@Override
	public void onBackPressed() {
	}
	
	private class ButtonClicked implements OnClickListener
	{
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.buttonSave_OT:
				if(!editTextCoconutOil_OT.getText().toString().trim().equals(""))
				{
					cnoRate = Integer.parseInt(editTextCoconutOil_OT.getText().toString().trim());
				}
				else
					cnoRate = 0;
				if(!editTextPalmOil_OT.getText().toString().trim().equals(""))
				{
					palmRate = Integer.parseInt(editTextPalmOil_OT.getText().toString().trim());
				}
				else
					palmRate = 0;				
					oilTrackingHelper.saveInTrnOilPrice(cnoRate, palmRate, check);
				
				finish();
				break;
			case R.id.buttonCancel_OT:
				finish();
				break;
			default:
				break;
			}
		}		
	}
}