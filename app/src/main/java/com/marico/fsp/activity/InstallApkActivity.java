package com.marico.fsp.activity;

import android.app.Activity;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.marico.fsp.R;
import com.marico.fsp.helper.DatabaseHelper;
import com.marico.fsp.helper.ServerSettingHelper;

public class InstallApkActivity extends Activity{

	private static final String LOG_TAG = "Log";
	public String PATH  = null;
	public String PackageName = null;
	private int viewcount = 0;
	private ServerSettingHelper serverSettingHelper = null;
	ServerSettingsActivity serverSettingsActivity;
	private String[] versionNumberArr = null;
	private String[] apkUrlArr = null;
	private String[] apkNameArr = null;
	private ListView listViewVersionNumber = null;
	private String Version = null;
	private String latestVersion = null;
	private String versionId ;
	private String apkUrl ,apkName;
	private Button updateApkDownload,updateApkDBack;
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lyt_updateapk_main);
		updateApkDownload = (Button) findViewById(R.id.updateApkDownload);
		updateApkDBack = (Button) findViewById(R.id.updateApkDBack);

		updateApkDownload.setOnClickListener(new ButtonClick());
		updateApkDBack.setOnClickListener(new ButtonClick());

		serverSettingHelper = new ServerSettingHelper(InstallApkActivity.this);

		PATH = ServerSettingHelper.PATH;
		PackageName = ServerSettingHelper.PACKAGENAME;

		TextView textViewUACurrentVersion = (TextView)findViewById(R.id.textViewUACurrentVersion);
		TextView textViewUALatestVersion = (TextView)findViewById(R.id.textViewUALatestVersion);

		try {
			Version = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}

		textViewUACurrentVersion.setText(Version);

		listViewVersionNumber = (ListView) findViewById(R.id.listViewVersionNumber);

		//HandlerClass.handlerClass = new HandlerClass(InstallApkActivity.this);


		Object []obj = serverSettingHelper.retrieveVersionDetails();
		versionNumberArr =(String[]) obj[0];
		apkUrlArr =(String[]) obj[1];
		latestVersion = (String) obj[2];
		apkNameArr = (String[]) obj[3];

		Log.i("log", "latestVersion"+latestVersion);
		Log.i("log", "apkUrlArr"+apkUrlArr[0]);
		Log.i("log", "versionNumberArr"+versionNumberArr[0]);


		viewcount=versionNumberArr.length;
		//ListVersionNumberAdapter listVersionNumberAdapter = new ListVersionNumberAdapter();
		//listViewVersionNumber.setAdapter(listVersionNumberAdapter);

		if(viewcount > 0 ){
			ArrayAdapter<String> arrayadapter = new ArrayAdapter<String>(InstallApkActivity.this,android.R.layout.simple_list_item_single_choice,apkNameArr);
			listViewVersionNumber.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
			listViewVersionNumber.setAdapter(arrayadapter);

			textViewUALatestVersion.setText(latestVersion);

			listViewVersionNumber.setOnItemClickListener(new AdapterView.OnItemClickListener()
			{
				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position,long id) 
				{
					Log.v(LOG_TAG, "InstallApkActivity : ListviewonItemclicklistner"+position);
					versionId = (String)listViewVersionNumber.getItemAtPosition(position);
					Log.v(LOG_TAG, "InstallApkActivity : ListviewonItemclicklistner : versionId : "+versionId);
					apkUrl = apkUrlArr[position];
					apkName=apkNameArr[position];
				}
			});
		}
	}//onCreate Ends

	@Override
	protected void onStart() {
		super.onStart();
	}//onStart Ends



	class ListVersionNumberAdapter extends BaseAdapter{

		@Override
		public int getCount() {
			Log.i(LOG_TAG, "View Count is "+viewcount);
			if(viewcount == 0){
				return 0;
			}
			return viewcount;
		}

		@Override
		public Object getItem(int position) {

			return null;
		}

		@Override
		public long getItemId(int position) {

			return position;
		}

		@Override
		public View getView(final int position, View view, ViewGroup parent) 
		{
			final ViewHolder viewHolder;
			View rowView = view;

			if(rowView == null){

				LayoutInflater layoutinflate =  LayoutInflater.from(InstallApkActivity.this);
				rowView=layoutinflate.inflate(R.layout.lyt_updataapk_list, parent, false);

				viewHolder = new ViewHolder();

				viewHolder.radioUAVersion = (RadioButton)rowView.findViewById(R.id.radioUAVersion);

				rowView.setTag(viewHolder);
			}else{
				viewHolder = (ViewHolder)rowView.getTag();
			}

			viewHolder.ref = position;
			viewHolder.radioUAVersion.setText(apkNameArr[position]);

			return rowView;			 
		}//End of method getView
	}//End of class ImageInfo

	class ViewHolder{
		RadioButton radioUAVersion = null;
		int ref;
	}//End of class ViewHolder



	public void hiturl(String apkName,String apkurl)
	{
		DownloadHandler dh = new DownloadHandler();
		dh.downloadFile(getApplicationContext(), apkurl, apkName, PATH);	      
	}//hitURL Ends

	/*public void InstallApplication()
	{   
		Uri packageURI = Uri.parse(PackageName.toString());
		Intent intent = new Intent(android.content.Intent.ACTION_VIEW, packageURI);

		//Intent intent = new Intent(android.content.Intent.ACTION_VIEW);
		//intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		//intent.setFlags(Intent.ACTION_PACKAGE_REPLACED);
		//intent.setAction(Settings.ACTION_APPLICATION_SETTINGS);

		intent.setDataAndType

		//(Uri.fromFile(new File(Environment.getExternalStorageDirectory() + "/"+PATH +"MBLPDAv1.01.apk")),//+ versionId.toString())),
		(Uri.fromFile(new File(Environment.getExternalStorageDirectory() + "/"+PATH +apkName)),//+ versionId.toString())), 
		"application/vnd.android.package-archive");

		startActivity(intent);  
	}*/

	/*public void UnInstallApplication(String packageName)// Specific package Name Uninstall.
	{
		//Uri packageURI = Uri.parse("package:com.CheckInstallApp");
		Uri packageURI = Uri.parse(packageName.toString());
		Intent uninstallIntent = new Intent(Intent.ACTION_DELETE, packageURI);
		startActivity(uninstallIntent); 
	}*/


	class ButtonClick implements OnClickListener
	{
		@Override
		public void onClick(View v)
		{
			switch (v.getId()) 
			{
			case R.id.updateApkDownload:

				HandlerClass.handlerClass = new HandlerClass(InstallApkActivity.this, DatabaseHelper.PACKAGENAME, apkName);

				if(apkUrl==null)
				{
					Toast.makeText(InstallApkActivity.this, "please select One Apk", Toast.LENGTH_SHORT).show();
					break;
				}
				
				Log.v(LOG_TAG, versionId+" :onCreateOptionsMenu : "+apkUrl);
				
				updateApkDownload.setEnabled(false);
				
				Thread t = new Thread(new Runnable() 
				{
					@Override
					public void run() 
					{
						Message msg = Message.obtain();
						hiturl(versionId, apkUrl);
						msg.what = 111;
						HandlerClass.handlerClass.sendMessage(msg);
					}
				});
				t.start();

				break;

			case R.id.updateApkDBack:
				finish();
				break;
			}
		}
	}
	
	@Override
	public void onBackPressed() {
	}
}//Activity Ends
