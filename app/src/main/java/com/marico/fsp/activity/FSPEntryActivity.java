package com.marico.fsp.activity;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.marico.fsp.R;
import com.marico.fsp.helper.FSPEntryHelper;
import com.marico.fsp.location.MyLocation;
import com.marico.fsp.location.MyLocationNetwork;
import com.marico.fsp.location.MyLocation.LocationResult;
import com.marico.fsp.location.MyLocationNetwork.LocationResultNetWork;
public class FSPEntryActivity extends Activity{

	private ListView listViewEntry;
	private TextView textViewFarmerNameFSPEntry;
	private EntryListAdapter entryListAdapter;
	private ArrayList<String> arrayListTreeName;

	private HashMap<String, ArrayList<String>> hashMapSaveToDB;

	//private CustomizeDialog customizeDialog;
	private String farmerName,farmerId;
	private int treeCount;
	private int flag;


	private ButtonClicked buttonClicked;
	private FSPEntryHelper fspEntryHelperHelper;


	private Button buttonSave,buttonCancel;

	/*
	 *  The below decalaration for Location only 
	 *  
	 *  (non-Javadoc)
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */

	private LocationManager locationManager;
	boolean gps_enabled = false;
	private LocationResultNetWork locationResultNetWork = null;
	public static String provider = "NA";
	public static double longitude = 0.0 ,lattitude = 0.0,accuracy = 0.0;
	//public static int count;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lyt_entry);
		methodForLocation();
		arrayListTreeName = new ArrayList<String>();
		savedInstanceState = getIntent().getExtras();
		farmerName = savedInstanceState.getString("FarmerName");
		farmerId = savedInstanceState.getString("FarmerId");
		flag = savedInstanceState.getInt("Flag");
		treeCount = savedInstanceState.getInt("TreeCount");


		if(treeCount>500)
		{
			for(int i = 1;i<=10;i++)
			{
				arrayListTreeName.add("Tree "+i);
			}
		}
		else
		{
			for(int i = 1;i<=5;i++)
			{
				arrayListTreeName.add("Tree "+i);
			}
		}

		//customizeDialog = new CustomizeDialog(FSPEntryActivity.this);
		textViewFarmerNameFSPEntry = (TextView) findViewById(R.id.textViewFarmerNameFSPEntry);
		textViewFarmerNameFSPEntry.setText(farmerName);
		listViewEntry = (ListView) findViewById(R.id.listViewEntry);
		buttonCancel = (Button) findViewById(R.id.buttonCancel);
		buttonSave = (Button) findViewById(R.id.buttonSave);
		buttonClicked = new ButtonClicked();
		fspEntryHelperHelper = new FSPEntryHelper(FSPEntryActivity.this);
		fspEntryHelperHelper.deleteFromFarmDataTemp();
		buttonSave.setOnClickListener(buttonClicked);
		buttonCancel.setOnClickListener(buttonClicked);
		if(flag == 1)
		{
			hashMapSaveToDB = new HashMap<String, ArrayList<String>>();
		}
		else
		{
			hashMapSaveToDB = fspEntryHelperHelper.getFarmerData(farmerId);
		}
		entryListAdapter = new EntryListAdapter();
		listViewEntry.setAdapter(entryListAdapter);
	}
	private void methodForLocation() {
		// TODO Auto-generated method stub
		if(locationManager == null){
			locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		}
		gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
		if(!gps_enabled)
		{
			showSettingsAlert();
		}
		MyLocation myLocation = new MyLocation();
		Log.i("Log", "This is BEFORE mylocation init method called");
		myLocation.init(FSPEntryActivity.this, locationResult);
	}
	LocationResult locationResult = new LocationResult(){
		@Override
		public void gotLocation(Location loc) {
			//locations = loc;
			if(loc != null)
			{
				//locatn = "Lattitude "+loc.getLatitude()+"  Longitude  "+loc.getLongitude()+"  Accuracy "+loc.getAccuracy()+" Provider  "+loc.getProvider();
				//	Log.i("Log", "Locn "+locatn);
				lattitude = loc.getLatitude();
				longitude = loc.getLongitude();
				provider = loc.getProvider();
				accuracy = loc.getAccuracy();
				/*runOnUiThread(new Runnable() {					
					@Override
					public void run() {
						if(progressDialog.isShowing())
						{
							progressDialog.dismiss();
						}
					Log.i("Log", "Lattitude "+lattitude+"  Longi "+longitude+"  provider "+provider+"  accuracy "+accuracy+"  inside FSPConfirm RUN");
					}
				});*/
				Log.i("Log", "Lattitude "+lattitude+"  Longi "+longitude+"  provider "+provider+"  accuracy "+accuracy+"  inside FSPConfirm");
			}
			else
			{
				getFromNetworkP();
			}
		}
	};
	private void getFromNetworkP() {
		// TODO Auto-generated method stub
		MyLocationNetwork myLocation = new MyLocationNetwork();
		locationResultNetWork = new LocationResultNetWork(){
			@Override
			public void gotLocationNetwork(Location location) {		
				if(location != null)
				{
					//locatn = "Lattitude: "+location.getLatitude()+"  longi  "+location.getLongitude()+"  Accur "+location.getAccuracy()+"  Time "+location.getTime();
					//Log.i("Log",locatn);
					/*runOnUiThread(new Runnable() {						
						@Override
						public void run() {
							if(progressDialog.isShowing())
							{
								progressDialog.dismiss();
							}
						Log.i("Log", "Lattitude "+lattitude+"  Longi "+longitude+"  provider "+provider+"  accuracy "+accuracy+"  inside FSPConfirm RUN on NP");						
						}
					});*/						
					lattitude = location.getLatitude();
					longitude = location.getLongitude();
					provider = location.getProvider();
					accuracy = location.getAccuracy();
					Log.i("Log", "Lattitude "+lattitude+"  Longi "+longitude+"  provider "+provider+"  accuracy "+accuracy+"  inside FSPConfirm");
				}
				else
				{
					Log.i("Log","Null location got here");
					lattitude = 0.0;
					longitude = 0.0;
					provider = "NA";
					accuracy = 0.0;
					Log.i("Log", "Lattitude "+lattitude+"  Longi "+longitude+"  provider "+provider+"  accuracy "+accuracy);
				}
			}
		};
		myLocation.getLocation(getApplicationContext(), locationResultNetWork);
	}
	public void showSettingsAlert(){
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(FSPEntryActivity.this);
		alertDialog.setCancelable(false);
		// Setting Dialog Title
		alertDialog.setTitle("GPS not Enabled");
		// Setting Dialog Message
		alertDialog.setMessage("Please Enabled GPS");
		// On pressing Settings button
		alertDialog.setNeutralButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int which) {
				dialog.cancel();
				Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
				startActivity(intent);
			}
		});
		// Showing Alert Message
		alertDialog.show();
	}
	private class ButtonClicked implements OnClickListener
	{
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {
			case R.id.buttonCancel:
				AlertDialog.Builder alertbox = new AlertDialog.Builder(FSPEntryActivity.this);
				alertbox.setMessage(" Do you want to go back? ");
				alertbox.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface arg0, int arg1) {
						fspEntryHelperHelper.deleteFromFarmDataTemp();
						finish();
					}
				});
				alertbox.setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface arg0, int arg1) {

					}
				});	
				alertbox.show();

				break;
			case R.id.buttonSave:

				new InsertIntoTemp().execute("");
				break;

			default:
				break;
			}
		}		
	}
	
	@Override
	public void onBackPressed() {
	}
	
	class InsertIntoTemp extends AsyncTask<String, String, String>
	{
		ProgressDialog progressDialog ;
		TextView textViewmsg;
		int count=0;
		@Override
		protected void onPreExecute() {
			progressDialog= new ProgressDialog(FSPEntryActivity.this);
			progressDialog.setTitle("Please Wait..");
			progressDialog.setMessage("Saving");
			progressDialog.setCancelable(false);
			progressDialog.show();
			super.onPreExecute();
		}
		@Override
		protected String doInBackground(String... params) {
			fspEntryHelperHelper.deleteFromFarmDataTemp();

			boolean b =fspEntryHelperHelper.checkHashMapdata(arrayListTreeName.size(), hashMapSaveToDB);
			if(b)
			{
				count=	fspEntryHelperHelper.saveInFarmDataTemp(farmerId,hashMapSaveToDB);
				Log.i("log", "total data insert in temp :"+count);
			}
			else
			{
				count=0;
			}

			return null;
		}
		@Override
		protected void onPostExecute(String result) {
			if(progressDialog.isShowing())
			{
				progressDialog.dismiss();
			}	
			if(count>0)
			{
				Intent intent = new Intent(FSPEntryActivity.this,FSPConfirmActivity.class);
				intent.putExtra("FarmerId", farmerId);
				intent.putExtra("FarmerName", farmerName);
				intent.putExtra("Flag", flag);
				startActivityForResult(intent,1004);
			}
			else
			{
				Toast.makeText(FSPEntryActivity.this, "Please Enter All data", Toast.LENGTH_SHORT).show();
			}
			super.onPostExecute(result);
		}
	}
	class EntryListAdapter extends BaseAdapter
	{
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return arrayListTreeName.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return arrayListTreeName.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			final ViewHolder viewHolder;
			View rowView = convertView;
			if(rowView == null)
			{
				LayoutInflater layoutinflate =LayoutInflater.from(FSPEntryActivity.this);
				rowView=layoutinflate.inflate(R.layout.lyt_entry_list, parent, false);
				viewHolder = new ViewHolder();
				viewHolder.textViewTreeNameEntryList = (TextView)rowView.findViewById(R.id.textViewTreeNameEntryList);


				viewHolder.editTextMatureBunchesEntryList = (EditText) rowView.findViewById(R.id.editTextMatureBunchesEntryList);
				viewHolder.editTextMatureNutsEntryList = (EditText) rowView.findViewById(R.id.editTextMatureNutsEntryList);
				viewHolder.editTextLastTwoBunchesCountEntryList = (EditText) rowView.findViewById(R.id.editTextLastTwoBunchesCountEntryList);
				viewHolder.editTextButtonNutsEntryList = (EditText) rowView.findViewById(R.id.editTextButtonNutsEntryList);


				viewHolder.editTextMatureBunchesEntryList.addTextChangedListener(new ListTextWatcher(viewHolder, viewHolder.editTextMatureBunchesEntryList));
				viewHolder.editTextMatureNutsEntryList.addTextChangedListener(new ListTextWatcher(viewHolder, viewHolder.editTextMatureNutsEntryList));
				viewHolder.editTextLastTwoBunchesCountEntryList.addTextChangedListener(new ListTextWatcher(viewHolder, viewHolder.editTextLastTwoBunchesCountEntryList));
				viewHolder.editTextButtonNutsEntryList.addTextChangedListener(new ListTextWatcher(viewHolder,viewHolder.editTextButtonNutsEntryList));

				
				rowView.setTag(viewHolder);
			}
			else
			{
				viewHolder = (ViewHolder)rowView.getTag();
			}
			viewHolder.ref=position;
			if(hashMapSaveToDB.containsKey(arrayListTreeName.get(position)))
			{
				ArrayList<String> al=hashMapSaveToDB.get(arrayListTreeName.get(viewHolder.ref));
				String btnNutsEntryList=al.get(0);
				String lastTwoBunchesCount = al.get(3);
				String matureBunchesEntry = al.get(2);
				String matureNutsEntry = al.get(1);


				String treeName = al.get(5);
				viewHolder.textViewTreeNameEntryList.setText(arrayListTreeName.get(position));

				viewHolder.editTextButtonNutsEntryList.setText(btnNutsEntryList);
				viewHolder.editTextLastTwoBunchesCountEntryList.setText(lastTwoBunchesCount);
				viewHolder.editTextMatureBunchesEntryList.setText(matureBunchesEntry);
				viewHolder.editTextMatureNutsEntryList.setText(matureNutsEntry);




				viewHolder.textViewTreeNameEntryList.setText(treeName);
				//viewHolder.textViewPrevoiousHarvestingMonthEntryList.setText(month);
			}
			else
			{
				viewHolder.textViewTreeNameEntryList.setText(arrayListTreeName.get(position));
				viewHolder.editTextButtonNutsEntryList.setText(null);
				viewHolder.editTextLastTwoBunchesCountEntryList.setText(null);
				viewHolder.editTextMatureBunchesEntryList.setText(null);
				viewHolder.editTextMatureNutsEntryList.setText(null);
			}
			return rowView;

		}	
	}
	private class ListTextWatcher implements TextWatcher
	{
		private ViewHolder viewHolder; 
		private View view;

		public ListTextWatcher(ViewHolder viewHolder,View view) {
			// TODO Auto-generated constructor stub
			this.viewHolder = viewHolder;
			this.view = view;
		}
		@Override
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub

			ArrayList<String> arrayList = new ArrayList<String>();
			switch (view.getId()) {
			case R.id.editTextButtonNutsEntryList:				
				//Toast.makeText(FSPEntryActivity.this, "Its done string is:  "+text, Toast.LENGTH_LONG).show();

				if(!s.toString().equals(null)|| s.toString() != null)
				{
					//ArrayList< String> arrayList = new ArrayList<String>();
					arrayList.add(0,s.toString());
					arrayList.add(1,viewHolder.editTextMatureNutsEntryList.getText().toString());
					arrayList.add(2, viewHolder.editTextMatureBunchesEntryList.getText().toString());
					arrayList.add(3, viewHolder.editTextLastTwoBunchesCountEntryList.getText().toString());
					arrayList.add(4,viewHolder.editTextButtonNutsEntryList.getText().toString());

					arrayList.add(5, viewHolder.textViewTreeNameEntryList.getText().toString());
					hashMapSaveToDB.put(arrayListTreeName.get(viewHolder.ref), arrayList);
				}				
				break;
			case R.id.editTextLastTwoBunchesCountEntryList:
				//Toast.makeText(FSPEntryActivity.this, "Its done string is:  "+text, Toast.LENGTH_LONG).show();	
				if(!s.toString().equals(null)|| s.toString() != null)
				{
					arrayList.add(0,viewHolder.editTextButtonNutsEntryList.getText().toString());
					arrayList.add(1,viewHolder.editTextMatureNutsEntryList.getText().toString());
					arrayList.add(2, viewHolder.editTextMatureBunchesEntryList.getText().toString());
					arrayList.add(3, s.toString());
					arrayList.add(4,viewHolder.editTextButtonNutsEntryList.getText().toString());

					arrayList.add(5, viewHolder.textViewTreeNameEntryList.getText().toString());
					hashMapSaveToDB.put(arrayListTreeName.get(viewHolder.ref), arrayList);

				}
				break;

			case R.id.editTextMatureBunchesEntryList:
				//Toast.makeText(FSPEntryActivity.this, "Its done string is:  "+text, Toast.LENGTH_LONG).show();				
				if(!s.toString().equals(null)|| s.toString() != null)
				{
					arrayList.add(0,viewHolder.editTextButtonNutsEntryList.getText().toString());
					arrayList.add(1,viewHolder.editTextMatureNutsEntryList.getText().toString());
					arrayList.add(2, s.toString());
					arrayList.add(3, viewHolder.editTextLastTwoBunchesCountEntryList.getText().toString());
					arrayList.add(4,viewHolder.editTextButtonNutsEntryList.getText().toString());

					arrayList.add(5, viewHolder.textViewTreeNameEntryList.getText().toString());
					hashMapSaveToDB.put(arrayListTreeName.get(viewHolder.ref), arrayList);
				}
				break;
			case R.id.editTextMatureNutsEntryList:
				//Toast.makeText(FSPEntryActivity.this, "Its done string is:  "+text, Toast.LENGTH_LONG).show();
				if(!s.toString().equals(null)|| s.toString() != null)
				{
					arrayList.add(0,viewHolder.editTextButtonNutsEntryList.getText().toString());
					arrayList.add(1,s.toString());
					arrayList.add(2, viewHolder.editTextMatureBunchesEntryList.getText().toString());
					arrayList.add(3, viewHolder.editTextLastTwoBunchesCountEntryList.getText().toString());
					arrayList.add(4,viewHolder.editTextButtonNutsEntryList.getText().toString());

					arrayList.add(5, viewHolder.textViewTreeNameEntryList.getText().toString());
					hashMapSaveToDB.put(arrayListTreeName.get(viewHolder.ref), arrayList);
				}
				break;
			default:
				break;
			}
		}
		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,	int after) {
			// TODO Auto-generated method stub		
		}
		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
			// TODO Auto-generated method stub		
		}		
	}
	private class ViewHolder{
		TextView textViewTreeNameEntryList;
		EditText editTextMatureBunchesEntryList;
		EditText editTextMatureNutsEntryList,editTextLastTwoBunchesCountEntryList;
		EditText editTextButtonNutsEntryList;

		int ref;
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode == 1001)
		{
			setResult(1001);
			finish();
		}
	}
}