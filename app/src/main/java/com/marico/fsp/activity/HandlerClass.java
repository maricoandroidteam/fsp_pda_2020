package com.marico.fsp.activity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.marico.fsp.helper.DatabaseHelper;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

@SuppressLint("SimpleDateFormat")
public class HandlerClass extends Handler {

	static HandlerClass handlerClass = null;
	private static final String LOG_TAG = "Log";
	private Activity activity;

	public HandlerClass(Activity act){
		activity = act;
	}
	
	//static HandlerClass handlerClass = null;
	//private static final String LOG_TAG = "Log";
	//private Activity activity;

	String PackageName, apkName; //, path;
	public HandlerClass(Activity act, String packageName, String apkName)
	{
		this.activity = act;
		this.PackageName = packageName;
		this.apkName = apkName;
		//this.path = path;
	}

	@Override
	public void handleMessage(Message msg) 
	{
		switch(msg.what)
		{
		case 111:
		{
			Log.v(LOG_TAG, "Handler: msg.what: 111");
			//dismissProgressDialog();
		}
		break;
		case 222:
		{
			Log.v(LOG_TAG, "Handler: msg.what: 222");
			/*InstallApkActivity installApk =(InstallApkActivity) activity;
			installApk.InstallApplication();
			installApk.UnInstallApplication(installApk.PackageName.toString());*/
			
			BackUpDatabase();
			InstallApplication();
			UnInstallApplication();
			DeleteDBFile();
		}
		break;
		}
		super.handleMessage(msg);
	}
	
	
	public void UnInstallApplication()// Specific package Name Uninstall.
	{
		//Uri packageURI = Uri.parse("package:com.CheckInstallApp");
		Uri packageURI = Uri.parse(PackageName.toString());
		Intent uninstallIntent = new Intent(Intent.ACTION_DELETE, packageURI);
		activity.startActivity(uninstallIntent); 
	}
	
	public void InstallApplication()
	{   
		Uri packageURI = Uri.parse(PackageName.toString());
		Intent intent = new Intent(android.content.Intent.ACTION_VIEW, packageURI);

		//Intent intent = new Intent(android.content.Intent.ACTION_VIEW);
		//intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		//intent.setFlags(Intent.ACTION_PACKAGE_REPLACED);
		//intent.setAction(Settings. ACTION_APPLICATION_SETTINGS);

		intent.setDataAndType(Uri.fromFile(new File(Environment.getExternalStorageDirectory() + "/"+DatabaseHelper.PATH + apkName.toString())), "application/vnd.android.package-archive");

		activity.startActivity(intent);  
	}
	
	public void DeleteDBFile()
	{
		String DBName = DatabaseHelper.DATABASE_NAME.toString();
		try 
		{
			File sd = Environment.getExternalStorageDirectory(); 
			String currentDBPath = DBName;
			
			if(sd.canWrite())
			{
				File currentDB = new File(sd, currentDBPath);
				if(currentDB.exists())
				{
					currentDB.delete();
					Toast.makeText(activity, "Delete "+DBName+" File From SDCard.", Toast.LENGTH_SHORT).show();
				}
			}else
			{
				Log.i("Log", "No SD card available");
			}
		} catch (Exception e) 
		{
			Log.e("Log", "msg if no db present "+e.toString());
		}
	}
	
	public void BackUpDatabase()
	{
		String DBName = DatabaseHelper.DATABASE_NAME.toString();
		try
		{
			File sd = Environment.getExternalStorageDirectory(); 
			//File data = Environment.getDataDirectory(); 
			
			final String STORAGE_PATH = DatabaseHelper.BD_BackUp_PATH; //"FSPDB_BackUp/";
			
			File cnxDir = new File(Environment.getExternalStorageDirectory(), STORAGE_PATH);
            if(!cnxDir.exists())
            {
                cnxDir.mkdir();
            }

			if (sd.canWrite())
			{ 
				Calendar nowDate = Calendar.getInstance();
				SimpleDateFormat dfnowDate = new SimpleDateFormat("dd-MM-yyyy_kk.mm");
				String strnowDate = dfnowDate.format(nowDate.getTime());
				
				String currentDBPath = DBName;
				String backupDBPath = strnowDate+"_BackUp_"+DatabaseHelper.DATABASE_NAME; 
				File currentDB = new File(sd, currentDBPath); 
				File backupDB = new File(cnxDir, backupDBPath); 
				Log.i("Log", "The value of database path is "+currentDBPath);
				if (currentDB.exists())
				{ 
					FileChannel src = new FileInputStream(currentDB).getChannel(); 
					FileChannel dst = new FileOutputStream(backupDB).getChannel(); 
					dst.transferFrom(src, 0, src.size()); 
					src.close(); 
					dst.close(); 
					Toast.makeText(activity, "Back Up Created with name: "+backupDBPath, Toast.LENGTH_SHORT).show();
				}
				else
				{
					Toast.makeText(activity, "No Database Available of Name: "+DBName, Toast.LENGTH_SHORT).show();
				}
			} 
			else
			{
				Log.i("Log", "No SD card available");
			}

		} catch (Exception e) { 

			Log.e("Log", "msg if no db present "+e.toString());
		} 
	}
}
