package com.marico.fsp.activity;

import com.marico.fsp.R;
import com.marico.fsp.helper.KilometerHelper;
import com.marico.fsp.helper.MainMenuHelper;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class KilometerActivity extends Activity{

	private Button buttonSave_KM,buttonCancel_KM;
	private ButtonClicked buttonClicked;
	private KilometerHelper kilometerHelper;
	private EditText editTextOpening_KM,editTextClosing_KM,editTextReson_KM;
	private TextView textViewTravelled_KM,textViewRoute_KM;
	
	private String route_code,reson;
	//private String opening, closing, travelled;
	//private int opening_Int = 0,closing_Int = 0,travelled_Int = 0;
	private int close,open,da = 0;
	private int traveld = 0;
	boolean closeEntered = false;
	public static int closingKM = 0;
	String[] oilprice;
	private boolean hasData = false;
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lyt_kilometer);
		buttonCancel_KM = (Button) findViewById(R.id.buttonCancel_KM);
		buttonSave_KM = (Button) findViewById(R.id.buttonSave_KM);
		
		textViewRoute_KM = (TextView) findViewById(R.id.textViewRoute_KM);
		editTextOpening_KM = (EditText) findViewById(R.id.editTextOpening_KM);
		editTextClosing_KM = (EditText) findViewById(R.id.editTextClosing_KM);
		editTextReson_KM = (EditText) findViewById(R.id.editTextReson_KM);
		
		textViewTravelled_KM = (TextView) findViewById(R.id.textViewTravelled_KM);
		
		Boolean userIsAdmin = false;
		userIsAdmin = new MainMenuHelper(this).userisAdmin(); 
		if(userIsAdmin)
		{
			editTextOpening_KM.setKeyListener(null);
			editTextOpening_KM.setEnabled(false);
			
			editTextClosing_KM.setKeyListener(null);
			editTextClosing_KM.setEnabled(false);
			
			editTextReson_KM.setKeyListener(null);
			editTextReson_KM.setEnabled(false);
			
			buttonSave_KM.setVisibility(View.GONE);
			buttonCancel_KM.setText("Back");
			buttonCancel_KM.setCompoundDrawablesWithIntrinsicBounds(R.drawable.back_32, 0, 0, 0);
		}
		
		kilometerHelper = new KilometerHelper(KilometerActivity.this);
		hasData = kilometerHelper.checkForTrnKilometer();
		String[] strArray = null;
		if(hasData == true)
		{
			strArray = kilometerHelper.getTrnKM_Data();
			open = Integer.parseInt(strArray[0]);
			close = Integer.parseInt(strArray[1]);
			traveld = Integer.parseInt(strArray[4]);
			reson = strArray[2];
			editTextOpening_KM.setText(String.valueOf(open));
			editTextClosing_KM.setText(String.valueOf(close));
			textViewTravelled_KM.setText(String.valueOf(traveld));
			editTextReson_KM.setText(reson);
		}					
		
		editTextOpening_KM.addTextChangedListener(new EditTextWatcherForOpening());
		editTextClosing_KM.addTextChangedListener(new EditTextWatcherForClosing());
		buttonClicked = new ButtonClicked();		
		buttonCancel_KM.setOnClickListener(buttonClicked);
		buttonSave_KM.setOnClickListener(buttonClicked);
		
		route_code = kilometerHelper.getRouteCode();
		textViewRoute_KM.setText(route_code);
	}
	
	@Override
	public void onBackPressed() {
	}
	
	private class EditTextWatcherForClosing implements TextWatcher
	{
		@Override
		public void afterTextChanged(Editable s) 
		{
			/*switch (et) {
			case value:
				
				break;

			default:
				break;
			}*/
			 String str = s.toString().trim();		
			 Log.i("Log", "in after text changed :"+str);
			 if(!str.equals(""))
			 {
				 close = Integer.parseInt(str); 
			 }
			 else
			 {
				 close = 0;
			 }
			 traveld = close - open;
			 textViewTravelled_KM.setText(String.valueOf(traveld));
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		}
		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
		}
	}
	private class EditTextWatcherForOpening implements TextWatcher
	{
		@Override
		public void afterTextChanged(Editable s) 
		{
			/*switch (et) {
			case value:
				break;
			default:
				break;
			}*/
			
			 String str = s.toString().trim();		
			 Log.i("Log", "in after text changed :"+str);
			 if(!str.equals(""))
			 {
				 open = Integer.parseInt(str); 
			 }
			 else
			 {
				 open = 0;
			 }
			 traveld = close - open;
			 textViewTravelled_KM.setText(String.valueOf(traveld));
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		}
		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
		}
	}
	
	private class ButtonClicked implements OnClickListener
	{
		@Override
		public void onClick(View v) 
		{
			switch (v.getId()) {
			case R.id.buttonCancel_KM:
				finish();
				break;
			case R.id.buttonSave_KM:				
				if(open == 0)
				{
					Toast.makeText(KilometerActivity.this, "Enter the Opening to start :Opening :"+open+" :closing : "+close, Toast.LENGTH_LONG).show();
					return;
				}				
				
				if(close > 0)
				{
					if(open > close)
					{
						Toast.makeText(KilometerActivity.this, "Closing should be greater Opening :"+open+" :closing : "+close, Toast.LENGTH_LONG).show();
						editTextClosing_KM.setText("");
						//closing_Int = 0;
						return;
					}	
					else
					{
						alertBox(1);
					}
				}
				reson = editTextReson_KM.getText().toString();
				if(open>0 && close == 0)
				{
					alertBox(2);
				}
				
				break;
			default:
				break;
			}
		}
		
	}
	private void alertBox(int i){
		
		closeEntered = false;
		AlertDialog.Builder alertbox = new AlertDialog.Builder(KilometerActivity.this);
		if(i == 1)
		{
			closeEntered = true;
			alertbox.setMessage("Saving will not allow you to do farmer entry for the day. \nContinue??");
		}
		else
		{
			closeEntered = false;
			alertbox.setMessage("Save opening for today ??");
		}		
		alertbox.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface arg0, int arg1) {
				
				Toast.makeText(KilometerActivity.this, "Saved Opening :"+open+" :closing : "+close, Toast.LENGTH_LONG).show();
				kilometerHelper.save(route_code,open,close,traveld,reson,da,hasData);
				
				/*if(closeEntered == true)
				{
					oilprice = kilometerHelper.checkForOilPrice();
					int cnoPrice = Integer.parseInt(oilprice[0]);
					int palmRate = Integer.parseInt(oilprice[1]);
					
					if(cnoPrice == 0 && palmRate == 0)
					{
						alertBack();
						return;
					}										
				}*/
				finish();
			}
		});

		alertbox.setNegativeButton("No", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface arg0, int arg1) {
				//opening_Int = 0;
				//closing_Int = 0;
				return;
			}
		});
		alertbox.show();
	}
	/*private void alertBack(){
		AlertDialog.Builder alertbox = new AlertDialog.Builder(KilometerActivity.this);
		alertbox.setMessage("Do you want to put Price for Oil ?? ");
		alertbox.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface arg0, int arg1) {
				
				Intent	intentoil = new Intent(KilometerActivity.this, OilTrackingActivity.class);
				startActivity(intentoil);
			}
		});

		alertbox.setNegativeButton("No", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface arg0, int arg1) {

			}
		});
		alertbox.show();
	}*/
}