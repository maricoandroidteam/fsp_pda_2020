package com.marico.fsp.activity;

import com.marico.fsp.R;
import com.marico.fsp.helper.MainMenuHelper;
import com.marico.fsp.helper.NutsTrackingHelper;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class NutsTrackingActivity extends Activity
{
	private EditText editTextCoconutNT,editTextCopraNT;
	private Button buttonCancel_NT,buttonSave_NT;
	private ButtonClicked buttonClicked;
	private NutsTrackingHelper nutsTrackingHelper;
	float coconut,copra;
	boolean check = false;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lyt_nuts_tracking);
		editTextCoconutNT = (EditText) findViewById(R.id.editTextCoconutNT);
		editTextCopraNT = (EditText) findViewById(R.id.editTextCopraNT);
		
		buttonCancel_NT = (Button) findViewById(R.id.buttonCancel_NT);
		buttonSave_NT = (Button) findViewById(R.id.buttonSave_NT);
		
		Boolean userIsAdmin = false;
		userIsAdmin = new MainMenuHelper(this).userisAdmin(); 
		if(userIsAdmin)
		{
			editTextCoconutNT.setKeyListener(null);
			editTextCoconutNT.setEnabled(false);
			
			editTextCopraNT.setKeyListener(null);
			editTextCopraNT.setEnabled(false);
			
			buttonSave_NT.setVisibility(View.GONE);
			buttonCancel_NT.setText("Back");
			buttonCancel_NT.setCompoundDrawablesWithIntrinsicBounds(R.drawable.back_32, 0, 0, 0);
		}
		
		buttonClicked = new ButtonClicked();
		nutsTrackingHelper = new NutsTrackingHelper(NutsTrackingActivity.this);
		buttonCancel_NT.setOnClickListener(buttonClicked);
		buttonSave_NT.setOnClickListener(buttonClicked);
		String[] strArray = null;
		check = nutsTrackingHelper.chechforTrnNuts();
		if(check == true)
		{
			strArray = nutsTrackingHelper.getTrnNuts_Data();
			coconut = Float.parseFloat(strArray[0]);
			copra = Float.parseFloat(strArray[1]);
			editTextCoconutNT.setText(String.valueOf(coconut));
			editTextCopraNT.setText(String.valueOf(copra));
		}
		else
		{
			editTextCoconutNT.setText("0");
			editTextCopraNT.setText("0");
		}
	}
	
	@Override
	public void onBackPressed() {
	}
	
	private class ButtonClicked implements OnClickListener
	{
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {
			case R.id.buttonCancel_NT:
				finish();
				break;
			case R.id.buttonSave_NT:
				if(!editTextCoconutNT.getText().toString().trim().equals(""))
				{
					coconut = Float.parseFloat(editTextCoconutNT.getText().toString().trim());
				}
				else
					coconut = 0;
				if(!editTextCopraNT.getText().toString().trim().equals(""))
				{
					copra = Float.parseFloat(editTextCopraNT.getText().toString().trim());
				}
				else
					copra = 0;
				nutsTrackingHelper.saveInTrnNutsPrice(coconut,copra,check);
				finish();
				break;
			default:
				break;
			}
		}
	}
}