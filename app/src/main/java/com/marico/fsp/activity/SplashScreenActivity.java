package com.marico.fsp.activity;

import com.marico.fsp.R;
import com.marico.fsp.helper.DatabaseHelper;
import com.marico.fsp.util.CheckForSDCardAvail;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;
import android.widget.Toast;

public class SplashScreenActivity extends Activity{
	private TextView textViewSSVersionName;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lyt_splash_screen);

		if(CheckForSDCardAvail.isSDCardAvailOrNot()==false)
		{
			finish();
			Toast.makeText(getApplicationContext(), "NO SD CARD", Toast.LENGTH_LONG).show();
			return;
		}



		new DatabaseHelper(SplashScreenActivity.this).getUrl();

		textViewSSVersionName = (TextView) findViewById(R.id.textViewSSVersionName);


		String label = getResources().getString(R.string.app_name)+" Version "+getVersionName();
		textViewSSVersionName.setText(label);

		
		
		new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
            	Intent i = new Intent(SplashScreenActivity.this, LoginActivity.class);
        		startActivity(i);
        		finish();
            }
        }, 5000);
	}

	public String getVersionName()
	{
		try {
			return getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
		} catch (Exception e) {
			return "0.0";
		}
	}
}
