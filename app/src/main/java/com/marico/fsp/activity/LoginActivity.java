package com.marico.fsp.activity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;

import com.marico.fsp.R;
import com.marico.fsp.helper.LoginHelper;
import com.marico.fsp.helper.MainMenuHelper;
import com.marico.fsp.util.GetTime;
import com.marico.fsp.util.RequiredPermissions;

public class LoginActivity extends Activity {
	/** Called when the activity is first created. */
	private EditText editTextLoginUserName,editTextLoginPassword;

	private Button buttonLoginLogin,buttonLoginCancel;
	private TextView textViewVersionName;
	private String username = null;
	private String password = null;
	private boolean loginChekServer=true;

	private LoginClick loginClick;
	private LoginHelper loginHelper;
	private boolean isAdmin= false;
	RequiredPermissions requiredPermissions;
	private String tempOldUserName="";
	public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 124;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lyt_login_main);

		requiredPermissions=new RequiredPermissions(this);
		checkPermissions();



		loginHelper = new LoginHelper(getApplicationContext());
		loginHelper.getWritableDatabase();
		//loginHelper.createDatabase();
		buttonLoginLogin = (Button) findViewById(R.id.buttonLoginLogin);
		buttonLoginCancel = (Button) findViewById(R.id.buttonLoginCancel);
		editTextLoginPassword = (EditText) findViewById(R.id.editTextLoginPassword);
		editTextLoginUserName = (EditText) findViewById(R.id.editTextLoginUserName);

		textViewVersionName = (TextView) findViewById(R.id.textViewVersionName);

		loginClick = new LoginClick();
		String label = getResources().getString(R.string.app_name)+" Version "+getVersionName();
		textViewVersionName.setText(label);
	}

	private void checkPermissions() {
		if (requiredPermissions.checkLoginRequestPermissions()) {
			GetTime.getDeviceImei(getApplicationContext());
		}else{
			ActivityCompat.requestPermissions(LoginActivity.this, requiredPermissions.getPermisions().toArray(new String[requiredPermissions.getPermisions().size()]),
					REQUEST_ID_MULTIPLE_PERMISSIONS);
		}
	}


	@Override
	protected void onStart() 
	{
		super.onStart();
		buttonLoginLogin.setOnClickListener(loginClick);
		buttonLoginCancel.setOnClickListener(loginClick);

		tempOldUserName = loginHelper.getUserName();
		if(tempOldUserName != "NA")
		{
			loginChekServer=false;
			editTextLoginUserName.setText(tempOldUserName);
			editTextLoginPassword.requestFocus();
			isAdmin = loginHelper.userisAdmin();
		}
	}
	
	@Override
	public void onBackPressed() {}
	
	public String getVersionName()
	{
		try {
			return getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
		} catch (Exception e) {
			return "0.0";
		}
	}
	private class LoginClick implements OnClickListener
	{
		@Override
		public void onClick(View view) {

			switch (view.getId()) {

			case R.id.buttonLoginCancel:
				finish();
				break;
			case R.id.buttonLoginLogin:
				username = editTextLoginUserName.getText().toString().trim();
				password = editTextLoginPassword.getText().toString().trim();

				if(username=="" || password == "")
				{
					Toast.makeText(getApplicationContext(), "Please Enter UserName And Password", Toast.LENGTH_LONG).show();
				}

				if(!tempOldUserName.equalsIgnoreCase(username))
				{
					MainMenuHelper mainMenuHelper = new MainMenuHelper(LoginActivity.this);
					int i = mainMenuHelper.getCountOfTrnFarm_Data();
					if(i>0 && isAdmin==false)
					{

						AlertDialog.Builder altDialog= new AlertDialog.Builder(LoginActivity.this);
						altDialog.setMessage("Upload Pending For : "+tempOldUserName); // here add your message
						altDialog.setNeutralButton("OK", new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								// TODO Auto-generated method stub
								editTextLoginUserName.requestFocus();
							}
						});
						altDialog.show();

						return;
					}
					loginChekServer=true;

				}
				else
				{
					loginChekServer=false;
				}

				if(loginChekServer)
				{
					if(isInternetOn() ==false )
					{
						Log.i("log", "Internet is : false ");
						Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
						return;
					}
				}
				new LoginCheck().execute("");
				break;
			default:
				break;
			}
		}	
	}

	private class LoginCheck extends AsyncTask<String, String, String>
	{
		ProgressDialog  progressDialog ;

		@Override
		protected void onPreExecute() {
			progressDialog= new ProgressDialog(LoginActivity.this);
			progressDialog.setTitle("Please Wait..");
			progressDialog.setMessage("Loading");
			progressDialog.setCancelable(false);



			if(loginChekServer)
				progressDialog.show();
			super.onPreExecute();
		}
		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub

			if(loginChekServer)
			{
				checkServerLogin();

			}
			return null;
		}		
		@Override
		protected void onPostExecute(String result) {
			if(loginHelper.loginChek(username,password)==1)
			{
				if(progressDialog.isShowing())
					progressDialog.dismiss();

				Toast.makeText(getApplicationContext(), getResources().getString(R.string.toast_msg_log_done), Toast.LENGTH_SHORT).show();
				Intent intent  = new Intent(LoginActivity.this,MainMenuActivity.class);
				startActivity(intent);

				finish();
			}
			else
			{
				if(progressDialog.isShowing())
					progressDialog.dismiss();
				Toast.makeText(getApplicationContext(), getResources().getString(R.string.toast_msg_log_faild), Toast.LENGTH_SHORT).show();

			}

			super.onPostExecute(result);
		}
	}
	private int checkServerLogin() 
	{
		/*String urlString  = "http://219.64.5.67/VietnamPDADev/User.aspx?userid="+username+"&password="+password;*/
		String URL=loginHelper.getUrl();
		Log.i("log", URL);
		String urlString  = URL+"/User.aspx?UserId="+username+"&password="+password;
		
		Log.i("log", "urlString : "+urlString);
		
		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("userId", username));
		nameValuePairs.add(new BasicNameValuePair("password", password));
		//password = "a";
		String result = null;
		InputStream is = null;
		StringBuilder sb=null;
		HttpClient httpClient = new DefaultHttpClient(); 
		HttpPost httpPost = new HttpPost(urlString);
		try {
			//httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse httpResponse = httpClient.execute(httpPost);

			HttpEntity httpEntity = httpResponse.getEntity();
			StatusLine statusLine = httpResponse.getStatusLine();

			int statuscode = statusLine.getStatusCode();
			if(statuscode == 200){
				Log.v("Log", "LoginActivity: StatusCode is 200");
			}
			is = httpEntity.getContent();
			BufferedReader br = null;

			br = new BufferedReader(new InputStreamReader(is,"UTF8"),16);
			sb = new StringBuilder();
			sb.append(br.readLine()+"\n");
			String line = "0";

			while((line = br.readLine()) != null){
				sb.append(line + "\n");
			}
			is.close();

			result = sb.toString().trim();
			Log.v("Log", "LoginActivity: result : "+result);
			if(result.contains("Login Failed"))
			{

				return 0;
			}
			else{
				//Toast.makeText(this, "Login Successful", Toast.LENGTH_LONG).show();
				JSONArray jArray = new  JSONArray(result);
				loginHelper.saveMstUser(jArray);
			}
			return 1;


		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			Log.e("log", e.getMessage(),e.fillInStackTrace());
			return 0;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.e("log", e.getMessage(),e.fillInStackTrace());
			return 0;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			Log.e("log", e.getMessage(),e.fillInStackTrace());
			return 0;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Log.e("log", e.getMessage()+" Reason :saveMstUser()",e.fillInStackTrace());
			return 0;
		}
	}

	public final boolean isInternetOn() {
		ConnectivityManager connec =  (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
		if ( connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED ||
				connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTING ||
				connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTING ||
				connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED ) 
		{
			return true;
		} else if ( connec.getNetworkInfo(0).getState() == NetworkInfo.State.DISCONNECTED ||  
				connec.getNetworkInfo(1).getState() == NetworkInfo.State.DISCONNECTED  ) 
		{
			return false;
		}
		return false;
	}//end of the Internet Connection Checking
}


