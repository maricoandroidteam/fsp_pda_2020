package com.marico.fsp.activity;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.database.SQLException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.marico.fsp.R;
import com.marico.fsp.helper.UploadHelper;
import com.marico.fsp.helper.UserTrnLogHelper;

public class UploadActivity extends Activity
{
	private UploadHelper uploadHelper;
	private ArrayList<String> uploadMsgList;
	private ListView listViewUPUpload;
	private Button buttonUPUpload,buttonUPBack;
	private ProgressBar progressBarUPUpload;
	private CustomListView customListView;

	private UploadDataAsyncTask uploadDataAsyncTask ;
	Boolean uploadCode=false;
	private int closing = 0;
	private int oilPriceAndFlag = 0;
	
	//private int  nutsPriceFlag = 0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lyt_upload);
		buttonIdSetter();
	}

	void buttonIdSetter()
	{
		uploadHelper= new UploadHelper(UploadActivity.this);
		closing = uploadHelper.checkForClosing();
		oilPriceAndFlag = uploadHelper.checkForOilPrice();
		//nutsPriceFlag = uploadHelper.checkForNutsPrice();
		
		uploadMsgList = new ArrayList<String>();
		customListView = new CustomListView();
		ButtonClickListener buttonClickListener = new ButtonClickListener();
		progressBarUPUpload = (ProgressBar) findViewById(R.id.progressBarUPUpload);
		buttonUPUpload = (Button) findViewById(R.id.buttonUPUpload);
		buttonUPBack = (Button) findViewById(R.id.buttonUPBack);
		buttonUPUpload.setOnClickListener(buttonClickListener);
		buttonUPBack.setOnClickListener(buttonClickListener);
		listViewUPUpload = (ListView) findViewById(R.id.listViewUPUpload);
		listViewUPUpload.setAdapter(customListView);
		uploadDataAsyncTask = new UploadDataAsyncTask();
	}
	
	@Override
	public void onBackPressed() {}
	class  CustomListView extends BaseAdapter // Custom base adapter for downloadMsgList
	{
		private TextView tetViewUPStatus;
		@Override
		public int getCount() {
			return uploadMsgList.size();
		}
		@Override
		public Object getItem(int arg0) {
			return uploadMsgList.get(arg0);
		}
		@Override
		public long getItemId(int arg0) {
			return arg0;
		}
		@Override
		public View getView(int arg0, View arg1, ViewGroup arg2) {
			View row=arg1;
			if(row==null)
			{
				LayoutInflater vi = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				row = vi.inflate(R.layout.lyt_upload_listview,null);
			}
			tetViewUPStatus = (TextView)row.findViewById(R.id.tetViewUPStatus);
			tetViewUPStatus.setText(uploadMsgList.get(arg0));
			return row;
		}
	}
	
	class ButtonClickListener implements OnClickListener
	{
		@Override
		public void onClick(View v) 
		{
			switch (v.getId()) {
			case R.id.buttonUPUpload:
				buttonUPUpload.setEnabled(false);
				uploadDataAsyncTask.execute("");
				break;
			case R.id.buttonUPBack:
				if(uploadCode==true)
				{
					finish();
				}
				else
				{
					finish();
				}
				break;
			default:
				break;
			}
		}
	}
	
	
	class UploadDataAsyncTask extends AsyncTask<String, String, String> 
	{
		String URL = uploadHelper.getUrl();
		String urlDetail = URL+"/upload.aspx?TableName=FSP_Farm_Detail_Pda";
		String urlData = URL+"/upload.aspx?TableName=FSP_Farm_Data_pda";
		String urlLocation = URL+"/upload.aspx?TableName=FSP_Farm_Location_Pda";
		String urlKilometer = URL+"/upload.aspx?TableName=FSP_Farm_Travel_Pda";
		String urlNutsPrice = URL+"/upload.aspx?TableName=FSP_Farm_Copra_Price_Pda";
		String urlOilPrice = URL+"/upload.aspx?TableName=FSP_Farm_OilPrice_Pda";
		
		@Override
		protected String doInBackground(String... params) 
		{
			Log.i("log", "urlDetail link"+urlDetail);
			Log.i("log", "urlData link"+urlData);
			Log.i("log", "urlLocation link"+urlLocation);
			int record = -999; // count for total recode
			
			
			//Farm_Data Uploading
			publishProgress("Uploading Started","1");
			publishProgress("Farm_Data Uploading","1");
			try
			{
				record = uploadHelper.sendDataFormFarm_DataTable(urlDetail);
			}
			catch (SQLException e) {
				Log.e("log", e.getMessage(),e.fillInStackTrace());
				return "";
			}
			catch (Exception e) {
				Log.e("log", e.getMessage(),e.fillInStackTrace());
				return "";
			}
			if(record>=-1)
			{
				publishProgress("Farm_Data Uploaded records "+record,"2");
			}
			else
			{
				publishProgress("Farm_Data  Uploaded Error Code "+record,"2");
				publishProgress("Uploading  fail try again ","1");
				return "";
			}
			
			
			//TrnAvg_Data Uploading
			publishProgress("TrnAvg_Data Uploading","1");
			record=uploadHelper.sendDataFormFormTrnAvg_Data(urlData);
			if(record>=-1)
			{
				publishProgress("TrnAvg_Data Uploaded records "+record,"2");
			}
			else
			{
				publishProgress("TrnAvg_Data  Uploaded Error Code "+record,"2");
				publishProgress("Uploading  fail try again ","1");
				return "";
			}
			
			
			// TrnFSPLocation Uploading
			publishProgress("TrnFSPLocation Uploading","1");
			record=uploadHelper.sendDataFromTrnFSPLocation(urlLocation);
			if(record>=-1)
			{
				publishProgress("TrnFSPLocation Uploaded records "+record,"2");
			}
			else
			{
				publishProgress("TrnFSPLocation  Uploaded Error Code "+record,"2");
				publishProgress("Uploading  fail try again ","1");
				return "";
			}
			
			
			if(closing > 0)
			{				
				//TrnKilometer Uploading				
				publishProgress("TrnKilometer Uploading","1");
				record=uploadHelper.sendDataFromTrnKilometer(urlKilometer);
				if(record>=-1)
				{
					publishProgress("TrnKilometer Uploaded records "+record,"2");
				}
				else
				{
					publishProgress("TrnKilometer  Uploaded Error Code "+record,"2");
					publishProgress("Uploading  fail try again ","1");
					return "";
				}
				
				
				// TrnNutsPrice Uploading
				publishProgress("TrnNutsPrice Uploading","1");
				record=uploadHelper.sendDataFromTrnNutsPrice(urlNutsPrice);
				if(record>=-1)
				{
					publishProgress("TrnNutsPrice Uploaded records "+record,"2");
				}
				else
				{
					publishProgress("TrnNutsPrice  Uploaded Error Code "+record,"2");
					publishProgress("Uploading  fail try again ","1");
					return "";
				}

				/*if(oilPrice > 0)
				{
					// TrnOilPrice Uploading
					
					publishProgress("TrnOilPrice Uploading","1");
					record=uploadHelper.sendDataFromTrnOilPrice(urlOilPrice);
					if(record>=-1)
					{
						publishProgress("TrnOilPrice Uploaded records "+record,"2");
					}
					else
					{
						publishProgress("TrnOilPrice  Uploaded Error Code "+record,"2");
						publishProgress("Uploading  fail try again ","1");
						return "";
					}
				}*/
			}
			
			/*if(nutsPriceFlag > 0)
			{
				// TrnNutsPrice Uploading
				publishProgress("TrnNutsPrice Uploading","1");
				record= uploadHelper.sendDataFromTrnNutsPrice(urlNutsPrice);
				if(record>=-1)
				{
					publishProgress("TrnNutsPrice Uploaded records "+record,"2");
				}
				else
				{
					publishProgress("TrnNutsPrice  Uploaded Error Code "+record,"2");
					publishProgress("Uploading  fail try again ","1");
					return "";
				}
			}*/
			
			if(oilPriceAndFlag > 0)
			{
				// TrnOilPrice Uploading
				
				publishProgress("TrnOilPrice Uploading","1");
				record=uploadHelper.sendDataFromTrnOilPrice(urlOilPrice);
				if(record >= -1)
				{
					publishProgress("TrnOilPrice Uploaded records "+record,"2");
				}
				else
				{
					publishProgress("TrnOilPrice Uploaded Error Code "+record,"2");
					publishProgress("Uploading  fail try again ","1");
					return "";
				}
			}
			
			
			// Upload User information on server  in UserTrnLog Table
			String UserInfo_URL = URL + "/upload.aspx?TableName=UserTrnLog";
			UserTrnLogHelper userTrnLogHelper = new UserTrnLogHelper(UploadActivity.this);
			userTrnLogHelper.UserTrnLogUpload("U", UserInfo_URL);
			/*int result = userTrnLogHelper.UserTrnLogUpload("U", UserInfo_URL);
			if(result < 0)
			{
				publishProgress("Error in Uploading  "+result, "1");
				return "";
			}*/
			
			
			publishProgress("Uploading complete","1");
			return null;
		}

		@Override
		protected void onPreExecute() {
			progressBarUPUpload.setVisibility(View.VISIBLE);
			super.onPreExecute();
		}

		@Override
		protected void onPostExecute(String result) 
		{
			super.onPostExecute(result);
			progressBarUPUpload.setVisibility(View.INVISIBLE);
			uploadCode=true;
		}

		@Override
		protected void onProgressUpdate(String... values) {
			super.onProgressUpdate(values);
			if(values[1]=="1")
			{
				uploadMsgList.add(values[0]);
			}
			else
			{
				uploadMsgList.set((uploadMsgList.size()-1),values[0]);

			}

			customListView.notifyDataSetChanged();
		}
	}
}
