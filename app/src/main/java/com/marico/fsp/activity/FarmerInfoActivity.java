package com.marico.fsp.activity;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.marico.fsp.R;
import com.marico.fsp.helper.FarmerInfoHelper;

public class FarmerInfoActivity extends Activity{
	private Button buttonFIBack;
	private Button buttonFISubmit;
	private ButtonClicked buttonClicked;
	private String farmerName,farmerId;
	int Flag =1;
	private LocationManager locationManager;
	private  boolean gps_enabled=false;
	private int treeCount;
	private TextView textViewFIFramCode,textViewFIFramVillage,textViewFIFramBlock,textViewFIFramRoute,textViewFIFramAcre,textViewFIFramTree,textViewTitleFarm;
	private TextView textViewFIFramAddress,textViewFIFramPhone;
	private FarmerInfoHelper helper;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lyt_farmer_info);
		savedInstanceState = getIntent().getExtras();
		farmerId = savedInstanceState.getString("FarmerId");
		farmerName = savedInstanceState.getString("FarmerName");
		if(locationManager == null)
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		buttonFIBack = (Button) findViewById(R.id.buttonFIBack);
		buttonFISubmit = (Button) findViewById(R.id.buttonFISubmit);
		textViewTitleFarm  = (TextView) findViewById(R.id.textViewTitleFarm);
		textViewFIFramCode = (TextView) findViewById(R.id.textViewFIFramCode);
		textViewFIFramVillage = (TextView) findViewById(R.id.textViewFIFramVillage);
		textViewFIFramBlock = (TextView) findViewById(R.id.textViewFIFramBlock);
		textViewFIFramRoute = (TextView) findViewById(R.id.textViewFIFramRoute);
		textViewFIFramAcre = (TextView) findViewById(R.id.textViewFIFramAcre);
		textViewFIFramTree = (TextView) findViewById(R.id.textViewFIFramTree);
		textViewFIFramAddress = (TextView) findViewById(R.id.textViewFIFramAddress);
		textViewFIFramPhone = (TextView) findViewById(R.id.textViewFIFramPhone);
		helper = new FarmerInfoHelper(FarmerInfoActivity.this);
		Object [] obj=helper.getFarmerInfo(farmerId);
		if(helper.entryCount(farmerId)>0)
		{
			Flag=2;
			buttonFISubmit.setText(getResources().getString(R.string.edit));
		}
		textViewTitleFarm.setText(farmerName);  //farmerName
		//textViewFIFramCode.setText((String) obj[0]);  //FramNo
		textViewFIFramCode.setText(farmerId);  //FramNo
		textViewFIFramVillage.setText((String) obj[1]);  //Village
		textViewFIFramAcre.setText((String) obj[2]);  //Acre
		textViewFIFramTree.setText((String) obj[3]);  //Tree
		treeCount = Integer.parseInt((String) obj[3]);
		textViewFIFramAddress.setText((String) obj[4]);  //Address
		textViewFIFramPhone.setText((String) obj[5]);  //Phon
		textViewFIFramBlock.setText((String) obj[6]);  //Block
		textViewFIFramRoute.setText((String) obj[7]);  //Route
		buttonClicked = new ButtonClicked();
	}
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		buttonFIBack.setOnClickListener(buttonClicked);
		buttonFISubmit.setOnClickListener(buttonClicked);
	}	
	class ButtonClicked implements OnClickListener
	{
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {
			case R.id.buttonFIBack:
				finish();
				break;
			case R.id.buttonFISubmit:	
				int closing = helper.getClosingKM();
				if(closing != 0)
				{
					Toast.makeText(FarmerInfoActivity.this, "You cannot do entry for today. \n Closing is already entered.", Toast.LENGTH_LONG).show();
					return;
				}
				try{
					gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
				}
				catch (Exception e) {
					e.printStackTrace();
				}
				Log.i("Log", "GPS IS ENABLED  "+gps_enabled);
				if(gps_enabled)
				{
					Intent goOrderScreenActivity = new Intent(FarmerInfoActivity.this, FSPEntryActivity.class);
					goOrderScreenActivity.putExtra("Flag", Flag);
					goOrderScreenActivity.putExtra("FarmerName", farmerName);
					goOrderScreenActivity.putExtra("FarmerId", farmerId);
					goOrderScreenActivity.putExtra("TreeCount", treeCount);
					startActivityForResult(goOrderScreenActivity, 1003);


				}
				else
				{
					showSettingsAlert();
				}
				break;
			default:
				break;
			}
		}
	}
	public void showSettingsAlert(){
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(FarmerInfoActivity.this);
		alertDialog.setCancelable(false);
		// Setting Dialog Title
		alertDialog.setTitle("GPS not Enabled");
		// Setting Dialog Message
		alertDialog.setMessage("Please Enabled GPS");
		// On pressing Settings button
		alertDialog.setNeutralButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int which) {
				dialog.cancel();
				Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
				startActivity(intent);
			}
		});
		// Showing Alert Message
		alertDialog.show();
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode == 1001)
		{
			setResult(1001);
			finish();
		}
	}
}