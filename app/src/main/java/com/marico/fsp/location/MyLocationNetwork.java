package com.marico.fsp.location;

import java.util.Timer;
import java.util.TimerTask;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

public class MyLocationNetwork {
    Timer timer1;
    LocationManager lm;
    LocationResultNetWork locationResultNetWork;
    boolean network_enabled=false;
    Context mContext;
    public boolean getLocation(Context context, LocationResultNetWork result)
    {
    	mContext = context;
    	locationResultNetWork=result;
        if(lm==null)
            lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        try{network_enabled=lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);}catch(Exception ex){}
        if(network_enabled)
            lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListenerNetwork);
        timer1=new Timer();
        timer1.schedule(new GetLastLocation(), 20000);
        return true;
    }
    LocationListener locationListenerNetwork = new LocationListener() {
        public void onLocationChanged(Location location) {
            timer1.cancel();
            Log.i("Log", "Inside location Listener method");
            if(location != null)
            {
            	Log.i("Log", "onLocationChangerd called");
            	String s = "Latti "+location.getLatitude()+"  Longi  "+location.getLongitude()+"  Accur  "+location.getAccuracy()+"  Provider  "+location.getProvider();
            	Log.i("Log", s);
            	//MainActivity.loctns = location;
            }
            else
            {
            	Log.i("Log", "Null got in onLocationChanged ");
            }
            locationResultNetWork.gotLocationNetwork(location);
           // lm.removeUpdates(this);
        }
        public void onProviderDisabled(String provider) {}
        public void onProviderEnabled(String provider) {}
        public void onStatusChanged(String provider, int status, Bundle extras) {}
    };

    class GetLastLocation extends TimerTask {
        @Override
        public void run() {
             lm.removeUpdates(locationListenerNetwork);

             Location net_loc=null;
             if(network_enabled)
                 net_loc=lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
              if(net_loc!=null){
            	  locationResultNetWork.gotLocationNetwork(net_loc);
                 return;
             }
              locationResultNetWork.gotLocationNetwork(null);
        }
    }

    public static abstract class LocationResultNetWork{
        public abstract void gotLocationNetwork(Location location);
    }
}