package com.marico.fsp.util;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;

import androidx.core.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;

public class RequiredPermissions {
   Context mContext;
    public RequiredPermissions(Context context ) {
        this.mContext=context;

    }


    public boolean checkLoginRequestPermissions() {
        int locationReadPhoneStatus = ContextCompat.checkSelfPermission(mContext, Manifest.permission.READ_PHONE_STATE);
        int permissionWriteExternalStorage = ContextCompat.checkSelfPermission(mContext, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int permissionReadExternalStorage = ContextCompat.checkSelfPermission(mContext, Manifest.permission.READ_EXTERNAL_STORAGE);
        int permissionCaptureCameraImage = ContextCompat.checkSelfPermission(mContext, Manifest.permission.CAMERA);

        List<String> listPermissionsNeeded = new ArrayList<>();

        if (locationReadPhoneStatus != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_PHONE_STATE);
        }

        if (permissionWriteExternalStorage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (permissionReadExternalStorage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (permissionCaptureCameraImage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            setPermisions(listPermissionsNeeded);
            return false;
        }
        return true;
    }
    public boolean checkAndRequestPermissions() {
        int locationMAP = ContextCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION);
        int locationCoarse = ContextCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION);

        List<String> listPermissionsNeeded = new ArrayList<>();

        if (locationMAP != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }   if (locationCoarse != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        }

        if (!listPermissionsNeeded.isEmpty()) {
            setPermisions(listPermissionsNeeded);
            return false;
        }
        return true;
    }


    public boolean checkCall_phonepermission() {
        int permissionCall_phone = ContextCompat.checkSelfPermission(mContext, Manifest.permission.CALL_PHONE);
       List<String> listPermissionsNeeded = new ArrayList<>();

        if (permissionCall_phone != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CALL_PHONE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            setPermisions(listPermissionsNeeded);
            return false;
        }
        return true;
    }
    List<String> mPermisions;
    public void setPermisions(List<String> permisions){
        mPermisions=permisions;
    }
    public List<String> getPermisions(){
        return mPermisions;
    }



    public boolean checkAndRequestCameraPermissions() {
        int permissionWriteExternalStorage = ContextCompat.checkSelfPermission(mContext, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int permissionCaptureCameraImage = ContextCompat.checkSelfPermission(mContext, Manifest.permission.CAMERA);

        List<String> listPermissionsNeeded = new ArrayList<>();

        if (permissionWriteExternalStorage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (permissionCaptureCameraImage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            setPermisions(listPermissionsNeeded);
            return false;
        }
        return true;
    }


}
