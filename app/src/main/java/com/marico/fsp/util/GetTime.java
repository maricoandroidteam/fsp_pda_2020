package com.marico.fsp.util;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.telephony.TelephonyManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class GetTime
{
	static String time;

	public static String Time()
	{
		Calendar c = Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat("dMMHHmmss");
		
		time = df.format(c.getTime());
		return time;
	}
	public static String getCurrentTimeStamp() {
	    SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//dd/MM/yyyy
	    Date now = new Date();
	    String strDate = sdfDate.format(now);
	    return strDate;
	}
	public static String getTime()
	{
		Calendar c = Calendar.getInstance();
		
		SimpleDateFormat df = new SimpleDateFormat("ddMMyyyy");
		time = df.format(c.getTime());
		return time;
	}
	public static String getSalesReturnId() {
	    //SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//dd/MM/yyyy
		SimpleDateFormat sdfDate = new SimpleDateFormat("ddMMyyyy");//dd/MM/yyyy
	    Date now = new Date();
	    String strDate = sdfDate.format(now);
	    System.out.println(strDate);
	    return strDate;
	}


	public static String getDeviceImei(Context context) {
		String deviceid = null;
		if(context==null)
			context=new AppCompatActivity();
		TelephonyManager mTelephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		int permissionCheck = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE);

		if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
			deviceid = mTelephonyManager.getDeviceId();
		}
		return deviceid;
	}
}
