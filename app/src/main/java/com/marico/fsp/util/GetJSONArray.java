package com.marico.fsp.util;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ConnectException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;

import android.util.Log;

public class GetJSONArray 
{
	public static JSONArray getfromURL(String url)
	{
		InputStream is = null;
		String result = "";
		JSONArray jArray = null;
		try{
			HttpPost httppost = new HttpPost(url);
			HttpParams httpParameters = new BasicHttpParams();
			int timeoutConnection = 50000;
			HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
			int timeoutSocket = 50000;
			HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
			HttpClient httpclient = new DefaultHttpClient(httpParameters);
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();
			StatusLine statusLine = response.getStatusLine();
			int statusCode = statusLine.getStatusCode();
			if(statusCode==200)
			{
				Log.i("log", "connection code status true");
			}
		
			is = entity.getContent();
		}catch(Exception e){
			e.printStackTrace();e.fillInStackTrace();
			Log.e("log", "Exception",e.fillInStackTrace());
			return null; // ConnectException
		}
		try{
			BufferedReader reader = new BufferedReader(new InputStreamReader(is,"UTF8"),16);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			result=sb.toString();
			Log.i("log", "result sucess ");
			Log.i("log", result);
		}
		catch (ConnectException e) {
			e.printStackTrace();e.fillInStackTrace();
			Log.e("log", "ConnectException",e.fillInStackTrace());
			return null; // ConnectException
		}
		catch (ConnectTimeoutException e) {
			e.printStackTrace();e.fillInStackTrace();
			Log.e("log", "ConnectTimeoutException");
			return null; //ConnectTimeoutException
		}
		catch (ClientProtocolException e) {
			return null; // ClientProtocolException
		}
		catch (IOException e) {
			return null; // IOException
		}			
		
		try
		{
			jArray = new JSONArray(result);
			//JSONObject joJsonObject = new JSONObject(result);
		}catch(JSONException e)
		{
			Log.e("log", "Error parsing data "+e.toString(),e.fillInStackTrace());
		}
		return jArray;
	}
}