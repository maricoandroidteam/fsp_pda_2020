package com.marico.fsp.util;


public class CheckForNullValues {
	//METHOD for Checking Null Values
	public static boolean checkNullValues(String valueToCheck){
		//Log.i("Log","CheckForNullValues : "+valueToCheck);
		if(!(valueToCheck == null)){
			String valueCheck = valueToCheck.trim();
			if(valueCheck.equals("") || valueCheck.equals("0")  ){
			//	Log.i("Log","Returning false 0 or Blank");
				return false;
			}
			return true;
		}
		//Log.i("Log","Returning false null");
		return false;
	}// End of method checkNullValues
	
	public static boolean checkNullValuesALL(String valueToCheck){
		//Log.i("Log","CheckForNullValues : "+valueToCheck);
		if(!(valueToCheck == null)){
			String valueCheck = valueToCheck.trim();
			if(valueCheck.equals("") || valueCheck.equals("0")|| valueCheck.equals("00")|| valueCheck.equals("000")|| valueCheck.equals("0000")|| valueCheck.equals("00000")  ){
			//	Log.i("Log","Returning false 0 or Blank");
				return false;
			}
			return true;
		}
		//Log.i("Log","Returning false null");
		return false;
	}// End of method checkNullValues
	
	public static boolean checkNullValuesWithoutZero(String valueToCheck){
	//	Log.i("Log","CheckForNullValues : "+valueToCheck);
		if(!(valueToCheck == null)){
			String valueCheck = valueToCheck.trim();
			if(valueCheck.equals("")){
				//Log.i("Log","Returning false 0 or Blank");
				return false;
			}
			return true;
		}
		//Log.i("Log","Returning false null");
		return false;
	}// End of method checkNullValues
}
