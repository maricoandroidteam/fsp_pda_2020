package com.marico.fsp.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ConnectException;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import android.util.Log;

public class UploadToServer 
{

	public  int uploadToServer(ArrayList<NameValuePair> arrlstNameValuePairs,String url)
	{
		InputStream is = null;
		StringBuilder sb = null;
		HttpClient httpclient=null;
		HttpPost httppost = null;
		try{
			httppost = new HttpPost(url);
			httppost.setEntity(new UrlEncodedFormEntity(arrlstNameValuePairs));
			HttpParams httpParameters = new BasicHttpParams();
			int timeoutConnection = 50000;
			HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
			int timeoutSocket = 50000;
			HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
			httpclient = new DefaultHttpClient(httpParameters);
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();
			StatusLine statusline = response.getStatusLine();
			int statuscode = statusline.getStatusCode();

			if(statuscode == 200)
			{
				Log.i("log","**Data is send On the Server***"+statuscode);
			}else{
				Log.i("log","**Data Sending Error***"+statuscode);
				return -500;
			}
			is = entity.getContent();
			BufferedReader reader = new BufferedReader(new InputStreamReader(is,"UTF8"),16);
			sb = new StringBuilder();
			sb.append(reader.readLine() + "\n");
			String line="0";
			while((line = reader.readLine()) != null)
			{
				sb.append(line + "\n");
			}
			is.close();
			String result = sb.toString().trim();

			Log.i("log","resultn is "+result);

			if(result.contains("Success")||result=="Success")
			{
				return 1;
			}
			else
			{
				return -20;
			}

		}
		catch (ConnectException e) {
			e.printStackTrace();e.fillInStackTrace();
			Log.i("log", "ConnectException");
			return -1; // ConnectException
		}
		catch (ConnectTimeoutException e) {
			e.printStackTrace();e.fillInStackTrace();
			Log.i("log", "ConnectTimeoutException");
			return -2; //ConnectTimeoutException
		}
		catch (ClientProtocolException e) {
			e.printStackTrace();e.fillInStackTrace();
			return -3; // ClientProtocolException
		} 
		catch (IOException e) {
			e.printStackTrace();e.fillInStackTrace();
			return -4; // IOException
		}
		catch (Exception e) {
			e.printStackTrace();e.fillInStackTrace();
			return -5; // IOException
		}
	}


}

