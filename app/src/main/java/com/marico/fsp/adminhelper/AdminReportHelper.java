package com.marico.fsp.adminhelper;

import java.util.ArrayList;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.marico.fsp.helper.DatabaseHelper;

public class AdminReportHelper extends DatabaseHelper
{

	public AdminReportHelper(Context context) {
		super(context);

	}


	public Object[] getRouteInfo() {
		// TODO Auto-generated method stub 
		db = this.getReadableDatabase();
		Cursor cursor = null;
		ArrayList<String> route;

		route = new ArrayList<String>();
		route.add(0, "All");
		cursor = db.rawQuery("Select distinct cast(ROUTE_CODE AS INT) as ROUTE_CODE from MstFarm", null);
		Log.i("Log", "getDsrNameAndDsrId is called");		
		cursor.moveToFirst();
		while(!cursor.isAfterLast())
		{
			route.add(cursor.getString(cursor.getColumnIndex("ROUTE_CODE")));

			cursor.moveToNext();
		}
		cursor.close();
		db.close();

		String [] routearry = route.toArray(new String[route.size()]);

		return new Object[]{routearry};
	}




	public Object[] getFarmerNameFromMstFarm(String routeid) {
		// TODO Auto-generated method stub
		db = this.getReadableDatabase();
		Cursor cursor = null;		
		ArrayList<String> arrayListFarmerId,arrayListFarmerName;
		arrayListFarmerId = new ArrayList<String>();
		arrayListFarmerName = new ArrayList<String>();



		if(routeid.equalsIgnoreCase("All"))
		{
			cursor= db.rawQuery("select distinct A.FARM_CODE,A.FARMER_NAME from mstFarm as A  order by FARMER_NAME collate NoCase ", null);
		}
		else
		{
			cursor= db.rawQuery("select distinct A.FARM_CODE,A.FARMER_NAME from mstFarm as A    where  A.ROUTE_CODE= ? order by FARMER_NAME collate NoCase   ", new String[]{routeid});

		}
		cursor.moveToFirst();
		while(!cursor.isAfterLast())
		{
			arrayListFarmerId.add(cursor.getString(cursor.getColumnIndex("FARM_CODE")));
			arrayListFarmerName.add(cursor.getString(cursor.getColumnIndex("FARMER_NAME")));

			cursor.moveToNext();			
		}
		cursor.close();
		db.close();


		String [] FarmerIDarry = arrayListFarmerId.toArray(new String[arrayListFarmerId.size()]);
		String [] FarmerNamearry = arrayListFarmerName.toArray(new String[arrayListFarmerName.size()]);

		return new Object[]{FarmerIDarry,FarmerNamearry};
	}



	public Object[] getReportDate(String FarmId)
	{
		db=this.getWritableDatabase();
		Cursor cursor=null;

		cursor= db.rawQuery("select MaturedBunches AS MB ,MaturedNuts AS MN , LastTwoBunches AS LB, ButtonNuts AS BN , NutsStock AS NS from TrnAvg_data where FarmCode=?", new String[]{FarmId});
		ArrayList<String> arrayTitle= new  ArrayList<String>();
		ArrayList<String>  arrayMB= new  ArrayList<String>();
		ArrayList<String>  arrayMN=  new  ArrayList<String>();
		ArrayList<String>  arrayLB=  new  ArrayList<String>();
		ArrayList<String>  arrayBN=  new  ArrayList<String>();
		ArrayList<String>  arrayNS=  new  ArrayList<String>();

		arrayTitle.add("Curr.Month");

		if(cursor.getCount()!=0)
		{
			cursor.moveToFirst();

			arrayMB.add(cursor.getString(cursor.getColumnIndex("MB")));
			arrayMN.add(cursor.getString(cursor.getColumnIndex("MN")));
			arrayLB.add(cursor.getString(cursor.getColumnIndex("LB")));
			arrayBN.add(cursor.getString(cursor.getColumnIndex("BN")));
			arrayNS.add(cursor.getString(cursor.getColumnIndex("NS")));

		}
		else
		{
			arrayMB.add("-");
			arrayMN.add("-");
			arrayLB.add("-");
			arrayBN.add("-");
			arrayNS.add("-");

		}


		cursor.close();


		cursor= db.rawQuery("select Matured_Bunches AS MB ,Matured_Nuts AS MN , L2B AS LB, Button_Nuts AS BN , Nut_Stock AS NS from MstAdminReport where Farm_Code=? Order By Entry_Date",new String[]{FarmId});

		arrayTitle.add("Prev.Month");

		if(cursor.moveToNext())
		{
			arrayMB.add(cursor.getString(cursor.getColumnIndex("MB")));
			arrayMN.add(cursor.getString(cursor.getColumnIndex("MN")));
			arrayLB.add(cursor.getString(cursor.getColumnIndex("LB")));
			arrayBN.add(cursor.getString(cursor.getColumnIndex("BN")));
			arrayNS.add(cursor.getString(cursor.getColumnIndex("NS")));

		}
		else
		{
			arrayMB.add("-");
			arrayMN.add("-");
			arrayLB.add("-");
			arrayBN.add("-");
			arrayNS.add("-");

		}



		arrayTitle.add("Prev.Year");

		if(cursor.moveToNext())
		{


			arrayMB.add(cursor.getString(cursor.getColumnIndex("MB")));
			arrayMN.add(cursor.getString(cursor.getColumnIndex("MN")));
			arrayLB.add(cursor.getString(cursor.getColumnIndex("LB")));
			arrayBN.add(cursor.getString(cursor.getColumnIndex("BN")));
			arrayNS.add(cursor.getString(cursor.getColumnIndex("NS")));

		}
		else
		{
			arrayMB.add("-");
			arrayMN.add("-");
			arrayLB.add("-");
			arrayBN.add("-");
			arrayNS.add("-");

		}

		cursor.close();
		db.close();

		String [] Titlearry = arrayTitle.toArray(new String[arrayTitle.size()]);
		String [] MBarry = arrayMB.toArray(new String[arrayMB.size()]);
		String [] MNarry = arrayMN.toArray(new String[arrayMN.size()]);
		String [] LBarry = arrayLB.toArray(new String[arrayLB.size()]);
		String [] BNarry = arrayBN.toArray(new String[arrayBN.size()]);
		String [] NSarry = arrayNS.toArray(new String[arrayNS.size()]);


		return new Object[]{Titlearry,MBarry,MNarry,LBarry,BNarry,NSarry};
	}


	public Object[] getProductivityData(String routeid) {
		// TODO Auto-generated method stub
		db = this.getReadableDatabase();
		Cursor cursor = null;		
		ArrayList<String> arrayListTree,arrayListFarmerName,arrayListTotal,arrayListProductivity;
		arrayListTree = new ArrayList<String>();
		arrayListFarmerName = new ArrayList<String>();
		arrayListTotal = new ArrayList<String>();
		arrayListProductivity = new ArrayList<String>();

		if(routeid.equalsIgnoreCase("All"))
		{
			cursor= db.rawQuery("SELECT A.Farm_Code AS Farmerid,B.FARMER_NAME AS FarmerName,B.NO_OF_TREES AS Tree,sum(Number_Of_nuts) AS Total, ROUND((sum(Number_Of_nuts)/" +
					" CASE when  (max(12*lt_plucking_year+lt_plucking_month)-min(12*lt_plucking_year+lt_plucking_month))=0 THEN 1 " +
					" ELSE   (max(12*lt_plucking_year+lt_plucking_month)-min(12*lt_plucking_year+lt_plucking_month)) END)/B.NO_OF_TREES||'.',0) AS  Productivity, " +
					" CASE when  (max(12*lt_plucking_year+lt_plucking_month)-min(12*lt_plucking_year+lt_plucking_month))=0 THEN 1" +
					" ELSE   (max(12*lt_plucking_year+lt_plucking_month)-min(12*lt_plucking_year+lt_plucking_month)) END AS Month" +
					" FROM Vw_mstProductivity AS A" +
					" inner join MstFarm AS B on A.Farm_Code=B.Farm_Code " +
					"GROUP BY A.Farm_Code",null);
									
		}
		else
		{

			cursor= db.rawQuery("SELECT A.Farm_Code AS Farmerid,B.FARMER_NAME AS FarmerName,B.NO_OF_TREES AS Tree,sum(Number_Of_nuts) AS Total, ROUND((sum(Number_Of_nuts)/" +
					" CASE when  (max(12*lt_plucking_year+lt_plucking_month)-min(12*lt_plucking_year+lt_plucking_month))=0 THEN 1 " +
					" ELSE   (max(12*lt_plucking_year+lt_plucking_month)-min(12*lt_plucking_year+lt_plucking_month)) END)/B.NO_OF_TREES||'.',0) AS  Productivity, " +
					" CASE when  (max(12*lt_plucking_year+lt_plucking_month)-min(12*lt_plucking_year+lt_plucking_month))=0 THEN 1" +
					" ELSE   (max(12*lt_plucking_year+lt_plucking_month)-min(12*lt_plucking_year+lt_plucking_month)) END AS Month" +
					" FROM Vw_mstProductivity AS A" +
					" inner join MstFarm AS B on A.Farm_Code=B.Farm_Code " +
					" where B.ROUTE_CODE=? " +
					" GROUP BY A.Farm_Code", new String[]{routeid});
		}

		while(cursor.moveToNext())
		{
			
			arrayListFarmerName.add(cursor.getString(cursor.getColumnIndex("FarmerName")));
			arrayListTotal.add(cursor.getString(cursor.getColumnIndex("Total")));
			arrayListTree.add(cursor.getString(cursor.getColumnIndex("Tree")));
			arrayListProductivity.add(cursor.getString(cursor.getColumnIndex("Productivity")));

		}
		cursor.close();
		db.close();

		String [] Treearry = arrayListTree.toArray(new String[arrayListTree.size()]);
		String [] FarmerNamearry = arrayListFarmerName.toArray(new String[arrayListFarmerName.size()]);
		String [] Totalarry = arrayListTotal.toArray(new String[arrayListTotal.size()]);
		String [] Productivityarry = arrayListProductivity.toArray(new String[arrayListProductivity.size()]);

		return new Object[]{FarmerNamearry,Totalarry,Treearry,Productivityarry};
	}
}
