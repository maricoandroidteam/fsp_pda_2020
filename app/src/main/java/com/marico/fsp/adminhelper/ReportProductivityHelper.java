package com.marico.fsp.adminhelper;

import java.util.ArrayList;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.marico.fsp.helper.DatabaseHelper;

public class ReportProductivityHelper extends DatabaseHelper
{

	public ReportProductivityHelper(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}
	
	public Object[] getRouteInfo() {
		// TODO Auto-generated method stub 
		db = this.getReadableDatabase();
		Cursor cursor = null;
		ArrayList<String> route;

		route = new ArrayList<String>();
		//route.add(0, "--Select--");
		cursor = db.rawQuery("Select distinct cast(ROUTE_CODE AS INT) as ROUTE_CODE from MstFarm", null);
		Log.i("Log", "getDsrNameAndDsrId is called");		
		cursor.moveToFirst();
		while(!cursor.isAfterLast())
		{
			route.add(cursor.getString(cursor.getColumnIndex("ROUTE_CODE")));

			cursor.moveToNext();
		}
		cursor.close();
		db.close();

		String [] routearry = route.toArray(new String[route.size()]);

		return new Object[]{routearry};
	}
	
	public Object[] getFarmCodeAndName(String RouteCode)
	{
		db = this.getWritableDatabase();
		Cursor cursor = null;
		
		ArrayList<String>  arrayCode = new  ArrayList<String>();
		ArrayList<String> arrayName= new  ArrayList<String>();
		String Query = "SELECT  F.FARM_CODE, F.FARMER_NAME FROM MstFarm F "+
						"INNER JOIN MstProductivity P ON P.Farm_Code = F.FARM_CODE WHERE ROUTE_CODE ="+RouteCode+" "+
						"GROUP BY P.Farm_Code";
		
		cursor = db.rawQuery(Query, null);
		
		while(cursor.moveToNext())
		{
			arrayCode.add(cursor.getString(cursor.getColumnIndex("FARM_CODE")));
			arrayName.add(cursor.getString(cursor.getColumnIndex("FARMER_NAME")));
		}
		
		cursor.close();
		db.close();
		
		String [] FarmCodearry = arrayCode.toArray(new String[arrayCode.size()]);
		String [] FarmNamearry = arrayName.toArray(new String[arrayName.size()]);
		
		return new Object[]{FarmCodearry, FarmNamearry};
	}
	
	public Object[] getProductivityByFarmCode(String FarmCode)
	{
		db=this.getWritableDatabase();				
		Cursor cursor=null;
		
			String Q_CurHar = "SELECT P.Farm_Code,  P.Lt_Plucking_Month, P.Number_Of_Nuts, F.NO_OF_TREES, "+
					"round((cast(P.Number_Of_Nuts AS float)/ F.NO_OF_TREES), 2) AS Productivity, "+ 
					"Lt_Plucking_Year || '-' || CASE WHEN length(Lt_Plucking_Month)=1 THEN '0'||Lt_Plucking_Month "+  
					"WHEN length(Lt_Plucking_Month)=2  THEN Lt_Plucking_Month END || '-01'  AS Dt "+
					"FROM MstProductivity P, MstFarm F "+
					"WHERE F.Farm_Code = P.Farm_Code AND P.Farm_Code='"+FarmCode+"' "+
					"ORDER BY Dt DESC LIMIT 1";
			
					cursor= db.rawQuery(Q_CurHar, null);
			
					ArrayList<String> arrayTitle= new  ArrayList<String>();
					ArrayList<String>  arrayMonth= new  ArrayList<String>();
					ArrayList<String>  arrayNutsHar =  new  ArrayList<String>();
					ArrayList<String>  arrayNoOfTrees =  new  ArrayList<String>();
					ArrayList<String>  arrayProd =  new  ArrayList<String>();
					
					arrayTitle.add("Current Harvest");
					
					if(cursor.getCount()!=0)
					{
						cursor.moveToFirst();
		
						arrayMonth.add(cursor.getString(cursor.getColumnIndex("Lt_Plucking_Month")));
						arrayNutsHar.add(cursor.getString(cursor.getColumnIndex("Number_Of_Nuts")));
						arrayNoOfTrees.add(cursor.getString(cursor.getColumnIndex("NO_OF_TREES")));
						arrayProd.add(cursor.getString(cursor.getColumnIndex("Productivity")));
		
					}
					else
					{
						arrayMonth.add("-");
						arrayNutsHar.add("-");
						arrayNoOfTrees.add("-");
						arrayProd.add("-");
					}
					
			cursor.close();
			
			String Q_PrevHar = "SELECT P.Farm_Code,  P.Lt_Plucking_Month, P.Number_Of_Nuts, F.NO_OF_TREES, "+
					"round((cast(P.Number_Of_Nuts AS float)/ F.NO_OF_TREES), 2) AS Productivity, "+ 
					"Lt_Plucking_Year || '-' || CASE WHEN length(Lt_Plucking_Month)=1 THEN '0'||Lt_Plucking_Month "+  
					"WHEN length(Lt_Plucking_Month)=2  THEN Lt_Plucking_Month END || '-01'  AS Dt "+
					"FROM MstProductivity P, MstFarm F "+
					"WHERE F.Farm_Code = P.Farm_Code AND P.Farm_Code='"+FarmCode+"' "+
					"ORDER BY Dt ASC LIMIT 1";
			
					cursor= db.rawQuery(Q_PrevHar, null);
			
					
					arrayTitle.add("Previous Harvest");
					
					if(cursor.getCount()!=0)
					{
						cursor.moveToFirst();

						arrayMonth.add(cursor.getString(cursor.getColumnIndex("Lt_Plucking_Month")));
						arrayNutsHar.add(cursor.getString(cursor.getColumnIndex("Number_Of_Nuts")));
						arrayNoOfTrees.add(cursor.getString(cursor.getColumnIndex("NO_OF_TREES")));
						arrayProd.add(cursor.getString(cursor.getColumnIndex("Productivity")));

					}
					else
					{
						arrayMonth.add("-");
						arrayNutsHar.add("-");
						arrayNoOfTrees.add("-");
						arrayProd.add("-");
					}
			cursor.close();
			
			String Q_ExptHarv = "SELECT P.Farm_Code,  P.Next_Plucking_Month, P.Exp_Nuts, F.NO_OF_TREES, "+
					"round((cast(P.Exp_Nuts AS float)/ F.NO_OF_TREES), 2) AS Productivity, "+
					"Next_Plucking_Year || '-' ||   CASE   WHEN length(Next_Plucking_Month)=1 THEN '0'||Next_Plucking_Month "+
					"WHEN length(Next_Plucking_Month)=2  THEN Next_Plucking_Month END || '-01'  AS Dt "+
					"FROM MstProductivity P, MstFarm F "+
					"WHERE F.Farm_Code = P.Farm_Code AND P.Farm_Code='"+FarmCode+"'"+
					"ORDER BY Dt DESC LIMIT 1 ";
			
					cursor= db.rawQuery(Q_ExptHarv, null);
			arrayTitle.add("Expected Harvest");
			
			if(cursor.getCount()!=0)
			{
				cursor.moveToFirst();

				arrayMonth.add(cursor.getString(cursor.getColumnIndex("Next_Plucking_Month")));
				arrayNutsHar.add(cursor.getString(cursor.getColumnIndex("Exp_Nuts")));
				arrayNoOfTrees.add(cursor.getString(cursor.getColumnIndex("NO_OF_TREES")));
				arrayProd.add(cursor.getString(cursor.getColumnIndex("Productivity")));

			}
			else
			{
				arrayMonth.add("-");
				arrayNutsHar.add("-");
				arrayNoOfTrees.add("-");
				arrayProd.add("-");
			}

			cursor.close();
			db.close();
	
			String [] Titlearry = arrayTitle.toArray(new String[arrayTitle.size()]);
			String [] Montharry = arrayMonth.toArray(new String[arrayMonth.size()]);
			String [] NutsHararry = arrayNutsHar.toArray(new String[arrayNutsHar.size()]);
			String [] NoOfTreesarry = arrayNoOfTrees.toArray(new String[arrayNoOfTrees.size()]);
			String [] Prodarry = arrayProd.toArray(new String[arrayProd.size()]);
			
			return new Object[]{Titlearry, Montharry, NutsHararry, NoOfTreesarry, Prodarry};
		
	}

}
