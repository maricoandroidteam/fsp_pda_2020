package com.marico.fsp.adminhelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.marico.fsp.helper.DatabaseHelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

public class AdminDownloadHelper extends DatabaseHelper
{

	DatabaseHelper databaseHelper;
	Context context;
	public AdminDownloadHelper(Context context)
	{
		super(context);
		this.context=context;


	}
	public int inserIntoMstUserFromDownload(JSONArray jsonArray)
	{
		// this method call when MstUser table download

		db=this.getWritableDatabase();

		ContentValues cv = new ContentValues();

		int count=0;
		try{
			JSONObject jb;
			for(int i=0;i<jsonArray.length();i++)
			{
				jb = jsonArray.getJSONObject(i);

				cv.put("DsrId", jb.getString("DsrId"));
				cv.put("DistId", jb.getString("DistId"));
				cv.put("Password",jb.getString("Password"));
				db.insert("MstUser", null,cv);
				count++;
				cv.clear();
			}
		}
		catch (Exception e) {
			count=-101;
			Log.e("log", e.getMessage(),e.fillInStackTrace());
		}
		db.close();
		return count;
	}



	public int inserIntoMstFarmFromDownload(JSONArray jsonArray)
	{
		// this method call when MstItem table download
		db=this.getWritableDatabase();
		Log.i("log",""+db);
		ContentValues cv = new ContentValues();

		int count=0;
		try{
			JSONObject jb;
			for(int i=0;i<jsonArray.length();i++)
			{
				jb = jsonArray.getJSONObject(i);
				cv.put("FARM_CODE",jb.getString("FARM_CODE").trim());
				cv.put("BLOCK_CODE",jb.getString("BLOCK_CODE"));
				cv.put("BLOCK_NAME",jb.getString("BLOCK_NAME"));
				cv.put("ROUTE_CODE",jb.getString("ROUTE_CODE"));
				cv.put("FARM_NO", jb.getString("FARM_NO"));
				cv.put("FARMER_NAME",jb.getString("FARMER_NAME"));
				cv.put("ADDRESS",jb.getString("ADDRESS"));
				cv.put("VILLAGE",jb.getString("VILLAGE"));
				cv.put("PHONE",jb.getString("PHONE"));
				cv.put("NO_OF_TREES", jb.getString("NO_OF_TREES"));
				cv.put("ACRE", jb.getString("ACRE"));
				cv.put("NO_OF_SAMPLE_TREES", jb.getString("NO_OF_SAMPLE_TREES"));
				db.insert("MstFarm", null,cv);
				count++;
				cv.clear();
			}
			return count;
		}
		catch (Exception e) {
			count=-101;
			Log.e("log", e.getMessage(),e.fillInStackTrace());
			return count;
		}
		finally
		{
			db.close();
		}
	}
	
	

	public int inserIntoMstAdminReport(JSONArray jsonArray)
	{
		// this method call when MstItem table download
		db=this.getWritableDatabase();
		Log.i("log",""+db);
		ContentValues cv = new ContentValues();

		int count=0;
		try{
			JSONObject jb;
			for(int i=0;i<jsonArray.length();i++)
			{
				jb = jsonArray.getJSONObject(i);
				//cv.put("Entry_date",jb.getString("Entry_Date"));
				cv.put("Entry_date",jb.getString("Entry_date"));
				cv.put("Matured_Nuts",jb.getString("Matured_Nuts"));
				cv.put("Matured_Bunches",jb.getString("Matured_Bunches"));

				cv.put("Button_Nuts",jb.getString("Button_Nuts"));
				cv.put("L2B", jb.getString("L2B"));
				
				cv.put("Nut_Stock",jb.getString("Nut_Stock"));

				cv.put("FSP_ID",jb.getString("FSP_ID"));
				cv.put("Farm_Code",jb.getString("Farm_Code").trim());
				cv.put("Tdate",jb.getString("Tdate"));


				db.insert("MstAdminReport", null,cv);
				count++;
				cv.clear();
			}
			return count;
		}
		catch (Exception e) {
			count=-101;
			Log.e("log", e.getMessage(),e.fillInStackTrace());
			return count;
		}
		finally
		{
			db.close();
		}
	}



	public int inserIntoMstproductivitt(JSONArray jsonArray) throws JSONException 
	{
		// this method call when MstItem table download
		db=this.getWritableDatabase();
		Log.i("log",""+db);
		ContentValues cv = new ContentValues();

		int count=0;
		try{
			JSONObject jb;
			for(int i=0;i<jsonArray.length();i++)
			{

				jb = jsonArray.getJSONObject(i);
				cv.put("Farm_Code",jb.getString("Farm_Code").trim());
				cv.put("Number_Of_Nuts",jb.getString("Number_Of_Nuts"));
				cv.put("Lt_Plucking_Month",jb.getString("Lt_Plucking_Month"));
				cv.put("Lt_Plucking_Year",jb.getString("Lt_Plucking_Year"));
				
				cv.put("Exp_Nuts", jb.getString("Exp_Nuts"));
				cv.put("Next_Plucking_Month", jb.getString("Next_Plucking_Month"));
				cv.put("Next_Plucking_Year", jb.getString("Next_Plucking_Year"));
				
				db.insert("Mstproductivity", null,cv);
				count++;
				cv.clear();				
			}
			//db.close();
			return count;
		}
		catch (Exception e) {
			count=-101;
			Log.e("log", e.getMessage(),e.fillInStackTrace());
			return count;
		}
		finally
		{
			db.close();
		}
			
	}
	public int inserIntoTrnAvg_DataFromDownload(JSONArray jsonArray)
	{
		// this method call when MstItem table download
		db=this.getWritableDatabase();
		Log.i("log",""+db);
		ContentValues cv = new ContentValues();

		int count=0;
		try{
			JSONObject jb;
			for(int i=0;i<jsonArray.length();i++)
			{
				jb = jsonArray.getJSONObject(i);

				cv.put("FarmCode",jb.getString("Farm_Code").trim());
				cv.put("Round",jb.getString("Round"));
				cv.put("CurrentDate",jb.getString("Entry_Date"));
				cv.put("FSPId",jb.getString("FSP_ID"));
				cv.put("MaturedBunches", jb.getString("Matured_Bunches"));
				cv.put("MaturedNuts",jb.getString("Matured_Nuts"));
				cv.put("ButtonBunches",jb.getString("Button_Bunches"));
				cv.put("ButtonNuts",jb.getString("Button_Nuts"));
				cv.put("Harvested",jb.getString("Harvested"));
				cv.put("LastpluckingMonth", jb.getString("Lt_Plucking_Month"));
				cv.put("LastpluckingYear", jb.getString("Lt_Plucking_Year"));
				cv.put("NumberOfNuts", jb.getString("Number_Of_Nuts"));
				cv.put("NutsStock",jb.getString("Nut_Stock"));
				cv.put("NextpluckingMonth",jb.getString("Next_Plucking_Month"));
				cv.put("NextpluckingYear", jb.getString("Next_Plucking_Year"));
				cv.put("ExpectedNuts",jb.getString("Exp_Nuts"));
				cv.put("OutLookOfTree",jb.getString("Outlook"));
				cv.put("TDate",jb.getString("TDate"));
				
				cv.put("LastTwoBunches",jb.getString("Last_2_Bunches"));
				//cv.put("UploadFlag",jb.getString("TDate"));
				
				cv.put("SecondLastpluckingMonth",jb.getString("SecondLastpluckingMonth"));
				cv.put("SecondLastpluckingYear",jb.getString("SecondLastpluckingYear"));
				cv.put("Fertilizer",jb.getString("Fertilizer"));
				
				
				

				cv.put("UploadFlag","N");


				db.insert("TrnAvg_Data", null,cv);
				count++;
				cv.clear();
			}
			return count;
		}
		catch (Exception e) {
			count=-101;
			Log.e("log", e.getMessage(),e.fillInStackTrace());
			return count;
		}
		finally
		{
			db.close();
		}
	}



	public int inserIntoFarm_DataFromDownload(JSONArray jsonArray)
	{
		// this method call when MstItem table download
		db=this.getWritableDatabase();
		Log.i("log",""+db);
		ContentValues cv = new ContentValues();

		int count=0;
		try{
			JSONObject jb;
			for(int i=0;i<jsonArray.length();i++)
			{
				jb = jsonArray.getJSONObject(i);
				cv.put("TreeName",jb.getString("TreeName").trim());
				cv.put("CurrentDate",jb.getString("Entry_Date"));
				cv.put("FarmCode",jb.getString("Farm_Code"));
				cv.put("FSPId",jb.getString("FSP_ID"));
				cv.put("MaturedBunches", jb.getString("Matured_Bunches"));
				cv.put("MaturedNuts",jb.getString("Matured_Nuts"));
				cv.put("ButtonBunches",jb.getString("Button_Bunches"));
				cv.put("ButtonNuts",jb.getString("Button_Nuts"));
				cv.put("LastTwoBunches",jb.getString("Last_2_Bunches"));
				//cv.put("UploadFlag",jb.getString("UploadFlag"));
				cv.put("UploadFlag","N");



				db.insert("Farm_Data", null,cv);
				count++;
				cv.clear();
			}
			return count;
		}
		catch (Exception e) {
			count=-101;
			Log.e("log", e.getMessage(),e.fillInStackTrace());
			return count;
		}
		finally
		{
			db.close();
		}
	}



	public int inserIntoMstRouteFromDownload(JSONArray jsonArray)
	{
		// this method call when MstItem table download
		db=this.getWritableDatabase();
		Log.i("log",""+db);
		ContentValues cv = new ContentValues();

		int count=0;
		try{
			JSONObject jb;
			for(int i=0;i<jsonArray.length();i++)
			{
				jb = jsonArray.getJSONObject(i);
				cv.put("ROUTE_CODE",jb.getString("ROUTE_CODE"));
				cv.put("BLOCK_CODE",jb.getString("BLOCK_CODE"));
				cv.put("BLOCK_NAME",jb.getString("BLOCK_NAME"));
				db.insert("MstRoute", null,cv);
				count++;
				cv.clear();
			}
			return count;
		}
		catch (Exception e) {
			count=-101;
			Log.e("log", e.getMessage(),e.fillInStackTrace());
			return count;
		}
		finally
		{
			db.close();
		}
	}


	public void deleteALLtableForDownload()
	{
		db=this.getWritableDatabase();
		db.execSQL("delete from MstFarm");
		db.execSQL("delete from MstRoute");
		db.execSQL("delete from Farm_Data");
		db.execSQL("delete from Farm_Data_Temp");
		db.execSQL("delete from MstUserAllList");
		db.execSQL("delete from TrnAvg_Data");
		db.execSQL("delete from MstAdminReport");
		db.execSQL("delete from Mstproductivity");



		db.close();
	}

	public String getUserId()
	{
		Cursor cursor;
		String distId = null;
		db=this.getWritableDatabase();

		cursor= db.rawQuery("select User_ID from MstUser", null);
		if(cursor.getCount()==1)
		{
			while(cursor.moveToNext())
			{
				distId= cursor.getString(cursor.getColumnIndex("User_ID"));
			}
		}
		cursor.close();
		db.close();
		return distId;
	}

	public String getLOC_CODE()
	{
		Cursor cursor;
		String distId = null;
		db=this.getWritableDatabase();

		cursor= db.rawQuery("select LOC_CODE from MstUser", null);
		if(cursor.getCount()==1)
		{
			while(cursor.moveToNext())
			{
				distId= cursor.getString(cursor.getColumnIndex("LOC_CODE"));
			}
		}
		cursor.close();
		db.close();
		return distId;
	}


	public int inserIntoMstAdminRouteFromDownload(JSONArray jsonArray)
	{
		// this method call when MstItem table download
		db=this.getWritableDatabase();
		db.execSQL("delete from MstRoute");
		Log.i("log",""+db);
		ContentValues cv = new ContentValues();

		int count=0;
		try{
			JSONObject jb;
			for(int i=0;i<jsonArray.length();i++)
			{
				jb = jsonArray.getJSONObject(i);
				cv.put("ROUTE_CODE",jb.getString("ROUTE_CODE"));
				cv.put("BLOCK_CODE",jb.getString("BLOCK_CODE"));
				cv.put("BLOCK_NAME",jb.getString("BLOCK_NAME"));
				db.insert("MstRoute", null,cv);
				count++;
				cv.clear();
			}
			return count;
		}
		catch (Exception e) {
			count=-101;
			Log.e("log", e.getMessage(),e.fillInStackTrace());
			return count;
		}
		finally
		{
			db.close();
		}
	}


	public int inserIntoMstUserAllListFromDownload(JSONArray jsonArray)
	{
		// this method call when MstItem table download
		db=this.getWritableDatabase();
		db.execSQL("delete from MstUserAllList");
		Log.i("log",""+db);
		ContentValues cv = new ContentValues();

		int count=0;
		try{
			JSONObject jb;
			for(int i=0;i<jsonArray.length();i++)
			{
				jb = jsonArray.getJSONObject(i);
				cv.put("User_ID",jb.getString("User_ID"));
				cv.put("First_Name",jb.getString("First_Name"));
				cv.put("Last_Name", jb.getString("Last_Name"));
				cv.put("User_RoleID",jb.getString("User_RoleID"));
				cv.put("Loc_Code",jb.getString("Loc_Code"));
				cv.put("Password", jb.getString("Password"));
				db.insert("MstUserAllList", null,cv);
				count++;
				cv.clear();
			}
			return count;
		}
		catch (Exception e) {
			count=-101;
			Log.e("log", e.getMessage(),e.fillInStackTrace());
			return count;
		}
		finally
		{
			db.close();
		}
	}




	public Object[] routeInfo ()
	{
		db=this.getWritableDatabase();
		Cursor cursor=null;

		cursor= db.rawQuery("select distinct cast(ROUTE_CODE AS INT) as ROUTE_CODE from MstRoute ORDER BY ROUTE_CODE ", null);


		String[] routeId= new String[cursor.getCount()];
		int i=0;


		while(cursor.moveToNext())
		{

			routeId[i]= cursor.getString(0);

			i++;
		}
		cursor.close();
		db.close();
		return new Object[]{routeId,routeId};

	}


	public Object[] allUserInfo ()
	{
		db=this.getWritableDatabase();
		Cursor cursor=null;

		cursor= db.rawQuery("select User_ID,User_ID||' - '||First_Name as First_Name from MstUserAllList ORDER BY User_ID ", null);


		String[] user_ID= new String[cursor.getCount()];
		String[] first_Name= new String[cursor.getCount()];
		int i=0;


		while(cursor.moveToNext())
		{
			user_ID[i]= cursor.getString(cursor.getColumnIndex("User_ID"));
			first_Name[i]= cursor.getString(cursor.getColumnIndex("First_Name"));

			i++;
		}
		cursor.close();
		db.close();
		return new Object[]{user_ID,first_Name};

	}

}