package com.marico.fsp.adminactivity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.SQLException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.marico.fsp.R;
import com.marico.fsp.adminhelper.AdminDownloadHelper;
import com.marico.fsp.util.GetJSONArray;

public class AdminUserSelectionActivity extends Activity
{

	private ListView listViewAdminUserlist;
	private Button buttonAdminUSBack,buttonAdminUSNext;
	private ButtonClick buttonClick;
	String [] UserId ,userName;
	AdminDownloadHelper helper;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.lyt_admin_user_selection);

		buttonAdminUSBack= (Button) findViewById(R.id.buttonAdminUSBack);
		buttonAdminUSNext= (Button) findViewById(R.id.buttonAdminUSNext);
		listViewAdminUserlist = (ListView) findViewById(R.id.listViewAdminUserlist);

		helper = new AdminDownloadHelper(AdminUserSelectionActivity.this);

		new BeatDownloadAsyncTask().execute("");

		buttonClick = new ButtonClick();
		buttonAdminUSBack.setOnClickListener(buttonClick);
		buttonAdminUSNext.setOnClickListener(buttonClick);
	}




	class ButtonClick implements OnClickListener
	{
		@Override
		public void onClick(View v)
		{
			switch (v.getId()) {
			case R.id.buttonAdminUSBack:
				finish();
				break;

			case R.id.buttonAdminUSNext:

				String userid=getSavedItems();
				if(userid.length()>0)
				{
					Intent i = new Intent(AdminUserSelectionActivity.this, AdminRouteDownloadActivity.class);
					i.putExtra("UserId", userid);
					startActivity(i);
				}
				else
				{
					Toast.makeText(getApplicationContext(),"Please Select User" , Toast.LENGTH_LONG).show();
				}
				break;
			}

		}

	}


	class BeatDownloadAsyncTask extends AsyncTask<String, String, String>
	{
		ProgressDialog progressDialog ;
		TextView textViewmsg;


		String downloadURL_MstUserAllList="";
		@Override
		protected void onPreExecute() {
			
			helper.deleteALLtableForDownload();
			
			String URL=helper.getUrl();
			downloadURL_MstUserAllList=URL+"/Download.aspx?TableName=ML_FSP_Master";


			progressDialog= new ProgressDialog(AdminUserSelectionActivity.this);
			progressDialog.setTitle("Please Wait..");
			progressDialog.setMessage("Loading");
			progressDialog.setCancelable(false);
			progressDialog.show();
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... params) 
		{
			try
			{
				helper.inserIntoMstUserAllListFromDownload(GetJSONArray.getfromURL(downloadURL_MstUserAllList));
			}
			catch (SQLException e) {
				return "SQLException";
			}
			catch (Exception e) {
				return "Exception";
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result)
		{
			super.onPostExecute(result);

			if(progressDialog.isShowing())
			{
				progressDialog.dismiss();
			}
			
			Object obj[]=helper.allUserInfo();
			UserId = (String[]) obj[0];
			userName = (String[]) obj[1];
			listViewAdminUserlist.setAdapter(new ArrayAdapter<String>(AdminUserSelectionActivity.this, android.R.layout.simple_list_item_single_choice, userName));

			listViewAdminUserlist.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
			listViewAdminUserlist.setItemsCanFocus(false);
		}

	}


	private String getSavedItems() 
	{
		String savedItems = "";
		int count = listViewAdminUserlist.getAdapter().getCount();
		for (int i = 0; i < count; i++) {
			if (listViewAdminUserlist.isItemChecked(i)) {

				savedItems=UserId[i];
			}
		}
		return savedItems;
	}

}
