package com.marico.fsp.adminactivity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.marico.fsp.R;
import com.marico.fsp.adminhelper.AdminReportHelper;

public class AdminReportMbMnLbBnActivity extends Activity
{
	private Button buttonARMBMNLBBNBack;
	private Spinner	spinnerARMBMNLBBNRoute;
	private Spinner	spinnerARMBMNLBBNFarmer;
	private ListView listViewARMBMNLBBN;
	private AdminReportHelper helper;

	String[] route;
	String[] frmId,frmName;

	String[]  arraytitle,arraymn, arraymb, arraylb,arraybn;

	String routeid;
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.lyt_admin_report_mb_mn_lb_bn);

		buttonARMBMNLBBNBack = (Button) findViewById(R.id.buttonARMBMNLBBNBack);
		spinnerARMBMNLBBNRoute = (Spinner) findViewById(R.id.spinnerARMBMNLBBNRoute);
		spinnerARMBMNLBBNFarmer = (Spinner) findViewById(R.id.spinnerARMBMNLBBNFarmer);
		listViewARMBMNLBBN  = (ListView) findViewById(R.id.listViewARMBMNLBBN);


		buttonARMBMNLBBNBack.setOnClickListener(new ButtonOnClickListener());

		helper = new AdminReportHelper(AdminReportMbMnLbBnActivity.this);
		Object [] obj1 = helper.getRouteInfo();
		route = (String[]) obj1[0];








		ArrayAdapter<String> spinnerArrayAdapter1 = new ArrayAdapter<String>(this,   android.R.layout.simple_spinner_item, route);
		spinnerArrayAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down vieww
		spinnerARMBMNLBBNRoute.setAdapter(spinnerArrayAdapter1);







		spinnerARMBMNLBBNRoute.setOnItemSelectedListener(new SpinnerListener());
		spinnerARMBMNLBBNFarmer.setOnItemSelectedListener(new SpinnerListener());




	}
	@Override
	public void onBackPressed() {}


	class ButtonOnClickListener implements OnClickListener
	{
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.buttonARMBMNLBBNBack:
				finish();
				break;

			default:
				break;
			}
		}
	}



	class EntryCustomListView extends BaseAdapter //custom list with help of base adapter for Order Modify Delete Activity
	{


		TextView textViewARmbmnlbbn_title,textViewARmbmnlbbn_mb,textViewARmbmnlbbn_mn,textViewARmbmnlbbn_lb,textViewARmbmnlbbn_bn;

		@Override
		public int getCount() {

			return arraymn.length;
		}
		@Override
		public Object getItem(int position) {

			return arraymn[position];
		}
		@Override
		public long getItemId(int position) {

			return position;
		}
		@Override
		public View getView(int position, View convertView, ViewGroup parent)
		{
			View row=convertView;
			if(row==null)
			{
				LayoutInflater vi = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				row = vi.inflate(R.layout.lyt_admin_report_mb_mn_lb_bn_list,null);
			}
			textViewARmbmnlbbn_title = (TextView) row.findViewById(R.id.textViewARmbmnlbbn_title);
			textViewARmbmnlbbn_mb = (TextView) row.findViewById(R.id.textViewARmbmnlbbn_mb);
			textViewARmbmnlbbn_mn = (TextView) row.findViewById(R.id.textViewARmbmnlbbn_mn);
			textViewARmbmnlbbn_lb = (TextView) row.findViewById(R.id.textViewARmbmnlbbn_lb);
			textViewARmbmnlbbn_bn = (TextView) row.findViewById(R.id.textViewARmbmnlbbn_bn);

			textViewARmbmnlbbn_title.setText(arraytitle[position]);
			textViewARmbmnlbbn_mb.setText(arraymb[position]);
			textViewARmbmnlbbn_mn.setText(arraymn[position]);
			textViewARmbmnlbbn_lb.setText(arraylb[position]);
			textViewARmbmnlbbn_bn.setText(arraybn[position]);


			return row;
		}
	}
	class SpinnerListener implements OnItemSelectedListener  // Listener class  for SpinnerOMBeatName
	{
		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) 
		{
			switch (arg0.getId()) {
			case R.id.spinnerARMBMNLBBNRoute:

				Object[] obj2=helper.getFarmerNameFromMstFarm(route[arg2]);
				frmId=(String[]) obj2[0];
				frmName=(String[]) obj2[1];

				ArrayAdapter<String> spinnerArrayAdapter2 = new ArrayAdapter<String>(AdminReportMbMnLbBnActivity.this,android.R.layout.simple_spinner_item, frmName);
				spinnerArrayAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down vieww
				spinnerARMBMNLBBNFarmer.setAdapter(spinnerArrayAdapter2);

				//Toast.makeText(getApplicationContext(), "route is : "+route[arg2], Toast.LENGTH_SHORT).show();
				break;

			case R.id.spinnerARMBMNLBBNFarmer:

				Object[] obj3=helper.getReportDate(frmId[arg2]);
				arraytitle =(String[]) obj3[0];

				arraymb =(String[]) obj3[1];
				arraymn =(String[]) obj3[2];
				arraylb =(String[]) obj3[3];
				arraybn =(String[]) obj3[4];
				listViewARMBMNLBBN.setAdapter(new EntryCustomListView());



				//	Toast.makeText(getApplicationContext(), "frmName is : "+frmName[arg2], Toast.LENGTH_SHORT).show();
				break;
			}

		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0)
		{			
		}
	}






}