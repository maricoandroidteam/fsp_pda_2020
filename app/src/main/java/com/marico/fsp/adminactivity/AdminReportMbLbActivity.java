package com.marico.fsp.adminactivity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.marico.fsp.R;
import com.marico.fsp.adminhelper.AdminReportHelper;

public class AdminReportMbLbActivity extends Activity
{
	private Button buttonARMBLBBack;
	private Spinner	spinnerARMBLBRoute;
	private Spinner	spinnerARMBLBFarmer;
	private ListView listViewARMBLM;
	private AdminReportHelper helper;


	String[] route;
	String[] frmId,frmName;

	String[]  arraytitle, arraymb, arraylb;

	String routeid;
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.lyt_admin_report_mb_lb);

		buttonARMBLBBack = (Button) findViewById(R.id.buttonARMBLBBack);
		spinnerARMBLBRoute = (Spinner) findViewById(R.id.spinnerARMBLBRoute);
		spinnerARMBLBFarmer = (Spinner) findViewById(R.id.spinnerARMBLBFarmer);
		listViewARMBLM  = (ListView) findViewById(R.id.listViewARMBLM);

		buttonARMBLBBack.setOnClickListener(new ButtonOnClickListener());

		helper = new AdminReportHelper(AdminReportMbLbActivity.this);
		Object [] obj1 = helper.getRouteInfo();
		route = (String[]) obj1[0];



		ArrayAdapter<String> spinnerArrayAdapter1 = new ArrayAdapter<String>(this,   android.R.layout.simple_spinner_item, route);
		spinnerArrayAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down vieww
		spinnerARMBLBRoute.setAdapter(spinnerArrayAdapter1);




		spinnerARMBLBRoute.setOnItemSelectedListener(new SpinnerListener());
		spinnerARMBLBFarmer.setOnItemSelectedListener(new SpinnerListener());



	}
	@Override
	public void onBackPressed() {}


	class ButtonOnClickListener implements OnClickListener
	{
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.buttonARMBLBBack:
				finish();
				break;

			default:
				break;
			}
		}
	}



	class EntryCustomListView extends BaseAdapter //custom list with help of base adapter for Order Modify Delete Activity
	{
		TextView textViewARmblb_title,textViewARmblb_mb,textViewARmblb_lb;

		@Override
		public int getCount() {

			return arraytitle.length;
		}

		@Override
		public Object getItem(int position) {

			return arraytitle[position];
		}
		@Override
		public long getItemId(int position) {

			return position;
		}
		@Override
		public View getView(int position, View convertView, ViewGroup parent)
		{
			View row=convertView;
			if(row==null)
			{
				LayoutInflater vi = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				row = vi.inflate(R.layout.lyt_admin_report_mb_lb_list,null);
			}
			textViewARmblb_title = (TextView) row.findViewById(R.id.textViewARmblb_title);
			textViewARmblb_mb = (TextView) row.findViewById(R.id.textViewARmblb_mb);
			textViewARmblb_lb = (TextView) row.findViewById(R.id.textViewARmblb_lb);


			textViewARmblb_title.setText(arraytitle[position]);
			textViewARmblb_mb.setText(arraymb[position]);
			textViewARmblb_lb.setText(arraylb[position]);

			return row;
		}
	}
	class SpinnerListener implements OnItemSelectedListener  // Listener class  for SpinnerOMBeatName
	{
		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) 
		{
			switch (arg0.getId()) {


			case R.id.spinnerARMBLBRoute:

				Object[] obj2=helper.getFarmerNameFromMstFarm(route[arg2]);
				frmId=(String[]) obj2[0];
				frmName=(String[]) obj2[1];

				ArrayAdapter<String> spinnerArrayAdapter2 = new ArrayAdapter<String>(AdminReportMbLbActivity.this,   android.R.layout.simple_spinner_item, frmName);
				spinnerArrayAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down vieww
				spinnerARMBLBFarmer.setAdapter(spinnerArrayAdapter2);

				//	Toast.makeText(getApplicationContext(), "route is : "+route[arg2], Toast.LENGTH_SHORT).show();
				break;

			case R.id.spinnerARMBLBFarmer:


				Object[] obj3=helper.getReportDate(frmId[arg2]);
				arraytitle =(String[]) obj3[0];
				arraymb =(String[]) obj3[1];
				arraylb =(String[]) obj3[3];
				listViewARMBLM.setAdapter(new EntryCustomListView());

				//	Toast.makeText(getApplicationContext(), "frmName is : "+frmName[arg2], Toast.LENGTH_SHORT).show();

				break;
			}

		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0)
		{			
		}
	}




}