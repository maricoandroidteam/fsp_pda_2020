package com.marico.fsp.adminactivity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.marico.fsp.R;
import com.marico.fsp.adminhelper.AdminReportHelper;

public class AdminReportNsActivity extends Activity
{
	private Button buttonARNSBack;
	private Spinner	spinnerARNSRoute;
	private Spinner	spinnerARNSFarmer;
	private ListView listViewARNS;
	private AdminReportHelper helper;

	String[] route;
	String[] frmId,frmName;

	String[]  arraytitle,arrayns, arraymb, arraybn;

	String routeid;
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.lyt_admin_report_ns);

		buttonARNSBack = (Button) findViewById(R.id.buttonARNSBack);
		spinnerARNSRoute = (Spinner) findViewById(R.id.spinnerARNSRoute);
		spinnerARNSFarmer = (Spinner) findViewById(R.id.spinnerARNSFarmer);
		listViewARNS  = (ListView) findViewById(R.id.listViewARNS);
		buttonARNSBack.setOnClickListener(new ButtonOnClickListener());

		helper = new AdminReportHelper(AdminReportNsActivity.this);
	    Object [] obj1 = helper.getRouteInfo();
		route = (String[]) obj1[0];
		
		
		

		

		ArrayAdapter<String> spinnerArrayAdapter1 = new ArrayAdapter<String>(this,   android.R.layout.simple_spinner_item, route);
		spinnerArrayAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down vieww
		spinnerARNSRoute.setAdapter(spinnerArrayAdapter1);

		



		spinnerARNSRoute.setOnItemSelectedListener(new SpinnerListener());
		spinnerARNSFarmer.setOnItemSelectedListener(new SpinnerListener());

		

	}
	@Override
	public void onBackPressed() {}


	class ButtonOnClickListener implements OnClickListener
	{
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.buttonARNSBack:
				finish();
				break;

			default:
				break;
			}
		}
	}



	class EntryCustomListView extends BaseAdapter //custom list with help of base adapter for Order Modify Delete Activity
	{


		TextView textViewARns_title,textViewARns_ns;

		@Override
		public int getCount() {

			return arrayns.length;
		}
		@Override
		public Object getItem(int position) {

			return arrayns[position];
		}
		@Override
		public long getItemId(int position) {

			return position;
		}
		@Override
		public View getView(int position, View convertView, ViewGroup parent)
		{
			View row=convertView;
			if(row==null)
			{
				LayoutInflater vi = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				row = vi.inflate(R.layout.lyt_admin_report_ns_list,null);
			}
			textViewARns_title = (TextView) row.findViewById(R.id.textViewARns_title);
			textViewARns_ns = (TextView) row.findViewById(R.id.textViewARns_ns);



			textViewARns_title.setText(arraytitle[position]);
			textViewARns_ns.setText(arrayns[position]);



			return row;
		}
	}
	class SpinnerListener implements OnItemSelectedListener  // Listener class  for SpinnerOMBeatName
	{
		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) 
		{
			switch (arg0.getId()) {
			case R.id.spinnerARNSRoute:
				
				Object[] obj2=helper.getFarmerNameFromMstFarm(route[arg2]);
				frmId=(String[]) obj2[0];
				frmName=(String[]) obj2[1];
				
				
				ArrayAdapter<String> spinnerArrayAdapter2 = new ArrayAdapter<String>(AdminReportNsActivity.this,   android.R.layout.simple_spinner_item, frmName);
				spinnerArrayAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down vieww
				spinnerARNSFarmer.setAdapter(spinnerArrayAdapter2);
				
				
			//	Toast.makeText(getApplicationContext(), "route is : "+route[arg2], Toast.LENGTH_SHORT).show();
				break;

			case R.id.spinnerARNSFarmer:
				
				Object[] obj3=helper.getReportDate(frmId[arg2]);
				arraytitle =(String[]) obj3[0];
				arrayns =(String[]) obj3[5];
				listViewARNS.setAdapter(new EntryCustomListView());
								
			//	Toast.makeText(getApplicationContext(), "frmName is : "+frmName[arg2], Toast.LENGTH_SHORT).show();
				break;
			}

		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0)
		{			
		}
	}





}