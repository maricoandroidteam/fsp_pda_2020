package com.marico.fsp.adminactivity;

import com.marico.fsp.R;
import com.marico.fsp.adminhelper.ReportProductivityHelper;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;

public class ReportProductivityActivity extends Activity 
{
	Spinner spinnerRProFarmer, spinnerRProRoute;
	ListView listViewRPro;
	Button buttonRProBack;
	String[] Route, FarmCode, FarmName;
	ReportProductivityHelper reportProductivityHelper;
	
	String[] arraytitle, arrayMonth, arrayNutsHar, arrayNoOfTrees, arrayProd;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.lyt_report_productivity);
		
		spinnerRProRoute = (Spinner)findViewById(R.id.spinnerRProRoute);
		spinnerRProFarmer = (Spinner)findViewById(R.id.spinnerRProFarmer);
		listViewRPro = (ListView)findViewById(R.id.listViewRPro);
		buttonRProBack = (Button)findViewById(R.id.buttonRProBack);
		buttonRProBack.setOnClickListener(new ButtonOnClickListener());
		
		reportProductivityHelper = new ReportProductivityHelper(this);
		
		Object [] obj2 = reportProductivityHelper.getRouteInfo();
		Route = (String[]) obj2[0];

		ArrayAdapter<String> spinnerArrayAdapter1 = new ArrayAdapter<String>(this,   android.R.layout.simple_spinner_item, Route);
		spinnerArrayAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down vieww
		spinnerRProRoute.setAdapter(spinnerArrayAdapter1);

		spinnerRProRoute.setOnItemSelectedListener(new SpinnerListener());
		spinnerRProFarmer.setOnItemSelectedListener(new SpinnerListener());
		
	}
	
	class SpinnerListener implements OnItemSelectedListener  // Listener class  for SpinnerOMBeatName
	{
		@Override
		public void onItemSelected(AdapterView<?> parent, View view, int position, long id) 
		{
			switch (parent.getId()) {
			case R.id.spinnerRProRoute:
				
				Object [] obj1 = reportProductivityHelper.getFarmCodeAndName(Route[position]);
				FarmCode = (String[]) obj1[0];
				FarmName = (String[]) obj1[1];
				
				Log.i("Farm Name: ", ""+FarmName.length);

				if(FarmName.length == 0)
				{					
					arraytitle =new String[]{};
					arrayMonth =new String[]{};
					arrayNutsHar =new String[]{};
					arrayNoOfTrees =new String[]{};
					arrayProd =new String[]{};
					listViewRPro.setAdapter(new CustomListView());
				}
				
				ArrayAdapter<String> spinnerArrayAdapter1 = new ArrayAdapter<String>(ReportProductivityActivity.this, android.R.layout.simple_spinner_item, FarmName);
				spinnerArrayAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down vieww
				spinnerRProFarmer.setAdapter(spinnerArrayAdapter1);
				//Toast.makeText(getApplicationContext(), "route is : "+route[arg2], Toast.LENGTH_SHORT).show();
				break;

			case R.id.spinnerRProFarmer:
					Object[] obj3 = reportProductivityHelper.getProductivityByFarmCode(FarmCode[position]);
					arraytitle =(String[]) obj3[0];
					arrayMonth =(String[]) obj3[1];
					arrayNutsHar =(String[]) obj3[2];
					arrayNoOfTrees =(String[]) obj3[3];
					arrayProd =(String[]) obj3[4];
					listViewRPro.setAdapter(new CustomListView());
				
				break;
			}
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
		}
	}
	
	class CustomListView extends BaseAdapter //custom list with help of base adapter for Order Modify Delete Activity
	{
		TextView textViewRPro_title, textViewRPro_Month, textViewRPro_NutsHarv, textViewRPro_NoOfTree, textViewRPro_Productivity;

		@Override
		public int getCount() {

			return arrayMonth.length;
		}
		@Override
		public Object getItem(int position) {

			return arrayMonth[position];
		}
		@Override
		public long getItemId(int position) {

			return position;
		}
		@Override
		public View getView(int position, View convertView, ViewGroup parent)
		{
			View row=convertView;
			if(row==null)
			{
				LayoutInflater vi = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				row = vi.inflate(R.layout.lyt_report_productivity_listview,null);
			}
			textViewRPro_title = (TextView) row.findViewById(R.id.textViewRPro_title);
			textViewRPro_Month = (TextView) row.findViewById(R.id.textViewRPro_Month);
			textViewRPro_NutsHarv = (TextView) row.findViewById(R.id.textViewRPro_NutsHarv);
			textViewRPro_NoOfTree = (TextView) row.findViewById(R.id.textViewRPro_NoOfTree);
			textViewRPro_Productivity = (TextView) row.findViewById(R.id.textViewRPro_Productivity);
			
			textViewRPro_title.setText(arraytitle[position]);
			textViewRPro_Month.setText(arrayMonth[position]);
			textViewRPro_NutsHarv.setText(arrayNutsHar[position]);
			textViewRPro_NoOfTree.setText(arrayNoOfTrees[position]);
			textViewRPro_Productivity.setText(arrayProd[position]);
			
			return row;
		}
	}
	
	@Override
	public void onBackPressed() {}
	
	class ButtonOnClickListener implements OnClickListener
	{
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.buttonRProBack:
				finish();
				break;

			default:
				break;
			}
		}
	}
}
