package com.marico.fsp.adminactivity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.marico.fsp.R;
import com.marico.fsp.adminhelper.AdminReportHelper;

public class AdminReportProductivityActivity extends Activity
{
	private Button buttonARProBack;
	private Spinner	spinnerARProRoute;

	private ListView listViewARPro;
	private AdminReportHelper helper;

	String[] route;


	String[]  arrayFarmerName,arrayNuts, arrayTree, arrayProductivity;

	String routeid;
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.lyt_admin_report_productivity);

		buttonARProBack = (Button) findViewById(R.id.buttonARProBack);
		spinnerARProRoute = (Spinner) findViewById(R.id.spinnerARProRoute);

		listViewARPro  = (ListView) findViewById(R.id.listViewARPro);


		buttonARProBack.setOnClickListener(new ButtonOnClickListener());


		helper = new AdminReportHelper(AdminReportProductivityActivity.this);
		Object [] obj1 = helper.getRouteInfo();
		route = (String[]) obj1[0];


		ArrayAdapter<String> spinnerArrayAdapter1 = new ArrayAdapter<String>(this,   android.R.layout.simple_spinner_item, route);
		spinnerArrayAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down vieww
		spinnerARProRoute.setAdapter(spinnerArrayAdapter1);

		spinnerARProRoute.setOnItemSelectedListener(new SpinnerFarmIdListener());

	}
	@Override
	public void onBackPressed() {}


	class ButtonOnClickListener implements OnClickListener
	{
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.buttonARProBack:
				finish();
				break;

			default:
				break;
			}
		}
	}



	class EntryCustomListView extends BaseAdapter //custom list with help of base adapter for Order Modify Delete Activity
	{


		TextView textViewARPro_farm,textViewARPro_nuts,textViewARPro_tree,textViewARPro_pro;

		@Override
		public int getCount() {

			return arrayNuts.length;
		}
		@Override
		public Object getItem(int position) {

			return arrayNuts[position];
		}
		@Override
		public long getItemId(int position) {

			return position;
		}
		@Override
		public View getView(int position, View convertView, ViewGroup parent)
		{
			View row=convertView;
			if(row==null)
			{
				LayoutInflater vi = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				row = vi.inflate(R.layout.lyt_admin_report_productivity_list,null);
			}
			textViewARPro_farm = (TextView) row.findViewById(R.id.textViewARPro_farm);
			textViewARPro_nuts = (TextView) row.findViewById(R.id.textViewARPro_nuts);
			textViewARPro_tree = (TextView) row.findViewById(R.id.textViewARPro_tree);
			textViewARPro_pro = (TextView) row.findViewById(R.id.textViewARPro_pro);


			textViewARPro_farm.setText(arrayFarmerName[position]);
			textViewARPro_nuts.setText(arrayNuts[position]);
			textViewARPro_tree.setText(arrayTree[position]);
			textViewARPro_pro.setText(arrayProductivity[position]);


			return row;
		}
	}
	class SpinnerFarmIdListener implements OnItemSelectedListener  // Listener class  for SpinnerOMBeatName
	{
		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) 
		{

			Object obj[] = 	helper.getProductivityData(route[arg2]);

			arrayFarmerName = (String[]) obj[0];
			arrayNuts  = (String[]) obj[1];
			arrayTree = (String[]) obj[2];
			arrayProductivity  = (String[]) obj[3];

			listViewARPro.setAdapter(new EntryCustomListView());

		//	Toast.makeText(getApplicationContext(), "route is : "+route[arg2], Toast.LENGTH_SHORT).show();





		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0)
		{			
		}
	}





}