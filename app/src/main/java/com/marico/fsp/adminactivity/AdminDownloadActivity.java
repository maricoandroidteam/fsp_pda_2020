package com.marico.fsp.adminactivity;

import java.util.ArrayList;

import org.json.JSONException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.marico.fsp.R;
import com.marico.fsp.activity.MainMenuActivity;
import com.marico.fsp.adminhelper.AdminDownloadHelper;
import com.marico.fsp.helper.UserTrnLogHelper;

import com.marico.fsp.util.GetJSONArray;

public class AdminDownloadActivity extends Activity
{
	private Button buttonDDwonload,buttonDBack;
	ProgressBar progressBarDDownload;
	ListView listViewDDownload;
	
	private ArrayList<String> downloadMsgList = new ArrayList<String>();
	
	CustomListView customListView;
	String route="";
	String userId="";
	private AdminDownloadHelper downloadHelper;
	Boolean downloadfalgdone=false;


	private DownloadDataAsyncTask downloadDataAsyncTask;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lyt_download);
		savedInstanceState = getIntent().getExtras();
		route = getIntent().getStringExtra("route");
		userId = getIntent().getStringExtra("user");
		ButtonIdSetter();
	}
	@Override
	public void onBackPressed() {}
	void ButtonIdSetter()
	{
		downloadHelper = new AdminDownloadHelper(AdminDownloadActivity.this);
		customListView = new CustomListView();  
		ButtonClickListener buttonClickListener = new ButtonClickListener();
		buttonDDwonload = (Button) findViewById(R.id.buttonDDownload);
		buttonDBack = (Button) findViewById(R.id.buttonDBackNew);

		listViewDDownload = (ListView) findViewById(R.id.listViewDDownload);
		progressBarDDownload = (ProgressBar) findViewById(R.id.progressBarDDownload);
		progressBarDDownload.setVisibility(View.INVISIBLE);
		buttonDDwonload.setOnClickListener(buttonClickListener);
		buttonDBack.setOnClickListener(buttonClickListener);
		listViewDDownload.setAdapter(customListView);

		downloadDataAsyncTask = new DownloadDataAsyncTask();
	}

	class ButtonClickListener implements OnClickListener
	{

		@Override
		public void onClick(View v) 
		{

			switch (v.getId()) {
			case R.id.buttonDDownload:
				Log.i("log", "buttonDDownload Clicked");
				buttonDDwonload.setEnabled(false);
				downloadDataAsyncTask.execute("");
				break;

			case R.id.buttonDBackNew:
				Log.i("log", "buttonDBackNew Clicked");
				alertBack();
				break;
			}
		}
	}
	class DownloadDataAsyncTask extends AsyncTask<String, String, String> 
	{

		String URL=downloadHelper.getUrl();
		String downloadURL_MstFarm=URL+"/Download.aspx?TableName=ML_Farm_Master&UserId="+userId+"&Routecode="+route;
		//String downloadURL_MstAdminReport=URL+"/Download.aspx?TableName=ML_Fsp_data&UserId="+userId+"&Routecode="+route;
		String downloadURL_MstAdminReport=URL+"/Download.aspx?TableName=FSP_Farm_Data_Pda&UserId="+userId+"&Routecode="+route;
		String downloadURL_Mstproductivity=URL+"/Download.aspx?TableName=productivity&UserId="+userId+"&Routecode="+route;
		String downloadURL_Farm_DataDetail=URL+"/Download.aspx?TableName=AdminDetail&UserId="+userId+"&Routecode="+route+"&AdminData=fspdetail";
		String downloadURL_TrnAvg_Data=URL+"/Download.aspx?TableName=AdminDetail&UserId="+userId+"&Routecode="+route+"&AdminData=fspdata";


		@Override
		protected String doInBackground(String... params) 
		{
			//create object of InsertDbFromJSONArray 

			int records; // count for total recode
			Log.i("log", "Url is downloadURL_MstFarm :"+ downloadURL_MstFarm);
			Log.i("log", "Url is downloadURL_MstAdminReport :"+ downloadURL_MstAdminReport);
			Log.i("log", "Url is downloadURL_Mstproductivity :"+ downloadURL_Mstproductivity);
			Log.i("log", "Url is downloadURL_Farm_DataDetail :"+ downloadURL_Farm_DataDetail);
			Log.i("log", "Url is downloadURL_TrnAvg_Data :"+ downloadURL_TrnAvg_Data);
			

			Log.i("log", "route is :"+ route);

			//table MstItem  Downloading
			publishProgress("Downloading Started","1");
			publishProgress(" MstFarm Downloading","1");

			records=downloadHelper.inserIntoMstFarmFromDownload(GetJSONArray.getfromURL(downloadURL_MstFarm));
			if(records<0)
			{
				publishProgress("Downloaded fail try Agin "+records,"1");
				return "";

			}
			publishProgress("MstFarm Downloaded records "+records,"2");

			if (isCancelled())
			{
				Log.i("log", "isCancelled");
				return ""; 
			}


			publishProgress(" productivity Downloading","1");
			try {
				records = downloadHelper.inserIntoMstproductivitt(GetJSONArray.getfromURL(downloadURL_Mstproductivity));
			} catch (JSONException e) { 
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(records<0)
			{
				publishProgress("Downloaded fail try Agin "+records,"1");
				return "";

			}
			publishProgress("MstProductivity Downloaded records "+records,"2");
			

			publishProgress(" AdminReport Downloading","1");
			records=downloadHelper.inserIntoMstAdminReport(GetJSONArray.getfromURL(downloadURL_MstAdminReport));
			if(records<0)
			{
				publishProgress("Downloaded fail try Agin "+records,"1");
				return "";
			}
			publishProgress("AdminReport Downloaded records "+records,"2");

			
			publishProgress(" TrnAvg_Data Downloading","1");
			records=downloadHelper.inserIntoTrnAvg_DataFromDownload(GetJSONArray.getfromURL(downloadURL_TrnAvg_Data));
			if(records<0)
			{
				publishProgress("Downloaded fail try Agin "+records,"1");
				return "";

			}
			publishProgress("TrnAvg_Data Downloaded records "+records,"2");

			
			publishProgress(" Farm_Data Downloading","1");
			records=downloadHelper.inserIntoFarm_DataFromDownload(GetJSONArray.getfromURL(downloadURL_Farm_DataDetail));
			if(records<0)
			{
				publishProgress("Downloaded fail try Agin "+records, "1");
				return "";

			}
			publishProgress("Farm_Data Downloaded records "+records, "2");
			
			
			// Upload User information on server  in UserTrnLog Table
			String UserInfo_URL = URL + "/upload.aspx?TableName=UserTrnLog";
			UserTrnLogHelper userTrnLogHelper = new UserTrnLogHelper(AdminDownloadActivity.this);
			int result = userTrnLogHelper.UserTrnLogUpload("D", UserInfo_URL);
			if(result < 0)
			{
				publishProgress("Error in Downloading  "+result, "1");
				return "";
			}
			
			
			publishProgress("Downloading complete","1");
			return null;
		}
		@Override
		protected void onPreExecute() {
			progressBarDDownload.setVisibility(View.VISIBLE);
			super.onPreExecute();
		}
		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			progressBarDDownload.setVisibility(View.INVISIBLE);
			downloadfalgdone=true;
		}
		@Override
		protected void onProgressUpdate(String... values) {
			super.onProgressUpdate(values);
			if(values[1]=="1")
			{
				downloadMsgList.add(values[0]);
			}
			else
			{
				downloadMsgList.set((downloadMsgList.size()-1),values[0]);
			}
			customListView.notifyDataSetChanged();
		}
	}
	
	
	class  CustomListView extends BaseAdapter // Custom base adapter for downloadMsgList
	{
		TextView tetViewDStatus;
		@Override
		public int getCount() {
			return downloadMsgList.size();
		}
		
		@Override
		public Object getItem(int arg0) {
			return downloadMsgList.get(arg0);
		}
		
		@Override
		public long getItemId(int arg0) {
			return arg0;
		}
		
		@Override
		public View getView(int arg0, View arg1, ViewGroup arg2) 
		{
			View row=arg1;
			if(row==null)
			{
				LayoutInflater vi = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				row = vi.inflate(R.layout.lyt_download_listview,null);
			}

			tetViewDStatus = (TextView)row.findViewById(R.id.tetViewDStatus);

			tetViewDStatus.setText(downloadMsgList.get(arg0));
			return row;
		}
	}



	private void alertBack(){
		AlertDialog.Builder alertbox = new AlertDialog.Builder(AdminDownloadActivity.this);

		alertbox.setMessage("Exit Download");

		alertbox.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface arg0, int arg1) {
				if(downloadfalgdone)
				{
					Intent intent = new Intent(AdminDownloadActivity.this, MainMenuActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);
				}				
				else
				{
					finish();
				}
			}
		});

		alertbox.setNegativeButton("No", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface arg0, int arg1) {

			}
		});
		alertbox.show();
	}

}