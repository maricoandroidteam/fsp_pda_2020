package com.marico.fsp.adminactivity;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.marico.fsp.R;
import com.marico.fsp.activity.UserReportActivity;


public class AdminReportMenuActivity extends Activity {
	//private Button buttonAR_mn_mb_bn,buttonAR_mb_lb;

	private Button buttonAR_productivity,buttonAR_nutstock,buttonAR_back,buttonAR_Report;
	private Button  buttonAR_mb_mn_lb_bn;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lyt_admin_report_menu);
		buttonIdSetter();
	}
	@Override
	public void onBackPressed() {}
	void buttonIdSetter()
	{

		ButtonClickListener buttonClickListener = new ButtonClickListener();
		//buttonAR_mn_mb_bn = (Button) findViewById(R.id.buttonAR_mn_mb_bn);
		//buttonAR_mb_lb = (Button) findViewById(R.id.buttonAR_mb_lb);
		buttonAR_productivity = (Button) findViewById(R.id.buttonAR_productivity);
		buttonAR_nutstock = (Button) findViewById(R.id.buttonAR_nutstock);
		buttonAR_mb_mn_lb_bn= (Button) findViewById(R.id.buttonAR_mb_mn_lb_bn);
		buttonAR_back = (Button) findViewById(R.id.buttonAR_back);

		buttonAR_Report = (Button) findViewById(R.id.buttonAR_Report);

		//buttonAR_mn_mb_bn.setOnClickListener(buttonClickListener);
		//buttonAR_mb_lb.setOnClickListener(buttonClickListener);
		buttonAR_productivity.setOnClickListener(buttonClickListener);
		buttonAR_nutstock.setOnClickListener(buttonClickListener);
		buttonAR_Report.setOnClickListener(buttonClickListener);
		buttonAR_back.setOnClickListener(buttonClickListener);
		buttonAR_mb_mn_lb_bn.setOnClickListener(buttonClickListener);
	}
	class ButtonClickListener implements OnClickListener
	{
		@Override
		public void onClick(View v) 
		{


			switch (v.getId()) 
			{
			/*case R.id.buttonAR_mn_mb_bn:

				Intent i1 = new Intent(AdminReportMenuActivity.this, AdminReportMnMbBnActivity.class);
				startActivity(i1);
				break;
			case R.id.buttonAR_mb_lb:
				Intent i2 = new Intent(AdminReportMenuActivity.this, AdminReportMbLbActivity.class);
				startActivity(i2);
				break;*/
			case R.id.buttonAR_productivity:


				//Intent i3 = new Intent(AdminReportMenuActivity.this, AdminReportProductivityActivity.class);
				Intent i3 = new Intent(AdminReportMenuActivity.this, ReportProductivityActivity.class);
				startActivity(i3);

				break;

			case R.id.buttonAR_nutstock:
				Intent i4 = new Intent(AdminReportMenuActivity.this, AdminReportNsActivity.class);
				startActivity(i4);
				break;

			case R.id.buttonAR_Report:
				Intent i5 = new Intent(AdminReportMenuActivity.this, UserReportActivity.class);
				i5.putExtra("Key", "admin");
				startActivity(i5);
				break;

			case R.id.buttonAR_back:

				finish();
				break;

			case R.id.buttonAR_mb_mn_lb_bn:
				Intent i6 = new Intent(AdminReportMenuActivity.this, AdminReportMbMnLbBnActivity.class);
				startActivity(i6);

				break;


			}
		}
	}



}
