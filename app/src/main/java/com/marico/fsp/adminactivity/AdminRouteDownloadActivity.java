package com.marico.fsp.adminactivity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.SQLException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.marico.fsp.R;
import com.marico.fsp.adminhelper.AdminDownloadHelper;
import com.marico.fsp.util.GetJSONArray;

public class AdminRouteDownloadActivity extends Activity
{

	private ListView listViewAdminRouteDownload;
	private Button buttonAdminRDBack,buttonAdminRDNext;
	private ButtonClick buttonClick;
	String [] Route ;
	String userid;
	AdminDownloadHelper helper;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.lyt_admin_route_download);

		buttonAdminRDBack= (Button) findViewById(R.id.buttonAdminRDBack);
		buttonAdminRDNext= (Button) findViewById(R.id.buttonAdminRDNext);
		listViewAdminRouteDownload = (ListView) findViewById(R.id.listViewAdminRouteDownload);

		userid=getIntent().getStringExtra("UserId");

		Log.i("log", "userid"+userid);
		buttonClick = new ButtonClick();
		buttonAdminRDBack.setOnClickListener(buttonClick);
		buttonAdminRDNext.setOnClickListener(buttonClick);

		new BeatDownloadAsyncTask().execute("");
	}


	private String getSavedItems() 
	{
		String savedItems = "";
		int count = listViewAdminRouteDownload.getAdapter().getCount();
		for (int i = 0; i < count; i++) {
			if (listViewAdminRouteDownload.isItemChecked(i)) {

				if (savedItems.length() > 0) {
					savedItems += "," + listViewAdminRouteDownload.getItemAtPosition(i);
				} else {
					savedItems += listViewAdminRouteDownload.getItemAtPosition(i);
				}
			}
		}
		return savedItems;
	}

	class ButtonClick implements OnClickListener
	{
		@Override
		public void onClick(View v)
		{
			switch (v.getId()) {
			case R.id.buttonAdminRDBack:
				finish();
				break;

			case R.id.buttonAdminRDNext:

				Intent i = new Intent(AdminRouteDownloadActivity.this, AdminDownloadActivity.class);
				i.putExtra("route", getSavedItems());
				i.putExtra("user", userid);
				startActivity(i);

				break;
			}

		}

	}


	class BeatDownloadAsyncTask extends AsyncTask<String, String, String>
	{
		ProgressDialog progressDialog;
		String downloadURL_MstAdminRoute="";
		@Override
		protected void onPreExecute() {
			helper= new AdminDownloadHelper(AdminRouteDownloadActivity.this);
			String URL=helper.getUrl();
			downloadURL_MstAdminRoute=URL+"/Download.aspx?TableName=MstRoute&userid="+userid;
			Log.v("log","downloadURL_MstRoute"+downloadURL_MstAdminRoute);

			progressDialog= new ProgressDialog(AdminRouteDownloadActivity.this);
			progressDialog.setTitle("Please Wait..");
			progressDialog.setMessage("Loading");
			progressDialog.setCancelable(false);
			progressDialog.show();
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... params) 
		{
			try
			{
				helper.inserIntoMstAdminRouteFromDownload(GetJSONArray.getfromURL(downloadURL_MstAdminRoute));
			}
			catch (SQLException e) {
				return "SQLException";
			}
			catch (Exception e) {
				return "Exception";
			}

			return null;
		}

		@Override
		protected void onPostExecute(String result)
		{
			super.onPostExecute(result);

			progressDialog.dismiss();



			Object obj[]=helper.routeInfo();
			Route = (String[]) obj[0];
			listViewAdminRouteDownload.setAdapter(new ArrayAdapter<String>(AdminRouteDownloadActivity.this,
					android.R.layout.simple_list_item_multiple_choice, Route));
			listViewAdminRouteDownload.setItemsCanFocus(true);
			listViewAdminRouteDownload.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
		}
	}
}
