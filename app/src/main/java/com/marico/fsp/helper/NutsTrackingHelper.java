package com.marico.fsp.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

public class NutsTrackingHelper extends DatabaseHelper{

	public NutsTrackingHelper(Context context) {
		super(context);
	}

	public boolean chechforTrnNuts() 
	{
		db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery("Select * from TrnNutsPrice", null);
		cursor.moveToFirst();
		int count = cursor.getCount();
		cursor.close();
		db.close();

		if(count>0)
		{
			return true;
		}
		return false;

	}

	public String[] getTrnNuts_Data() 
	{
		db = this.getReadableDatabase();
		Cursor cursor = null;		
		String coconut = "0",copra= "0";

		cursor= db.rawQuery("Select * from TrnNutsPrice",null);
		while(cursor.moveToNext())
		{
			coconut = cursor.getString(cursor.getColumnIndex("Coconut_Price"));
			copra = cursor.getString(cursor.getColumnIndex("Copra_price"));			
		}
		cursor.close();
		db.close();
		return new String[]{coconut,copra};
	}

	public void saveInTrnNutsPrice(float coconut, float copra, boolean hasData) 
	{
		String loc_Code = getLocationCode();
		db = this.getWritableDatabase();
		ContentValues cv = new ContentValues();
		
		cv.put("Copra_Price", String.valueOf(copra));
		cv.put("Coconut_price", String.valueOf(coconut));
		cv.put("Loc_Code", loc_Code);
		if(hasData)
		{
			//db.update("TrnKilometer", cv, "FSP_ID = ? ", new String[]{fspId});
			db.execSQL("Delete from TrnNutsPrice");
		}
			db.insert("TrnNutsPrice", String.valueOf(copra), cv);	
		db.close();
	}
	public String getLocationCode()
	{
		db = this.getReadableDatabase();
		Cursor cursor = null;		
		String Loc_Code = "";

		cursor= db.rawQuery("Select Loc_Code from MstUser",null);
		while(cursor.moveToNext())
		{
			Loc_Code = cursor.getString(cursor.getColumnIndex("Loc_Code"));		
		}
		cursor.close();
		db.close();
		return Loc_Code;
	}

}
