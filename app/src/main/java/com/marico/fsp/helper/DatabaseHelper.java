package com.marico.fsp.helper;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;
@SuppressLint("SimpleDateFormat")
public class DatabaseHelper extends SQLiteOpenHelper
{
	protected SQLiteDatabase db;

	//url given by vijay tak on 08-01-2020
	private  final String DEFAULTURL  = "https://fsppda.maricoapps.biz/";

	public static final String BD_BackUp_PATH = "FSPDB_BackUp/";
	public static final String PATH = "FSPPDAAPK/";
	public static final String PACKAGENAME = "package:com.marico.fsp".toString();
	public static final String DATABASE_NAME = "FSP.db";
	public static int APPCOUNT=3;

	private static final int DATABASE_VERSION = 4;
	private static  String DATABASE_FILE_PATH_EXTERNAL;		
	private static final String dbTable_MstUser ="CREATE  TABLE if not exists MstUser(User_ID TEXT,First_Name TEXT,Last_Name TEXT,User_RoleID TEXT,Loc_Code TEXT,Password TEXT)";	
	//private static final String dbTable_MstCheckLastOilPrice ="Create Table if not exists MstCheckLastOilPrice ( FSPID Text, LastUploadDate Text, datediff Text, ToDay Text, Upload Text)";
	private static final String dbTable_TrnFSPLocation ="Create Table IF Not Exists TrnFSPLocation (FarmerId Text, Lattitude Text, Longitude Text, FspId Text, Accuracy Text, Provider TEXT, TrnDate DEFAULT (datetime('now','localtime')), UploadFlag DEFAULT ('N'))";
	private static final String dbTable_TrnNutsPrice ="Create Table if not exists TrnNutsPrice ( Entry_Date DEFAULT (strftime('%Y-%m-%d','now', 'localtime')),Copra_price Text, Coconut_Price Text, Loc_Code Text, UploadFlag DEFAULT ('N'))";		
	private static final String dbTable_TrnOilPrice ="Create Table if not exists TrnOilPrice ( OilPriceID Text, FSPID Text,OilPriceDate DEFAULT (strftime('%Y-%m-%d','now', 'localtime')), CNORate Text, PalmRate Text, CreatedBy Text, UploadFlag DEFAULT ('N'))";
	private static final String dbTable_MstFarm="CREATE TABLE if not exists MstFarm(FARM_CODE TEXT,BLOCK_CODE TEXT,BLOCK_NAME TEXT,ROUTE_CODE TEXT,FARM_NO TEXT,FARMER_NAME TEXT,ADDRESS TEXT,VILLAGE TEXT,PHONE TEXT,NO_OF_TREES TEXT,ACRE TEXT,NO_OF_SAMPLE_TREES TEXT)";
	private static final String dbTable_MstLastHarvested="CREATE TABLE if not exists MstLastHarvested(Farm_Code TEXT,Lt_Plucking_Month TEXT,Lt_Plucking_Year TEXT,Next_Plucking_Month TEXT,Next_Plucking_Year TEXT,Number_Of_Nuts TEXT,Exp_Nuts TEXT)";

	private static final String dbTable_MstRoute ="CREATE  TABLE if not exists MstRoute(ROUTE_CODE TEXT,BLOCK_CODE TEXT ,BLOCK_NAME TEXT)";
	private static final String dbTable_TrnFarm_Data_Temp="CREATE TABLE if not exists Farm_Data_Temp (TreeName TEXT, CurrentDate DEFAULT (strftime('%Y-%m-%d','now', 'localtime')), FarmCode TEXT, FSPId TEXT, MaturedBunches TEXT,MaturedNuts TEXT, ButtonBunches TEXT, ButtonNuts TEXT, LastTwoBunches TEXT, " +
			"  UploadFlag DEFAULT ('N'));";
	private static final String dbTable_TrnFarm_Data="CREATE TABLE if not exists Farm_Data (TreeName TEXT,CurrentDate DEFAULT (datetime('now','localtime')), FarmCode TEXT, FSPId TEXT, MaturedBunches TEXT,MaturedNuts TEXT, ButtonBunches TEXT, ButtonNuts TEXT, LastTwoBunches TEXT, " +
			"  UploadFlag DEFAULT ('N'));";




	private static final String dbTable_TrnAvg_Data="CREATE TABLE if not exists TrnAvg_Data (FarmCode TEXT,Round TEXT, CurrentDate DEFAULT (strftime('%Y-%m-%d','now', 'localtime')), FSPId TEXT, MaturedBunches TEXT,MaturedNuts TEXT, ButtonBunches TEXT, ButtonNuts TEXT, LastTwoBunches TEXT, Harvested TEXT," +
			" LastpluckingMonth TEXT, LastpluckingYear TEXT,   NumberOfNuts TEXT,  NutsStock TEXT,  NextpluckingMonth TEXT,  NextpluckingYear TEXT, ExpectedNuts TEXT, OutLookOfTree  TEXT, Fertilizer TEXT,  SecondLastpluckingMonth TEXT, SecondLastpluckingYear TEXT,TDate DEFAULT (datetime('now','localtime')) , UploadFlag DEFAULT ('N'));";

	private static final String dbTable_MstUserAllList ="CREATE  TABLE if not exists MstUserAllList(User_ID TEXT,First_Name TEXT,Last_Name TEXT,User_RoleID TEXT,Loc_Code TEXT,Password TEXT)";
	public static final String DATABASE_MSTVERSIONCONTROL_SQL = "CREATE TABLE if Not Exists MstVersionControl (id integer, VersionNumber TEXT, ApkUrl TEXT, ApkName TEXT, Mandatory TEXT)";

	public static final String MstAdminReport = "CREATE TABLE if Not Exists MstAdminReport (Entry_date TEXT,Matured_Nuts TEXT,Matured_Bunches TEXT,Button_Nuts TEXT,L2B TEXT, Nut_Stock TEXT,FSP_ID TEXT,Farm_Code TEXT,Tdate TEXT)";		

	public static final String Mstproductivity = "CREATE TABLE if Not Exists Mstproductivity (Farm_Code TEXT,Number_Of_Nuts TEXT,Lt_Plucking_Month TEXT,Lt_Plucking_Year TEXT, Exp_Nuts TEXT, Next_Plucking_Month TEXT, Next_Plucking_Year TEXT)";

	public static final String dbTable_TrnKilometer = "Create Table if not exists TrnKilometer ( Entry_Date  DEFAULT (strftime('%Y-%m-%d','now', 'localtime')), FSP_ID text, Route_Code text, Opening_KM text, Closing_KM text, Total_KM text, Reason text, Daily_Allowance text,  UploadFlag DEFAULT ('N'))";

	public static final String Vw_mstProductivity =" Create View if Not Exists Vw_mstProductivity AS " +
			" select * from (" +
			" select FarmCode AS Farm_Code,NumberOfNuts AS Number_Of_Nuts ,LastpluckingMonth AS Lt_Plucking_Month,LastpluckingYear " +
			" AS Lt_Plucking_Year ,DATE( LastpluckingYear || '-' ||  LastpluckingMonth || '-01' ) as Dt  from TrnAvg_Data " +
			" UNION  " +
			" select   Farm_Code, Number_Of_Nuts , Lt_Plucking_Month,Lt_Plucking_Year , DATE( Lt_Plucking_Year || '-' ||  " +
			" CASE   WHEN length(Lt_Plucking_Month)=1 THEN '0'||Lt_Plucking_Month  WHEN length(Lt_Plucking_Month)=2  THEN Lt_Plucking_Month END || '-01' ) as Dt from mstProductivity ) AS A " +
			" WHERE Dt IN" +
			" (SELECT  Dt from " +
			" (select FarmCode AS Farm_Code,NumberOfNuts AS Number_Of_Nuts ,LastpluckingMonth AS Lt_Plucking_Month,LastpluckingYear " +
			" AS Lt_Plucking_Year ,DATE( LastpluckingYear || '-' ||  LastpluckingMonth || '-01' ) as Dt  from TrnAvg_Data" +
			" where FarmCode = A.Farm_Code" +
			" UNION " +
			" select   Farm_Code, Number_Of_Nuts , Lt_Plucking_Month,Lt_Plucking_Year , DATE( Lt_Plucking_Year || '-' ||  " +
			" CASE   WHEN length(Lt_Plucking_Month)=1 THEN '0'||Lt_Plucking_Month  WHEN length(Lt_Plucking_Month)=2 " +
			" THEN Lt_Plucking_Month END || '-01' ) " +
			" as Dt from mstProductivity where Farm_Code = A.Farm_Code ) Tab" +
			" ORDER by Dt DESC LIMIT 2)" +
			" Order By Farm_Code,Dt DESC";


	private SharedPreferences myPrefs;
	public  String urlStringMain;
	File filename = null;
	Context context;


	public DatabaseHelper(Context context) 
	{
		super(context, DATABASE_FILE_PATH_EXTERNAL, null, DATABASE_VERSION);
		this.context=context;
	}

	static 
	{
		String filename = Environment.getExternalStorageDirectory().getAbsolutePath()+File.separator+DATABASE_NAME;
		Log.i("log", "db path"+filename);
		DATABASE_FILE_PATH_EXTERNAL = filename;			
	}

	@Override
	public void onCreate(SQLiteDatabase db)
	{
		Toast.makeText(context, "on create called", Toast.LENGTH_LONG).show();
		db.execSQL(DATABASE_MSTVERSIONCONTROL_SQL);
		db.execSQL(dbTable_MstUser);
		//db.execSQL(dbTable_MstCheckLastOilPrice);
		db.execSQL(dbTable_MstFarm);
		db.execSQL(dbTable_MstLastHarvested);
		db.execSQL(dbTable_TrnFarm_Data);
		db.execSQL(dbTable_TrnFarm_Data_Temp);
		db.execSQL(dbTable_MstRoute);
		db.execSQL(dbTable_TrnAvg_Data);
		db.execSQL(dbTable_MstUserAllList);
		db.execSQL(MstAdminReport);
		db.execSQL(Mstproductivity);
		db.execSQL(Vw_mstProductivity);
		db.execSQL(dbTable_TrnFSPLocation);
		db.execSQL(dbTable_TrnKilometer);
		db.execSQL(dbTable_TrnNutsPrice);
		db.execSQL(dbTable_TrnOilPrice);		
		Log.i("log", "onCreate call");		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
	{
		Log.i("log", "onUpgrade call");

		if(oldVersion == 2)
		{
			BackUpDatabase();

			db.execSQL("ALTER TABLE MstVersionControl ADD Mandatory TEXT");

			db.execSQL("ALTER TABLE TrnAvg_Data ADD Fertilizer TEXT");
			db.execSQL("ALTER TABLE TrnAvg_Data ADD SecondLastpluckingMonth TEXT");
			db.execSQL("ALTER TABLE TrnAvg_Data ADD SecondLastpluckingYear TEXT");

			//db.execSQL("ALTER TABLE TrnAvg_Data ADD LastTwoBunches TEXT");

			// TrnAvg_Data ----- LastTwoBunches TEXT, Fertilizer TEXT, SecondLastpluckingMonth TEXT, SecondLastpluckingYear TEXT

			//db.execSQL("ALTER TABLE Farm_Data_Temp ADD LastTwoBunches TEXT");
			//db.execSQL("ALTER TABLE Farm_Data ADD LastTwoBunches TEXT");

			// Farm_Data_Temp , Farm_Data  ---------  LastTwoBunches TEXT
		}

		Toast.makeText(context, "on Upgrade called", Toast.LENGTH_LONG).show();
	}

	/*
	@Override
	public synchronized SQLiteDatabase getWritableDatabase() {
		try{
			db = SQLiteDatabase.openDatabase(DATABASE_FILE_PATH_EXTERNAL, null, SQLiteDatabase.OPEN_READWRITE + SQLiteDatabase.CREATE_IF_NECESSARY);

			return db;

		}catch(Exception e){
			Log.e("log",e.getMessage(),e.fillInStackTrace());
			if(db!=null)
				db.close();
		}
		return db;
	}

	public synchronized void createDatabase()
	{
		try{
			db = SQLiteDatabase.openDatabase(DATABASE_FILE_PATH_EXTERNAL, null, SQLiteDatabase.OPEN_READWRITE + SQLiteDatabase.CREATE_IF_NECESSARY);
			try{
				onCreate(db);
			}
			catch(Exception e){
				Log.e("log",e.getMessage(),e.fillInStackTrace());
			}
			db.close();
		}
		catch (Exception e) {
			// TODO: handle exception
		}
	}*/

	public void setUrl(String url) 
	{
		urlStringMain	= url;
		SharedPreferences.Editor prefsEditor = myPrefs.edit();
		prefsEditor.putString("url", urlStringMain);
		prefsEditor.commit();
	}

	public String getUrl() 
	{
		try
		{
			if(urlStringMain==null)
			{
				myPrefs =PreferenceManager.getDefaultSharedPreferences(context);
				urlStringMain = myPrefs.getString("url", DEFAULTURL);
				SharedPreferences.Editor prefsEditor = myPrefs.edit();
				prefsEditor.putString("url", urlStringMain);
				prefsEditor.commit();
				return urlStringMain;
			}
			else
				return urlStringMain;
		}
		catch (Exception e) 
		{
			urlStringMain = DEFAULTURL;
			return urlStringMain;
		}
	}

	public void BackUpDatabase()
	{
		String DBName = DatabaseHelper.DATABASE_NAME.toString();
		try
		{
			File sd = Environment.getExternalStorageDirectory(); 
			//File data = Environment.getDataDirectory(); 

			final String STORAGE_PATH = DatabaseHelper.BD_BackUp_PATH; //"FSPDB_BackUp/";

			File cnxDir = new File(Environment.getExternalStorageDirectory(), STORAGE_PATH);
			if(!cnxDir.exists())
			{
				cnxDir.mkdir();
			}

			if (sd.canWrite())
			{ 
				Calendar nowDate = Calendar.getInstance();
				SimpleDateFormat dfnowDate = new SimpleDateFormat("dd-MM-yyyy_kk.mm");
				String strnowDate = dfnowDate.format(nowDate.getTime());

				String currentDBPath = DBName;
				String backupDBPath = strnowDate+"_BackUp_"+DatabaseHelper.DATABASE_NAME; 
				File currentDB = new File(sd, currentDBPath); 
				File backupDB = new File(cnxDir, backupDBPath); 
				Log.i("Log", "The value of database path is "+currentDBPath);
				if (currentDB.exists())
				{ 
					FileChannel src = new FileInputStream(currentDB).getChannel(); 
					FileChannel dst = new FileOutputStream(backupDB).getChannel(); 
					dst.transferFrom(src, 0, src.size()); 
					src.close(); 
					dst.close(); 
					Toast.makeText(context, "Back Up Created with name: "+backupDBPath, Toast.LENGTH_SHORT).show();
				}
				else
				{
					Toast.makeText(context, "No Database Available of Name: "+DBName, Toast.LENGTH_SHORT).show();
				}
			} 
			else
			{
				Log.i("Log", "No SD card available");
			}

		} catch (Exception e) { 

			Log.e("Log", "msg if no db present "+e.toString());
		} 
	}
}