package com.marico.fsp.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

public class FSPEntryConfirmHelper extends DatabaseHelper{

	Context context;
	public FSPEntryConfirmHelper(Context context) {
		super(context);
		this.context = context;
		// TODO Auto-generated constructor stub
	}

	public String[] getEntryAvgDate(String FarmId)
	{
		db=this.getWritableDatabase();
		Cursor cursor=null;

		String matureNuts= "";
		String matureBunches= "";
		String lastTwoBunches= "";
		String buttonNuts= "";

		cursor= db.rawQuery("Select ROUND(avg(MaturedNuts),2) as MaturedNuts,ROUND(avg(MaturedBunches),2) as MaturedBunches,ROUND(avg(LastTwoBunches),2) as LastTwoBunches,ROUND(avg(ButtonNuts) ,2)as ButtonNuts from Farm_Data_Temp where FarmCode =?", new String[]{FarmId}); 

		while(cursor.moveToNext())
		{
			matureNuts= cursor.getString(cursor.getColumnIndex("MaturedNuts"));
			matureBunches= cursor.getString(cursor.getColumnIndex("MaturedBunches"));
			lastTwoBunches= cursor.getString(cursor.getColumnIndex("LastTwoBunches"));
			buttonNuts= cursor.getString(cursor.getColumnIndex("ButtonNuts"));
		}
		cursor.close();

		db.close();
		return new String[]{matureNuts,matureBunches,lastTwoBunches,buttonNuts};
	}

	public void saveToAvgTable(String farmerId,String avgmatureBunches,String avgmatureNuts, String avgbunchesCount,String avgbuttonNuts,
			String harvested,String previousmonth,String previousnuts,
			String nutstock,String nextmonth,String expectednuts, String outlook,String round,String fertilizer,String secondLastP) 
	{
		String fspid=getUserName();
		db = this.getWritableDatabase(); 

		db.execSQL("delete from TrnAvg_Data where FarmCode='"+farmerId+"'");

		ContentValues cv = new ContentValues();		

		cv.put("FarmCode",farmerId);
		cv.put("FSPId", fspid);
		cv.put("MaturedBunches",avgmatureBunches);
		cv.put("MaturedNuts", avgmatureNuts); 
		cv.put("ButtonBunches","0");
		cv.put("LastTwoBunches",avgbunchesCount);
		cv.put("ButtonNuts", avgbuttonNuts);

		Log.i("log", "1MaturedBunches"+avgmatureBunches);
		Log.i("log", "1MaturedNuts"+avgmatureNuts);
		Log.i("log", "1ButtonBunches"+avgbunchesCount);
		Log.i("log", "1ButtonNuts"+avgbuttonNuts);

		cv.put("Harvested",harvested);

		if(!previousmonth.equals("NA"))
		{
			String s1[]=previousmonth.split("/");
			cv.put("LastpluckingMonth", s1[0]); 
			cv.put("LastpluckingYear",s1[1]);
		}
		else
		{
			cv.put("LastpluckingMonth", "0"); 
			cv.put("LastpluckingYear","0");
		}

		cv.put("NumberOfNuts", previousnuts);
		cv.put("NutsStock",nutstock);

		if(!nextmonth.equals("NA"))
		{
			String s2[]=nextmonth.split("/");

			cv.put("NextpluckingMonth", s2[0]); 
			cv.put("NextpluckingYear",s2[1]);
		}
		else
		{
			cv.put("NextpluckingMonth", "0"); 
			cv.put("NextpluckingYear","0");
		}
		
		String s5[]=secondLastP.split("/");
		cv.put("Fertilizer", fertilizer); 
		cv.put("SecondLastpluckingMonth",s5[0]);
		cv.put("SecondLastpluckingYear",s5[1]);
		
		
		

		cv.put("ExpectedNuts", expectednuts); 
		cv.put("OutLookOfTree",outlook);
		cv.put("Round",round);

		db.insertOrThrow("TrnAvg_Data", farmerId, cv);
		db.close();
	}


	private String getUserName() { 
		db = this.getReadableDatabase();
		String uName = "";
		Cursor cursor = null;
		cursor = db.rawQuery("Select User_ID from mstUser", null);
		cursor.moveToFirst();
		while(!cursor.isAfterLast())
		{

			uName = cursor.getString(cursor.getColumnIndex("User_ID"));
			cursor.moveToNext();
		}
		cursor.close();
		db.close();
		return uName;
	}

	public void saveToFarmData(String farmerId) {
		db = this.getWritableDatabase();
		db.execSQL("delete from Farm_Data where FarmCode='"+farmerId+"'");
		String qry = "INSERT INTO Farm_Data SELECT * FROM Farm_data_temp";
		db.execSQL(qry);
		db.close();
	}



	public void delete_FarmDataTemp() 
	{ 
		db = this.getWritableDatabase();
		db.execSQL("delete from Farm_data_temp");
		db.close();
	}

	public String[] getdataFromAvg_data(String FarmId)
	{
		db=this.getWritableDatabase();
		Cursor cursor=null;
		cursor= db.rawQuery("select * from TrnAvg_Data where FarmCode='"+FarmId+"'", null);

		String harvested= "";
		String previousmonth= "";

		String previousnuts= "";
		String nutstock= "";
		String nextmonth= "";

		String expectednuts= "";
		String outlook= "";
		String round= "";
		
		String lastplucking="";
		String fertilizer="";
		while(cursor.moveToNext())
		{
			harvested = cursor.getString(cursor.getColumnIndex("Harvested"));
			String pmonth= cursor.getString(cursor.getColumnIndex("LastpluckingMonth"));
			String pmyear= cursor.getString(cursor.getColumnIndex("LastpluckingYear"));
			previousmonth =pmonth+"/"+pmyear;
			previousnuts= cursor.getString(cursor.getColumnIndex("NumberOfNuts"));
			nutstock= cursor.getString(cursor.getColumnIndex("NutsStock"));
			String nmonth= cursor.getString(cursor.getColumnIndex("NextpluckingMonth"));
			String nmyear= cursor.getString(cursor.getColumnIndex("NextpluckingYear"));
			nextmonth=nmonth+"/"+nmyear;
			expectednuts= cursor.getString(cursor.getColumnIndex("ExpectedNuts"));
			outlook= cursor.getString(cursor.getColumnIndex("OutLookOfTree"));
			round= cursor.getString(cursor.getColumnIndex("Round"));
			
			String slmonth= cursor.getString(cursor.getColumnIndex("SecondLastpluckingMonth"));
			String slyear= cursor.getString(cursor.getColumnIndex("SecondLastpluckingYear"));
			lastplucking=slmonth+"/"+slyear;
			
			fertilizer= cursor.getString(cursor.getColumnIndex("Fertilizer"));
			
			
		}
		cursor.close();

		db.close();
		return new String[]{harvested,previousmonth,previousnuts,nutstock,nextmonth,expectednuts,outlook,round,lastplucking,fertilizer};
	}

	public void saveLocation(String farmerId, double lattitude,double longitude, double accuracy, String provider) { 
		
		/*
		 * Here if the user edit the orders and trying to save then we are not allowing to update the location
		 * We only adding the fresh order location right now.
		 * 
		 * Later on whenever the criteria will arise then we have to update this code.
		 */
		
		String fspid=getUserName();
		//db.execSQL("delete from TrnFSPLocation where FarmerId='"+farmerId+"' and UploadFlag = 'N' ");
		int flagForFarmerId = getFarmerIdPresentOrNotIn_TrnFSPLocation(farmerId);		
		if(flagForFarmerId == 0)
		{
			if(provider.equalsIgnoreCase("NA"))
			{
				String[] records = getLatestRecodeFrom_TrmFSPLocation();
				lattitude = Double.parseDouble(records[0]);
				longitude = Double.parseDouble(records[1]);
				accuracy = Double.parseDouble(records[2]);
				provider = records[3];
			}
			db = this.getWritableDatabase();
				ContentValues cv = new ContentValues();		
				cv.put("FarmerId",farmerId);
				cv.put("FSPId", fspid);
				cv.put("Lattitude",lattitude);
				cv.put("Longitude", longitude); 
				cv.put("Accuracy",accuracy);
				cv.put("Provider", provider);						
			db.insertOrThrow("TrnFSPLocation", farmerId, cv);
		}	
		else
		{
			Log.i("Log", "Already present location for this farmer id");
		}
		db.close();		
	}

	private String[] getLatestRecodeFrom_TrmFSPLocation() { 
		db=this.getWritableDatabase();
		Cursor cursor=null;
		cursor= db.rawQuery("SELECT Lattitude,Longitude,Accuracy,Provider FROM TrnFSPLocation where UploadFlag = 'N' ORDER BY TrnDate DESC LIMIT 1", null);
		int count = cursor.getCount();
		String lattitude = null,longitude = null,accuracy = null,provider = null;
		if(count>0)
		{
			while(cursor.moveToNext())
			{
				lattitude = cursor.getString(cursor.getColumnIndex("Lattitude"));
				longitude = cursor.getString(cursor.getColumnIndex("Longitude"));
				accuracy = cursor.getString(cursor.getColumnIndex("Accuracy"));
				provider = cursor.getString(cursor.getColumnIndex("Provider"));
			}
		}
		else
		{
			lattitude = "0.0";
			longitude = "0.0";
			provider = "NA";
			accuracy = "0.0";
			Log.i("Log", "Inserting 0 because last record not exists");
		}
		cursor.close();
		db.close();
		return new String[]{lattitude,longitude,accuracy,provider};
	}

	private int getFarmerIdPresentOrNotIn_TrnFSPLocation(String farmerId) {
		
		db=this.getWritableDatabase();
		Cursor cursor=null;
		cursor= db.rawQuery("Select * from TrnFSPLocation where FarmerId='"+farmerId+"' and UploadFlag = 'N'", null);

		int count = cursor.getCount();
		cursor.close();
		db.close();
		return count;
	}

	public void delete_PreviousLocation(String farmerId) { 
	}
	public String[] getLastHarvestedData(String farmerId) {
		db=this.getWritableDatabase();
		Cursor cursor=null;
		
		String lastPluckingMonth= "";
		String lastPluckingYear= "";
		String nextPluckingMonth= "";
		String nextPluckingYear= "";
		String lastPlucking= "";
		String nextPlucking= "";
		String numOfNuts = "";
		String expNuts = "";
		cursor= db.rawQuery("Select * from MstLastHarvested where Farm_Code = ?", new String[]{farmerId}); 
		while(cursor.moveToNext())
		{
			lastPluckingMonth= cursor.getString(cursor.getColumnIndex("Lt_Plucking_Month"));
			lastPluckingYear= cursor.getString(cursor.getColumnIndex("Lt_Plucking_Year"));
			nextPluckingMonth= cursor.getString(cursor.getColumnIndex("Next_Plucking_Month"));
			nextPluckingYear= cursor.getString(cursor.getColumnIndex("Next_Plucking_Year"));
			numOfNuts = cursor.getString(cursor.getColumnIndex("Number_Of_Nuts"));
			expNuts= cursor.getString(cursor.getColumnIndex("Exp_Nuts"));
		}
		lastPlucking = lastPluckingMonth+"/"+lastPluckingYear;
		nextPlucking = nextPluckingMonth+"/"+nextPluckingYear;
		cursor.close();
		db.close();
		return new String[]{lastPlucking,nextPlucking,numOfNuts,expNuts};
	}
}
