package com.marico.fsp.helper;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class RouteDownloadHelper  extends DatabaseHelper
{
	private SQLiteDatabase db;
	DatabaseHelper databaseHelper;
	Context context;
	public RouteDownloadHelper(Context context)
	{
		super(context);
		this.context=context;
	}


	public Object[] routeInfo ()
	{
		db=this.getWritableDatabase();
		Cursor cursor=null;

		cursor= db.rawQuery("select distinct cast(ROUTE_CODE AS INT) as ROUTE_CODE from MstRoute ORDER BY ROUTE_CODE ", null);


		String[] routeId= new String[cursor.getCount()];
		int i=0;


		while(cursor.moveToNext())
		{

			routeId[i]= cursor.getString(0);

			i++;
		}
		cursor.close();
		db.close();
		return new Object[]{routeId,routeId};

	}

}
