package com.marico.fsp.helper;

import java.util.ArrayList;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;



public class FarmerSelectionHelper  extends DatabaseHelper
{
	private SQLiteDatabase db;
	DatabaseHelper databaseHelper;
	Context context;
	public FarmerSelectionHelper(Context context)
	{
		super(context);
		this.context=context;
	}


	public Object[] getRouteInfo() {
		// TODO Auto-generated method stub 
		db = this.getReadableDatabase();
		Cursor cursor = null;
		ArrayList<String> route;

		route = new ArrayList<String>();
		route.add(0, "All");
		cursor = db.rawQuery("Select distinct cast(ROUTE_CODE AS INT) as ROUTE_CODE from MstFarm", null);
		Log.i("Log", "getDsrNameAndDsrId is called");		
		cursor.moveToFirst();
		while(!cursor.isAfterLast())
		{
			route.add(cursor.getString(cursor.getColumnIndex("ROUTE_CODE")));

			cursor.moveToNext();
		}
		cursor.close();
		db.close();
		return new Object[]{route};
	}
	
	public Object[] getFarmerNameFromMstFarm(String routeid) {
		// TODO Auto-generated method stub
		db = this.getReadableDatabase();
		Cursor cursor = null;		
		ArrayList<String> arrayListFarmerId,arrayListFarmerName,arrayListColorCode;
		arrayListFarmerId = new ArrayList<String>();
		arrayListFarmerName = new ArrayList<String>();
		arrayListColorCode = new ArrayList<String>();
		

		if(routeid.equalsIgnoreCase("All"))
		{
			cursor= db.rawQuery("select distinct A.FARM_CODE,A.FARMER_NAME,case when B.FarmCode is null then '1' when B.UploadFlag='N' then '2' else '3' End as ColorCode from mstFarm as A left join TrnAvg_data AS B on A.FARM_CODE=B.FarmCode  order by FARMER_NAME collate NoCase ", null);
		}
		else
		{
			cursor= db.rawQuery("select distinct A.FARM_CODE,A.FARMER_NAME,case when B.FarmCode is null then '1' when B.UploadFlag='N' then '2' else '3' End as ColorCode from mstFarm as A left join TrnAvg_data AS B on A.FARM_CODE=B.FarmCode   where  A.ROUTE_CODE= ? order by FARMER_NAME collate NoCase   ", new String[]{routeid});

		}
		cursor.moveToFirst();
		while(!cursor.isAfterLast())
		{
			arrayListFarmerId.add(cursor.getString(cursor.getColumnIndex("FARM_CODE")));
			arrayListFarmerName.add(cursor.getString(cursor.getColumnIndex("FARMER_NAME")));
			arrayListColorCode.add(cursor.getString(cursor.getColumnIndex("ColorCode")));
			cursor.moveToNext();			
		}
		cursor.close();
		db.close();
		return new Object[]{arrayListFarmerId,arrayListFarmerName,arrayListColorCode};
	}


}
