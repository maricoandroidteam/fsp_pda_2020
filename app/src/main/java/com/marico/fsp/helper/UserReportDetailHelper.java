package com.marico.fsp.helper;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class UserReportDetailHelper extends DatabaseHelper
{
	private SQLiteDatabase db;
	DatabaseHelper databaseHelper;
	Context context;
	public UserReportDetailHelper(Context context) {
		super(context);
		this.context = context;		
	}



	public Object[] getEntryDate(String FarmId)
	{
		db=this.getWritableDatabase();
		Cursor cursor=null;
		cursor= db.rawQuery("select *, cast(subStr(TreeName,6,2) AS INT) AS no  from Farm_Data where FarmCode='"+FarmId+"'  Order by no", null);
		String[] arrayTreeName= new String[cursor.getCount()];
		String[] arrayMatureNuts= new String[cursor.getCount()];
		String[] arrayMatureBunches= new String[cursor.getCount()];
		String[] arrayLastTwoBunchesCount= new String[cursor.getCount()];
		String[] arrayButtonNuts= new String[cursor.getCount()];
	
		int i=0;
		while(cursor.moveToNext())
		{
			arrayTreeName[i]= cursor.getString(cursor.getColumnIndex("TreeName"));
			arrayMatureNuts[i]= cursor.getString(cursor.getColumnIndex("MaturedNuts"));
			arrayMatureBunches[i]= cursor.getString(cursor.getColumnIndex("MaturedBunches"));
			arrayLastTwoBunchesCount[i]= cursor.getString(cursor.getColumnIndex("LastTwoBunches"));
			arrayButtonNuts[i]= cursor.getString(cursor.getColumnIndex("ButtonNuts"));
		
			i++;
		}
		cursor.close();
		db.close();
		return new Object[]{arrayTreeName, arrayMatureNuts, arrayMatureBunches, arrayLastTwoBunchesCount, arrayButtonNuts};
	}



}
