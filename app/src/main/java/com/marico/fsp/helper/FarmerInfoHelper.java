package com.marico.fsp.helper;

import android.content.Context;
import android.database.Cursor;

public class FarmerInfoHelper extends DatabaseHelper
{

	public FarmerInfoHelper(Context context) {
		super(context);
	}

	public Object[] getFarmerInfo(String farmerid) 
	{

		db = this.getWritableDatabase();

		Cursor cursor=null;
		cursor= db.rawQuery("select * from MstFarm where FARM_CODE='"+farmerid+"'", null);
		String farm_no="",phone="",village="",address="",acre="",tree="",block="",route="";

		while(cursor.moveToNext())
		{
			farm_no= cursor.getString(cursor.getColumnIndex("FARM_NO"));
			village= cursor.getString(cursor.getColumnIndex("VILLAGE"));		
			acre= cursor.getString(cursor.getColumnIndex("ACRE"));
			tree= cursor.getString(cursor.getColumnIndex("NO_OF_TREES"));
			address= cursor.getString(cursor.getColumnIndex("ADDRESS"));
			phone= cursor.getString(cursor.getColumnIndex("PHONE"));
			block= cursor.getString(cursor.getColumnIndex("BLOCK_NAME"));
			route= cursor.getString(cursor.getColumnIndex("ROUTE_CODE"));
		}

		cursor.close();
		db.close();
		return new Object[]{farm_no,village,acre,tree,address,phone,block,route}; 
	}
	
	
	
	public int entryCount(String FarmCode) {

		// TODO Auto-generated method stub
		db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery("Select * from Farm_Data where FarmCode='"+FarmCode+"' AND UploadFlag='N'", null);
		int count = cursor.getCount();
		cursor.close();
		db.close();
		
		return count; 

	}

	public int getClosingKM() 
	{
		String closing = "0";
		db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery("Select Closing_KM from TrnKilometer", null);
		while(cursor.moveToNext())
		{
			closing = cursor.getString(cursor.getColumnIndex("Closing_KM"));
		}
		cursor.close();
		db.close();
		return Integer.parseInt(closing);
	}
}
