package com.marico.fsp.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

public class KilometerHelper extends DatabaseHelper{

	public KilometerHelper(Context context) {
		super(context); 
	}

	public String getRouteCode()
	{
		db = this.getWritableDatabase();

		Cursor cursor=null;
		cursor= db.rawQuery("Select distinct cast(ROUTE_CODE AS INT) as ROUTE_CODE from MstFarm", null);
		String route_code = "";
		while(cursor.moveToNext())
		{
			route_code= cursor.getString(cursor.getColumnIndex("ROUTE_CODE"));
		}
		cursor.close();
		db.close();
		return route_code;
	}
	private String getUserName() { 
		db = this.getReadableDatabase();
		String uName = "";
		Cursor cursor = null;
		cursor = db.rawQuery("Select User_ID from mstUser", null);
		cursor.moveToFirst();
		while(!cursor.isAfterLast())
		{
			uName = cursor.getString(cursor.getColumnIndex("User_ID"));
			cursor.moveToNext();
		}
		cursor.close();
		db.close();
		return uName;
	}

	public void save(String route_code,int open, int close, int traveld,String reson, int da, boolean hasData) {
		String	fspId = getUserName();
		db = this.getWritableDatabase(); 
		ContentValues cv = new ContentValues();		

		cv.put("FSP_ID", fspId);
		cv.put("Route_code", route_code);
		cv.put("Opening_KM",String.valueOf(open));
		cv.put("Closing_KM", String.valueOf(close));
		cv.put("Total_KM", String.valueOf(traveld));
		cv.put("Reason", reson);
		cv.put("Daily_Allowance", String.valueOf(da));
		if(hasData)
		{
			db.update("TrnKilometer", cv, "FSP_ID = ? ", new String[]{fspId});
		}
		else
		{
			db.insert("TrnKilometer", fspId, cv);
		}		
		db.close();
	}

	public String[] getTrnKM_Data() 
	{
		db = this.getReadableDatabase();
		Cursor cursor = null;		
		String opening = "0",closing= "0",reason= "",routeCode= "",traveld= "0";

		cursor= db.rawQuery("Select * from TrnKilometer",null);
		while(cursor.moveToNext())
		{
			opening = cursor.getString(cursor.getColumnIndex("Opening_KM"));
			closing = cursor.getString(cursor.getColumnIndex("Closing_KM"));
			reason = cursor.getString(cursor.getColumnIndex("Reason"));
			routeCode = cursor.getString(cursor.getColumnIndex("Route_Code"));			
			traveld = cursor.getString(cursor.getColumnIndex("Total_KM"));
		}
		cursor.close();
		db.close();
		Log.i("Log", "Opening is : "+opening);
		Log.i("Log", "Closing is : "+closing);
		Log.i("Log", "traveled is : "+traveld);
		return new String[]{opening,closing,reason,routeCode,traveld};
	}

	public boolean checkForTrnKilometer() 
	{
		Log.i("Log", "Has Data ");
		db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery("Select * from TrnKilometer", null);
		cursor.moveToFirst();
		int count = cursor.getCount();
		cursor.close();
		db.close();

		if(count>0)
		{
			return true;
		}
		return false;

	}
	public String[] checkForOilPrice() {

		String cnoRate = null;
		String palmRate = null;
		db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery("Select CNORate,PalmRate from TrnOilPrice", null);
		cursor.moveToFirst();
		int count = cursor.getCount();		
		if(count>0)
		{
			cnoRate = cursor.getString(cursor.getColumnIndex("CNORate"));
			palmRate = cursor.getString(cursor.getColumnIndex("PalmRate"));

		}
		else
		{
			cnoRate = "0";
			palmRate = "0";
		}
		cursor.close();
		db.close();
		return new String[]{cnoRate,palmRate};
	}

}
