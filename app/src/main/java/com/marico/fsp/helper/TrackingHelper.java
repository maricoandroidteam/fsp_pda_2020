package com.marico.fsp.helper;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

public class TrackingHelper extends DatabaseHelper{

	public TrackingHelper(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}
	public boolean checkForMstFarm() 
	{
		db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery("Select * from MstFarm", null);
		cursor.moveToFirst();
		int count = cursor.getCount();
		cursor.close();
		db.close();

		if(count>0)
		{
			Log.i("Log", "opening entered");
			return true;
		}
		return false;
	}
	public int checkForClosing() 
	{
		int closing = 0;
		String closeString = "";
		db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery("Select Closing_KM from TrnKilometer", null);
		cursor.moveToFirst();
		int count = cursor.getCount();
		if(count>0)
		{
			closeString = cursor.getString(cursor.getColumnIndex("Closing_KM"));

			if(!closeString.equals("") || !closeString.equals("0"))
			{
				closing = Integer.parseInt(closeString);
			}
			else
			{
				closing = 0;
			}
		}
		cursor.close();
		db.close();
		return closing;
	}
	public boolean checkOilTrackingUploadFlag() 
	{

		db = this.getWritableDatabase();
		String upload = "";
		Cursor cursor = db.rawQuery("Select UploadFlag from TrnOilPrice", null);
		cursor.moveToFirst();
		int count = cursor.getCount();
		if(count>0)
		{
			upload = cursor.getString(cursor.getColumnIndex("UploadFlag"));	
		}
		cursor.close();
		db.close(); 
		if(count>0)
		{
			if(upload.equals("N"))
			{
				return false;
			}		
			else
			{
				return true;
			}
		}
		else
		{
			return false;
		}
		
	}
	public boolean checkForNutUploaded() 
	{
		db = this.getWritableDatabase();
		String upload = "";
		Cursor cursor = db.rawQuery("Select UploadFlag from TrnNutsPrice", null);
		cursor.moveToFirst();
		int count = cursor.getCount();
		if(count>0)
		{
			upload = cursor.getString(cursor.getColumnIndex("UploadFlag"));	
		}
		cursor.close();
		db.close(); 
		if(count>0)
		{
			if(upload.equals("N"))
			{
				return false;
			}		
			else
			{
				return true;
			}
		}
		else
		{
			return false;
		}
		
	}
}