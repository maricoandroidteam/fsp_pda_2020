package com.marico.fsp.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

public class FSPEntryHelper extends DatabaseHelper {

	Context context;
	public FSPEntryHelper(Context context) {
		super(context);
		this.context = context;
	}

	public int saveInFarmDataTemp(String farmerId,	HashMap<String, ArrayList<String>> hashMapSaveToDB) 
	{

		String	fspId = getUserName();
		int count = 0;

		db = this.getWritableDatabase(); 
		ContentValues cv = new ContentValues();		

		Iterator<String> iterator = hashMapSaveToDB.keySet().iterator();

		//db.beginTransaction();
		while(iterator.hasNext())
		{
			String treeName = iterator.next();
			ArrayList<String> arrayList = hashMapSaveToDB.get(treeName);
			String buttonNuts = arrayList.get(0).toString().trim();
			String matureNuts = arrayList.get(1).toString().trim();
			String matureBunches = arrayList.get(2).toString().trim();
			String lastTwoMatureBunchesCount = arrayList.get(3).toString().trim();
			//String nutsEntryList = arrayList.get(4).toString().trim();

			String treeNames = arrayList.get(5).toString().trim();

			if((!buttonNuts.equals("")&& !buttonNuts.equals(null))|| (!matureNuts.equals("")&& !matureNuts.equals(null))||(!matureBunches.equals("")&& !matureBunches.equals(null))||(!lastTwoMatureBunchesCount.equals("")&& !lastTwoMatureBunchesCount.equals(null)))
			{
				Log.i("Log", "buttonNuts "+buttonNuts);
				Log.i("Log", "matureNuts "+matureNuts);
				Log.i("Log", "matureBunches "+matureBunches);
				Log.i("Log", "Button bunches "+lastTwoMatureBunchesCount);

				cv.put("FarmCode",farmerId);
				cv.put("FSPId", fspId); 
				cv.put("MaturedBunches",matureBunches);
				cv.put("ButtonBunches", "1");
				cv.put("ButtonNuts", buttonNuts);

				cv.put("MaturedNuts", matureNuts);
				cv.put("TreeName",treeNames);
				
				 //LastTwoBunches
				cv.put("LastTwoBunches", lastTwoMatureBunchesCount);
				
				db.insert("Farm_Data_Temp", farmerId, cv);
				count++;
			}
		}
		//db.endTransaction();
		db.close();

		return count;
	}


	public Boolean checkHashMapdata(final int totalCount,	HashMap<String, ArrayList<String>> hashMapSaveToDB) 
	{
		Log.i("log","totalCount"+ totalCount);
		int count = 0;
		Iterator<String> iterator = hashMapSaveToDB.keySet().iterator();

		while(iterator.hasNext())
		{
			String treeName = iterator.next();
			ArrayList<String> arrayList = hashMapSaveToDB.get(treeName);
			String buttonNuts = arrayList.get(0);
			String matureNuts = arrayList.get(1);
			String matureBunches = arrayList.get(2);
			String lastTwoMatureBunchesCount = arrayList.get(3);


			if((buttonNuts.equalsIgnoreCase("") || buttonNuts==null)
					|| (matureNuts.equalsIgnoreCase("") || matureNuts==null)
					|| (matureBunches.equalsIgnoreCase("") || matureBunches==null)
					|| (lastTwoMatureBunchesCount.equalsIgnoreCase("") || lastTwoMatureBunchesCount==null)
			)
			{
				
				Log.i("log", "count"+count);
				return false;
			}
			else
			{
				count++;
				Log.i("log", "done");
			}

		}
		if(totalCount==count)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public void deleteFromFarmDataTemp() {
		// TODO Auto-generated method stub
		db = this.getReadableDatabase();
		db.execSQL("delete from Farm_Data_Temp");
		db.close();
	}
	private String getUserName() { 
		// TODO Auto-generated method stub
		db = this.getReadableDatabase();
		String uName = "";
		Cursor cursor = null;
		cursor = db.rawQuery("Select User_ID from mstUser", null);
		cursor.moveToFirst();
		while(!cursor.isAfterLast())
		{
			//route.add(cursor.getString(cursor.getColumnIndex("ROUTE_CODE")));
			uName = cursor.getString(cursor.getColumnIndex("User_ID"));
			cursor.moveToNext();
		}
		cursor.close();
		db.close();
		return uName;
	}
	public HashMap<String, ArrayList<String>> getFarmerData(String farmerId) {

		copyFromFarmDataTO_Farm_Temp(farmerId); 
		HashMap<String, ArrayList<String>> hashMap= new HashMap<String, ArrayList<String>>();
		db = this.getReadableDatabase();
		Cursor cursor = null;

		//cursor = db.rawQuery("select A.MaturedNuts,A.MaturedBunches,A.ButtonBunches,A.ButtonNuts,A.Harvested,A.LastpluckingMonth,A.LastpluckingYear,A.NumberOfNuts,A.NutsStock,A.OutlookOfTree from Farm_Data AS A where FarmCode = ? and UploadFlag = 'N'", new String[]{farmerId});

		cursor = db.rawQuery("select A.TreeName, A.MaturedNuts,A.MaturedBunches,A.ButtonBunches,A.ButtonNuts, A.LastTwoBunches from Farm_Data_Temp AS A", null);
		String maturedNuts,maturedBunches,lastTwoBunches,buttonNuts;
		String treeName;
		cursor.moveToFirst();
		//int count = 1;
		while(!cursor.isAfterLast())
		{
			maturedNuts = cursor.getString(cursor.getColumnIndex("MaturedNuts"));
			maturedBunches = cursor.getString(cursor.getColumnIndex("MaturedBunches"));
			lastTwoBunches = cursor.getString(cursor.getColumnIndex("LastTwoBunches"));
			buttonNuts = cursor.getString(cursor.getColumnIndex("ButtonNuts"));
 

			treeName = cursor.getString(cursor.getColumnIndex("TreeName"));

			ArrayList<String> al = new ArrayList<String>();
			al.add(0,buttonNuts);
			al.add(1,maturedNuts);
			al.add(2,maturedBunches);
			al.add(3,lastTwoBunches);
			al.add(4,buttonNuts);

			al.add(5, treeName);
			//hashMap.put("Tree "+count, al);
			hashMap.put(treeName, al);

			//count++; 
			cursor.moveToNext();
		} 
		cursor.close();
		db.close();

		return hashMap;
	}
	private void copyFromFarmDataTO_Farm_Temp(String farmerId) {
		// TODO Auto-generated method stub
		db = this.getReadableDatabase();
		db.execSQL("INSERT INTO Farm_data_temp SELECT * FROM Farm_data where FarmCode = ?",new String[]{farmerId});
		db.close();
	}
	public int getCountOfTemp() {
		db = this.getWritableDatabase();
		Cursor cursor = null;
		cursor = db.rawQuery("select * from Farm_Data_Temp", null);
		int count = cursor.getCount();
		cursor.close();
		db.close();
		return count; 
	}
}