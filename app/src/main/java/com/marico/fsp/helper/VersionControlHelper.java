package com.marico.fsp.helper;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.util.Log;


public class VersionControlHelper extends DatabaseHelper 
{

	Context context;
	public VersionControlHelper(Context context)
	{
		super(context);
		this.context=context;
	}
	
	
	public int inserIntoMstVersionControlFromDownload(JSONArray jsonArray)
	{
		db=this.getWritableDatabase();

		db.execSQL("delete from MstVersionControl");
		
		ContentValues cv = new ContentValues();

		int count=0;
		try{
			JSONObject jb;
			for(int i=0;i<jsonArray.length();i++)
			{
				jb = jsonArray.getJSONObject(i);

				cv.put("id", jb.getString("id"));
				cv.put("VersionNumber", jb.getString("VersionNumber"));
				cv.put("ApkUrl",jb.getString("ApkUrl"));
				cv.put("ApkName",jb.getString("ApkName"));
				cv.put("Mandatory",jb.getString("Mandatory"));
				
				db.insertOrThrow("MstVersionControl", null,cv);
				count++;
				cv.clear();
			}
		}
		catch (Exception e) 
		{
			count=-101;
			Log.e("Log", "Exception In inserIntoMstVersionControlFromDownload() : "+e);
			db.close();
			return count;
		}
		db.close();
		return count;
	}
	
	
	public List<String> getUpdateVersion()
	{
		double currentVersion = 0;
		String strVersion = "0";
		
		strVersion = getVersionNo();
		//currentVersion = Double.parseDouble(strVersion);
		currentVersion = getVersionNumberInDouble(strVersion);
		
		List<String> arryListVersionControl = new ArrayList<String>();
		try 
		{
			int id;
			double latestVersion = 0;
			String apkUrl, apkName, mandatory, strlatestVersion;
			db = this.getWritableDatabase();
			
			String strQuery = "SELECT ifnull(id, 0) AS Id, ifnull(MAX(VersionNumber), 0.0) AS Version, ifnull(ApkUrl, '') AS ApkUrl, ifnull(ApkName, '') AS AkpName, ifnull(Mandatory, '') AS Mandatory FROM MstVersionControl";
			Cursor cursor = db.rawQuery(strQuery, null);
			cursor.moveToFirst();
	
			id = cursor.getInt(cursor.getColumnIndex("Id"));
			//latestVersion = cursor.getDouble(cursor.getColumnIndex("Version"));
			strlatestVersion = cursor.getString(cursor.getColumnIndex("Version"));
			apkName = cursor.getString(cursor.getColumnIndex("AkpName"));
			apkUrl = cursor.getString(cursor.getColumnIndex("ApkUrl"));
			mandatory = cursor.getString(cursor.getColumnIndex("Mandatory"));
						
			cursor.close();
			db.close();
		
			latestVersion = getVersionNumberInDouble(strlatestVersion);
		
			if(currentVersion < latestVersion)
			{
				arryListVersionControl.add(""+latestVersion);
				arryListVersionControl.add(apkName);
				arryListVersionControl.add(apkUrl);
				arryListVersionControl.add(mandatory);
			}
		} catch (Exception e) 
		{
			Log.e("Log", "Exception In UpdateVersion() : "+e);
		}
		return arryListVersionControl;
	}
	
	
	public double getVersionNumberInDouble(String strVersion)
	{
		//String strVersion = getVersionNo(); //"4.23e_jkd454gdk";
	    int length = strVersion.length();
	    String result = "0";
	    double versionNumber = 0.0;
	    
		try 
		{
		    for (int i = 0; i < length; i++) 
		    {
		        Character character = strVersion.charAt(i);
		        //Log.i("Log", "character is : " + character);
		        if (character.equals('.') || Character.isDigit(character)) 
		        {
		            result += character;
		        }
		    }
		    versionNumber = Double.parseDouble(result);
		  //Log.i("Log", "result is: " + result);
		    
		} catch (Exception e) {
			Log.e("Log", "Exception : "+e.getMessage());
		}
	    
		return versionNumber;
	}
	
	
	public String getVersionNo()
	{
		String version = "0";
		
		try 
		{
			PackageManager manager = context.getPackageManager();
			PackageInfo info;
			info = manager.getPackageInfo(context.getPackageName(), 0);
			version = info.versionName;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		
		if(version == null && version.equalsIgnoreCase(""))
		{
			version = "0";
		}
		return version;
	}
}
