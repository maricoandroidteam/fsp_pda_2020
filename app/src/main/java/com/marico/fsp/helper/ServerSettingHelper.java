package com.marico.fsp.helper;



import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.util.Log;

public class ServerSettingHelper extends DatabaseHelper
{
	
	String LOG="log";

	public ServerSettingHelper(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}
	public void deleteMstVersionControl(){
		try{
			db = this.getWritableDatabase();
			db.delete("MstVersionControl", null, null);
		}catch(Exception e){
			Log.e("Log", e.getMessage(), e.fillInStackTrace());
		}finally{
			db.close();
		}


	}//deleteMstVersionControl


	public void saveMstVersionControl(String[]  id,String[] versionNumber,String[] apkUrl,String[] apkName)throws Exception{
		Log.i(LOG, "MerchandisingHelper.saveMstValueType Start Save data in MstValueType");
		try {
			ContentValues cv = new ContentValues();
			db = this.getWritableDatabase();

			for(int i=0;i<id.length;i++){

				cv.put("id", id[i]);
				cv.put("versionNumber", versionNumber[i]);
				cv.put("apkUrl", apkUrl[i]);
				cv.put("ApkName", apkName[i]);
				db.insertOrThrow("MstVersionControl", "id", cv);
			}

		} catch (SQLException e) {
			Log.e("Log", e.getMessage());
			throw e;

		}finally{
			Log.i(LOG, "MerchandisingHelper.saveMstVersionControl inserted data in VersionControl");
			db.close(); 
		}		
	}//saveMstVersionControl
	

	public Object[] retrieveVersionDetails()
	{
		Cursor c = null;
		String latestVersion = null;

		db = this.getWritableDatabase();
		c = db.rawQuery("Select VersionNumber,ApkUrl,ApkName from MstVersionControl",null);

		int count = c.getCount();     



		int versionDataCursorCounter = 0;
		String[] versionNumberArr = new String[count];
		String[] apkUrlArr = new String[count];
		String[] apkName = new String[count];
		Log.i("log", "count"+c.getCount());
		while(c.moveToNext()){
			versionNumberArr[versionDataCursorCounter] = ""+c.getString(c.getColumnIndex("VersionNumber"))+".apk";
			apkUrlArr[versionDataCursorCounter] = c.getString(c.getColumnIndex("ApkUrl"));
			latestVersion = c.getString(c.getColumnIndex("VersionNumber"));
			apkName[versionDataCursorCounter] = c.getString(c.getColumnIndex("ApkName"));
			versionDataCursorCounter++;
		}


		c.close();
		db.close();
		return new Object[]{versionNumberArr,apkUrlArr,latestVersion,apkName};

	}//retrieveImageDataFromActivityStroage ends

}
