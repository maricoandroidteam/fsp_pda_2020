package com.marico.fsp.helper;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

public class DownloadHelper extends DatabaseHelper
{

	//DatabaseHelper databaseHelper;
	Context context;
	public DownloadHelper(Context context)
	{
		super(context);
		this.context=context;


	}
	public int inserIntoMstUserFromDownload(JSONArray jsonArray)
	{
		// this method call when MstUser table download

		db=this.getWritableDatabase();

		ContentValues cv = new ContentValues();

		int count=0;
		try{
			JSONObject jb;
			for(int i=0;i<jsonArray.length();i++)
			{
				jb = jsonArray.getJSONObject(i);

				cv.put("DsrId", jb.getString("DsrId"));
				cv.put("DistId", jb.getString("DistId"));
				cv.put("Password",jb.getString("Password"));
				db.insert("MstUser", null,cv);
				count++;
				cv.clear();
			}
		}
		catch (Exception e) {
			count=-101;
			Log.e("log", e.getMessage(),e.fillInStackTrace());
		}
		db.close();
		return count;
	}



	public int inserIntoMstFarmFromDownload(JSONArray jsonArray)
	{
		// this method call when MstItem table download
		db=this.getWritableDatabase();
		Log.i("log",""+db);
		ContentValues cv = new ContentValues();

		int count=0;
		try{
			JSONObject jb;
			for(int i=0;i<jsonArray.length();i++)
			{
				jb = jsonArray.getJSONObject(i);
				cv.put("FARM_CODE",jb.getString("FARM_CODE").trim());
				cv.put("BLOCK_CODE",jb.getString("BLOCK_CODE"));
				cv.put("BLOCK_NAME",jb.getString("BLOCK_NAME"));
				cv.put("ROUTE_CODE",jb.getString("ROUTE_CODE"));
				cv.put("FARM_NO", jb.getString("FARM_NO"));
				cv.put("FARMER_NAME",jb.getString("FARMER_NAME"));
				cv.put("ADDRESS",jb.getString("ADDRESS"));
				cv.put("VILLAGE",jb.getString("VILLAGE"));
				cv.put("PHONE",jb.getString("PHONE"));
				cv.put("NO_OF_TREES", jb.getString("NO_OF_TREES"));
				cv.put("ACRE", jb.getString("ACRE"));
				cv.put("NO_OF_SAMPLE_TREES", jb.getString("NO_OF_SAMPLE_TREES"));
				db.insert("MstFarm", null,cv);
				count++;
				cv.clear();
			}
			return count;
		}
		catch (Exception e) {
			count=-101;
			Log.e("log", e.getMessage(),e.fillInStackTrace());
			return count;
		}
		finally
		{
			db.close();
		}
	}




	public int inserIntoMstRouteFromDownload(JSONArray jsonArray)
	{
		// this method call when MstItem table download
		db=this.getWritableDatabase();
		Log.i("log",""+db);
		ContentValues cv = new ContentValues();

		int count=0;
		try{
			JSONObject jb;
			for(int i=0;i<jsonArray.length();i++)
			{
				jb = jsonArray.getJSONObject(i);
				cv.put("ROUTE_CODE",jb.getString("ROUTE_CODE"));
				cv.put("BLOCK_CODE",jb.getString("BLOCK_CODE"));
				cv.put("BLOCK_NAME",jb.getString("BLOCK_NAME"));
				db.insert("MstRoute", null,cv);
				count++;
				cv.clear();
			}
			return count;
		}
		catch (Exception e) {
			count=-101;
			Log.e("log", e.getMessage(),e.fillInStackTrace());
			return count;
		}
		finally
		{
			db.close();
		}
	}
	public void deleteALLtableForDownload()
	{
		db=this.getWritableDatabase();
		db.execSQL("delete from MstFarm");
		db.execSQL("delete from MstRoute");
		db.execSQL("delete from Farm_Data");
		db.execSQL("delete from Farm_Data_Temp");
		db.execSQL("delete from MstUserAllList");
		db.execSQL("delete from TrnAvg_Data");
		db.execSQL("delete from MstAdminReport");
		db.execSQL("delete from Mstproductivity");
		db.execSQL("delete from MstLastHarvested");
		db.execSQL("delete from TrnFSPLocation");
		
		/*
		db.execSQL("delete from TrnKilometer");
		db.execSQL("delete from TrnNutsPrice");
		db.execSQL("delete from TrnOilPrice");
		*/
		
		//db.execSQL("delete from MstVersionControl");
		
		db.close();
	}

	public String getUserId()
	{
		Cursor cursor;
		String distId = null;
		db=this.getWritableDatabase();

		cursor= db.rawQuery("select User_ID from MstUser", null);
		if(cursor.getCount()==1)
		{
			while(cursor.moveToNext())
			{
				distId= cursor.getString(cursor.getColumnIndex("User_ID"));
			}
		}
		cursor.close();
		db.close();
		return distId;
	}

	public String getLOC_CODE()
	{
		Cursor cursor;
		String distId = null;
		db=this.getWritableDatabase();

		cursor= db.rawQuery("select LOC_CODE from MstUser", null);
		if(cursor.getCount()==1)
		{
			while(cursor.moveToNext())
			{
				distId= cursor.getString(cursor.getColumnIndex("LOC_CODE"));
			}
		}
		cursor.close();
		db.close();
		return distId;
	}
	
	/*public int inserIntoMstCheckLastOilPrice(JSONArray jsonArray) {
		db=this.getWritableDatabase();
		Log.i("log",""+db);
		ContentValues cv = new ContentValues();

		int count=0;
		try{
			JSONObject jb;
			for(int i=0;i<jsonArray.length();i++)
			{
				jb = jsonArray.getJSONObject(i);
				cv.put("FSPID",jb.getString("FSPID"));
				cv.put("LastUploadDate",jb.getString("LastUploadDate"));
				cv.put("datediff",jb.getString("datediff"));
				cv.put("ToDay",jb.getString("ToDay"));
				cv.put("Upload",jb.getString("Upload"));
				db.insert("MstCheckLastOilPrice", null,cv);
				count++;
				cv.clear();
			}
			return count;
		}
		catch (Exception e) {
			count=-101;
			Log.e("log", e.getMessage(),e.fillInStackTrace());
			return count;
		}
		finally
		{
			db.close();
		}
	}*/
	public int inserIntoMstLastHarvestedFromDownload(JSONArray jsonArray) 
	{
		// this method call when MstItem table download
		db=this.getWritableDatabase();
		Log.i("log",""+db);
		ContentValues cv = new ContentValues();

		int count=0;
		try{
			JSONObject jb;
			for(int i=0;i<jsonArray.length();i++)
			{
				jb = jsonArray.getJSONObject(i);
				cv.put("Farm_Code",jb.getString("Farm_Code").trim());
				cv.put("Lt_Plucking_Month",jb.getString("Lt_Plucking_Month"));
				cv.put("Lt_Plucking_Year",jb.getString("Lt_Plucking_Year"));
				cv.put("Next_Plucking_Month",jb.getString("Next_Plucking_Month"));
				cv.put("Next_Plucking_Year", jb.getString("Next_Plucking_Year"));
				cv.put("Number_Of_Nuts",jb.getString("Number_Of_Nuts"));
				cv.put("Exp_Nuts",jb.getString("Exp_Nuts"));
				
				db.insert("MstLastHarvested", null,cv);
				count++;
				cv.clear();
			}
			return count;
		}
		catch (Exception e) {
			count=-101;
			Log.e("log", e.getMessage(),e.fillInStackTrace());
			return count;
		}
		finally
		{
			db.close();
		}
	}
}