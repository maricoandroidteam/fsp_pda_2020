package com.marico.fsp.helper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ConnectException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.telephony.TelephonyManager;
import android.util.Log;


public class UserTrnLogHelper extends DatabaseHelper
{
	Context context;

	public UserTrnLogHelper(Context context) 
	{
		super(context);
		this.context = context;
	}

	public int UserTrnLogUpload(String strUploadDownloadFlag, String UserInfo_URL)
	{
		int result = -999;

		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

		//User_ID, First_Name, Last_Name, User_RoleID, ServerTime ,MobileTime,VersionNo, IMEI, SIM ,UploadDownloadFlag
		String column_Name = "User_ID#"+"First_Name#"+"Last_Name#"+"User_RoleID#"+"MobileTime#"+"VersionNo#"+"IMEI#"+"SIM#"+"UploadDownloadFlag";

		nameValuePairs.add(new BasicNameValuePair("columnName", column_Name));

		String user_ID = null;
		String first_Name = null;
		String last_Name = null;
		String user_RoleID = null;

		String mobileTime = null;
		String imei = null;
		String versionNo = null;
		String sim = null;
		String uploadDownloadFlag = null;

		// User_ID, First_Name, Last_Name, User_RoleID
		String[] userDetails = getUserDetails();
		user_ID = userDetails[0];
		first_Name = userDetails[1];
		last_Name = userDetails[2];
		user_RoleID = userDetails[3];

		// MobileTime
		Calendar c = Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		mobileTime = df.format(c.getTime());

		// VersionNo
		versionNo = getVersionNo();

		// IMEI, SIM
		String[] imeiANDsim = getSIM();
		imei = imeiANDsim[0];
		//sim = imeiANDsim[1];
		sim = "99999";

		// UploadDownloadFlag
		uploadDownloadFlag = strUploadDownloadFlag;

		// User_ID, First_Name, Last_Name, User_RoleID, MobileTime, VersionNo, IMEI, SIM ,UploadDownloadFlag
		String[] column_data = new String[1];

		column_data[0] = user_ID+"#"+first_Name+"#"+last_Name+"#"+user_RoleID+"#"+mobileTime+"#"+versionNo+"#"+imei+"#"+sim+"#"+uploadDownloadFlag+"|";

		nameValuePairs.add(new BasicNameValuePair("columndata",column_data[0]));

		Log.i("Log", "UserInfo_URL : "+UserInfo_URL);

		int code = uploadToServer(nameValuePairs, UserInfo_URL);

		result = code;

		return result;
	}



	public String[] getUserDetails()
	{
		String[] userDetails = new String[4];

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = null;
		String strQuery = "SELECT User_ID, First_Name, Last_Name, User_RoleID FROM MstUser ";

		cursor = db.rawQuery(strQuery, null);

		cursor.moveToFirst();

		userDetails[0] = cursor.getString(cursor.getColumnIndex("User_ID"));
		userDetails[1] = cursor.getString(cursor.getColumnIndex("First_Name"));
		userDetails[2] = cursor.getString(cursor.getColumnIndex("Last_Name"));
		userDetails[3] = cursor.getString(cursor.getColumnIndex("User_RoleID"));

		cursor.close();
		db.close();

		return userDetails;
	}


	public String getVersionNo()
	{
		String version = "";

		try 
		{
			PackageManager manager = context.getPackageManager();
			PackageInfo info;
			info = manager.getPackageInfo(context.getPackageName(), 0);
			version = info.versionName;
		}
		catch (NameNotFoundException e) 
		{
			e.printStackTrace();
		}
		return version;
	}


	public String[] getSIM()
	{
		String[] imeiNsim = new String[2];

		String ImeiNo = "000000";
		String simSerialNumber = "000000";

		try 
		{
			TelephonyManager telephonyManager = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);

			ImeiNo =telephonyManager.getDeviceId();
			simSerialNumber = telephonyManager.getSimSerialNumber();
		} catch (Exception e) 
		{

		}

		imeiNsim[0] = ImeiNo;
		imeiNsim[1] = simSerialNumber;

		return imeiNsim;
	}


	public int uploadToServer(ArrayList<NameValuePair> arrlstNameValuePairs, String url)
	{
		InputStream is = null;
		StringBuilder sb = null;
		HttpClient httpclient=null;
		HttpPost httppost = null;
		try{
			httppost = new HttpPost(url);
			httppost.setEntity(new UrlEncodedFormEntity(arrlstNameValuePairs));
			HttpParams httpParameters = new BasicHttpParams();
			int timeoutConnection = 50000;
			HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
			int timeoutSocket = 50000;
			HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
			httpclient = new DefaultHttpClient(httpParameters);
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();
			StatusLine statusline = response.getStatusLine();
			int statuscode = statusline.getStatusCode();

			if(statuscode == 200)
			{
				Log.i("log","**Data is send On the Server***  "+statuscode);
			}else{
				Log.i("log","**Data Sending Error***"+statuscode);
				return -500;
			}
			is = entity.getContent();
			BufferedReader reader = new BufferedReader(new InputStreamReader(is,"UTF8"),16);
			sb = new StringBuilder();
			sb.append(reader.readLine() + "\n");
			String line="0";
			while((line = reader.readLine()) != null)
			{
				sb.append(line + "\n");
			}
			is.close();
			String result = sb.toString().trim();

			Log.i("log","resultn is "+result);

			if(result.contains("Success") || result=="Success")
			{
				return 1;
			}
			else
			{
				return -20;
			}

		}
		catch (ConnectException e) {
			e.printStackTrace();e.fillInStackTrace();
			Log.i("log", "ConnectException");
			return -1; // ConnectException
		}
		catch (ConnectTimeoutException e) {
			e.printStackTrace();e.fillInStackTrace();
			Log.i("log", "ConnectTimeoutException");
			return -2; //ConnectTimeoutException
		}
		catch (ClientProtocolException e) {
			e.printStackTrace();e.fillInStackTrace();
			return -3; // ClientProtocolException
		} 
		catch (IOException e) {
			e.printStackTrace();e.fillInStackTrace();
			return -4; // IOException
		}
		catch (Exception e) {
			e.printStackTrace();e.fillInStackTrace();
			return -5; // IOException
		}
	}

}
