package com.marico.fsp.helper;
import org.json.JSONArray;
import org.json.JSONObject;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class LoginHelper extends DatabaseHelper
{
	private SQLiteDatabase db;
	DatabaseHelper databaseHelper;
	Context context;
	public LoginHelper(Context context) {
		
		super(context);
		this.context = context;		
	}
	public String getUserName() {
		db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery("Select * from MstUser", null);
		cursor.moveToFirst();
		String uid = "";
		int count = cursor.getCount();
		if(count!=0)
		{
			while(!cursor.isAfterLast())
			{
				uid = cursor.getString(cursor.getColumnIndex("User_ID"));
				cursor.moveToNext();
			}
		}
		else {
			uid = "NA";
		}
		cursor.close();
		db.close();
		return uid;
	}
	public void saveMstUser(JSONArray jsonArray) 
	{
		db = this.getWritableDatabase();
		
		db.execSQL("delete from MstUser ");
		db.execSQL("delete from MstFarm");
		db.execSQL("delete from MstRoute");
		db.execSQL("delete from Farm_Data");
		db.execSQL("delete from Farm_Data_Temp");
		db.execSQL("delete from MstUserAllList");
		db.execSQL("delete from TrnAvg_Data");
		db.execSQL("delete from MstAdminReport");
		db.execSQL("delete from Mstproductivity");
		db.execSQL("delete from TrnFSPLocation");
		
		db.execSQL("delete from TrnKilometer");
		db.execSQL("delete from TrnNutsPrice");
		db.execSQL("delete from TrnOilPrice");
		
		db.execSQL("delete from MstLastHarvested");
		db.execSQL("delete from MstVersionControl");
		
		ContentValues cv = new ContentValues();
		int count=0;
		try{
			JSONObject jb;
			for(int i=0;i<jsonArray.length();i++)
			{
				jb = jsonArray.getJSONObject(i);

				cv.put("User_ID",jb.getString("User_ID"));
				cv.put("First_Name",jb.getString("First_Name"));
				cv.put("Last_Name", jb.getString("Last_Name"));
				cv.put("User_RoleID",jb.getString("User_RoleID"));
				cv.put("Loc_Code",jb.getString("Loc_Code"));
				cv.put("Password", jb.getString("Password"));

				db.insert("MstUser", null,cv);
				count++;
				cv.clear();
			}
		}
		catch (Exception e) {
			count=-101;
			Log.i("log", e.getMessage(),e.fillInStackTrace());
		}
		db.close();
	}
	public int loginChek(String uId, String uPwd) 
	{
		db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery("select * from MstUser where User_ID='"+uId+"' COLLATE NOCASE AND Password='"+uPwd+"' COLLATE NOCASE", null);
		int count = cursor.getCount();
		cursor.close();
		db.close();
		if(count==1)
		{
			return 1;
		}
		return 0; 

	}

	public Boolean userisAdmin()
	{

		db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery("Select * from MstUser where User_RoleId like '%admin%'", null);
		cursor.moveToFirst();
		int count = cursor.getCount();
		cursor.close();
		db.close();

		if(count>0)
		{
			Log.i("log", "userisAdmin True");
			return true;
		}
		return false;


	}
}
