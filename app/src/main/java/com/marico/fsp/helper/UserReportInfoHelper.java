package com.marico.fsp.helper;

import android.content.Context;
import android.database.Cursor;

public class UserReportInfoHelper extends DatabaseHelper{

	Context context;
	public UserReportInfoHelper(Context context) {
		super(context);
		this.context = context;
		// TODO Auto-generated constructor stub
	}


	public String[] getdataFromAvg_data(String FarmId)
	{
		db=this.getWritableDatabase();
		Cursor cursor=null;
		cursor= db.rawQuery("select * from TrnAvg_Data where FarmCode='"+FarmId+"'", null);

		String harvested= "";
		String previousmonth= "";

		String previousnuts= "";
		String nutstock= "";
		String nextmonth= "";

		String expectednuts= "";
		String outlook= "";
		String round= "";
		
		String secondLastP= "";
		String fertilizer= "";
		
		while(cursor.moveToNext())
		{
			harvested = cursor.getString(cursor.getColumnIndex("Harvested"));
			String pmonth= cursor.getString(cursor.getColumnIndex("LastpluckingMonth"));
			String pmyear= cursor.getString(cursor.getColumnIndex("LastpluckingYear"));
			previousmonth =pmonth+"/"+pmyear;
			previousnuts= cursor.getString(cursor.getColumnIndex("NumberOfNuts"));
			nutstock= cursor.getString(cursor.getColumnIndex("NutsStock"));
			String nmonth= cursor.getString(cursor.getColumnIndex("NextpluckingMonth"));
			String nmyear= cursor.getString(cursor.getColumnIndex("NextpluckingYear"));
			nextmonth=nmonth+"/"+nmyear;
			expectednuts= cursor.getString(cursor.getColumnIndex("ExpectedNuts"));
			outlook= cursor.getString(cursor.getColumnIndex("OutLookOfTree"));
			round= cursor.getString(cursor.getColumnIndex("Round"));
			
			
			
			
			
			
			String slmonth= cursor.getString(cursor.getColumnIndex("SecondLastpluckingMonth"));
			String slmyear= cursor.getString(cursor.getColumnIndex("SecondLastpluckingYear"));
			secondLastP=slmonth+"/"+slmyear;
			
			
			fertilizer= cursor.getString(cursor.getColumnIndex("Fertilizer"));
		}
		cursor.close();

		db.close();
		return new String[]{harvested,previousmonth,previousnuts,nutstock,nextmonth,expectednuts,outlook,round,secondLastP,fertilizer};
	}
}
