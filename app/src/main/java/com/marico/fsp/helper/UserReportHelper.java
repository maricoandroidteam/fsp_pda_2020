package com.marico.fsp.helper;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class UserReportHelper extends DatabaseHelper
{
	private SQLiteDatabase db;
	DatabaseHelper databaseHelper;
	Context context;
	public UserReportHelper(Context context) {
		super(context);
		this.context = context;		
	}

	public Object[] getRouteInfo() {
		// TODO Auto-generated method stub 
		db = this.getReadableDatabase();
		Cursor cursor = null;

		String[]route;


		cursor = db.rawQuery("Select distinct cast(ROUTE_CODE AS INT) as ROUTE_CODE from MstFarm AS A inner join TrnAvg_Data AS B on B.FarmCode=A.FARM_CODE", null);
		route = new String[cursor.getCount()+1];	

		route[0]="All";
		int i =1;
		cursor.moveToFirst();
		while(!cursor.isAfterLast())
		{
			route[i]=cursor.getString(cursor.getColumnIndex("ROUTE_CODE"));

			cursor.moveToNext();
			i++;
		}
		cursor.close();
		db.close();
		return new Object[]{route};
	}


	public Object[] getEntryDate(String route)
	{
		db=this.getWritableDatabase();
		Cursor cursor=null;
		if(route.equalsIgnoreCase("All"))
		{
			cursor= db.rawQuery("Select FARMER_NAME,* from TrnAvg_Data as A inner join  mstFarm AS B on A.FarmCode=B.FARM_CODE", null);
		}
		else
		{
			cursor= db.rawQuery("Select FARMER_NAME,* from TrnAvg_Data as A inner join  mstFarm AS B on A.FarmCode=B.FARM_CODE where B.ROUTE_CODE='"+route+"'", null);
		}
		String[] arrayFrmName= new String[cursor.getCount()];
		String[] arrayFrmId= new String[cursor.getCount()];
		String[] arrayMatureNuts= new String[cursor.getCount()];
		String[] arrayMatureBunches= new String[cursor.getCount()];
		String[] arrayLastTwoBunchesCount= new String[cursor.getCount()];
		String[] arrayButtonNuts= new String[cursor.getCount()];
		String[] arrayHarvested= new String[cursor.getCount()];
		String[] arrayPHCount= new String[cursor.getCount()];
		String[] arrayPHMonth= new String[cursor.getCount()];
		String[] arrayNutsStock= new String[cursor.getCount()];
		String[] arrayOutlook= new String[cursor.getCount()];
		int i=0;
		while(cursor.moveToNext())
		{
			arrayFrmId[i]= cursor.getString(cursor.getColumnIndex("FarmCode"));
			arrayFrmName[i]= cursor.getString(cursor.getColumnIndex("FARMER_NAME"));
			arrayMatureNuts[i]= cursor.getString(cursor.getColumnIndex("MaturedNuts"));
			arrayMatureBunches[i]= cursor.getString(cursor.getColumnIndex("MaturedBunches"));
			arrayLastTwoBunchesCount[i]= cursor.getString(cursor.getColumnIndex("LastTwoBunches"));
			arrayButtonNuts[i]= cursor.getString(cursor.getColumnIndex("ButtonNuts"));
			arrayHarvested[i]= cursor.getString(cursor.getColumnIndex("Harvested"));
			arrayPHCount[i]= cursor.getString(cursor.getColumnIndex("NumberOfNuts"));

			String monthYear=cursor.getString(cursor.getColumnIndex("LastpluckingMonth"));
			if(monthYear!=null)
			{
				monthYear=monthYear+"-"+cursor.getString(cursor.getColumnIndex("LastpluckingYear"));    
			}
			else
			{
				monthYear="";
			}
			arrayPHMonth[i]= monthYear;
			arrayNutsStock[i]= cursor.getString(cursor.getColumnIndex("NutsStock"));
			arrayOutlook[i]= cursor.getString(cursor.getColumnIndex("OutLookOfTree"));
			i++;
		}
		cursor.close();
		db.close();
		return new Object[]{arrayFrmId,arrayFrmName,arrayMatureNuts,arrayMatureBunches,arrayLastTwoBunchesCount,arrayButtonNuts
				,arrayHarvested,arrayPHCount,arrayPHMonth,arrayNutsStock,arrayOutlook};
	}



}
