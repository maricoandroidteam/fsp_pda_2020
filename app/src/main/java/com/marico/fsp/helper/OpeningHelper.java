package com.marico.fsp.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

public class OpeningHelper extends DatabaseHelper{

	public OpeningHelper(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	public String getRouteCode()
	{
		db = this.getWritableDatabase();

		Cursor cursor=null;
		cursor= db.rawQuery("Select distinct cast(ROUTE_CODE AS INT) as ROUTE_CODE from MstFarm", null);
		String route_code = "";
		while(cursor.moveToNext())
		{
			route_code= cursor.getString(cursor.getColumnIndex("ROUTE_CODE"));
		}
		cursor.close();
		db.close();
		return route_code;
	}
	private String getUserName() { 
		// TODO Auto-generated method stub
		db = this.getReadableDatabase();
		String uName = "";
		Cursor cursor = null;
		cursor = db.rawQuery("Select User_ID from mstUser", null);
		cursor.moveToFirst();
		while(!cursor.isAfterLast())
		{
			uName = cursor.getString(cursor.getColumnIndex("User_ID"));
			cursor.moveToNext();
		}
		cursor.close();
		db.close();
		return uName;
	}
	public void saveFromOpening(String route_code, String open) 
	{
		String	fspId = getUserName();
		db = this.getWritableDatabase(); 
		ContentValues cv = new ContentValues();		

		cv.put("FSP_ID", fspId);
		cv.put("Route_code", route_code);
		cv.put("Opening_KM",open);
		cv.put("Closing_KM", "0");
		cv.put("Total_KM", "0");
		cv.put("Reason", "");
		cv.put("Daily_Allowance", "0");
		db.insert("TrnKilometer", fspId, cv);
		db.close();
	}
	
}
