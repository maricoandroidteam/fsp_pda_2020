package com.marico.fsp.helper;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

public class MainMenuHelper extends DatabaseHelper
{
	public MainMenuHelper(Context context)
	{
		super(context);
	}
	public int getCountOfTrnFarm_Data() 
	{
		db = this.getWritableDatabase();
		//Cursor cursor = db.rawQuery("Select A.FSPId from TrnAvg_Data A where A.UploadFlag = 'N'  Union Select B.FSPId from Farm_data B where B.UploadFlag = 'N'  Union Select C.FspId from TrnFSPLocation C where C.UploadFlag = 'N'", null);
		Cursor cursor = db.rawQuery("Select A.FSPId from TrnAvg_Data A where A.UploadFlag = 'N'  Union Select B.FSPId from Farm_data B where B.UploadFlag = 'N'  Union Select C.FspId from TrnFSPLocation C where C.UploadFlag = 'N' Union Select D.FSP_ID from TrnKilometer D where D.UploadFlag = 'N' Union Select E.Loc_Code from TrnNutsPrice E where E.UploadFlag = 'N' Union Select F.FSPID from TrnOilPrice F where UploadFlag = 'N'", null);
		
		int count = cursor.getCount();
		cursor.close();
		db.close();
		return count; 
	}
	public Boolean userisAdmin()
	{
		db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery("Select * from MstUser where User_RoleId like '%admin%'", null);
		cursor.moveToFirst();
		int count = cursor.getCount();
		cursor.close();
		db.close();

		if(count>0)
		{
			Log.i("log", "userisAdmin True");
			return true;
		}
		return false;
	}
	
	public String getUserName()
	{
		String UserName="NA";
		db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(" Select ' Welcome '||First_Name || '-' || User_RoleID  AS Name from MstUser", null);
		cursor.moveToFirst();
		int count = cursor.getCount();

		if(count!=0)
		{
			UserName=cursor.getString(cursor.getColumnIndex("Name"));
		}

		cursor.close();
		db.close();
		return UserName;
	}
	public boolean checkForOpening() 
	{
		db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery("Select * from TrnKilometer", null);
		cursor.moveToFirst();
		int count = cursor.getCount();
		cursor.close();
		db.close();

		if(count>0)
		{
			Log.i("Log", "opening entered");
			return true;
		}
		return false;
	}
	public int checkForClosing() 
	{
		int closing = 0;
		String closeString = "";
		db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery("Select Closing_KM from TrnKilometer where UploadFlag = 'N'", null);
		cursor.moveToFirst();
		int count = cursor.getCount();
		if(count>0)
		{
			closeString = cursor.getString(cursor.getColumnIndex("Closing_KM"));

			if(!closeString.equals("") || !closeString.equals("0"))
			{
				closing = Integer.parseInt(closeString);
			}
			else
			{
				closing = 0;
			}
		}
		cursor.close();
		db.close();
		return closing;
	}
	public int getCoutOfTrnAvg_Data() 
	{
		db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery("Select * from TrnAvg_data ", null);
		int count = cursor.getCount();
		cursor.close();
		db.close();
		return count; 
	}
	public int getCountOF_Farmers() 
	{
		db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery("Select * from MstFarm", null);
		int count = cursor.getCount();
		cursor.close();
		db.close();
		return count;
	}
	public boolean checkForMstFarm() 
	{
		db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery("Select * from MstFarm", null);
		cursor.moveToFirst();
		int count = cursor.getCount();
		cursor.close();
		db.close();

		if(count>0)
		{
			Log.i("Log", "opening entered");
			return true;
		}
		return false;
	}
	public void deleteTrnKilometer() 
	{		
		db = this.getWritableDatabase();
		db.execSQL("Delete from TrnKilometer");
		db.close();
	}
	public void deleteNutsPrice() {
		db = this.getWritableDatabase();
		db.execSQL("Delete from TrnNutsPrice");
		db.close();
	}
	public void deleteOilPrice() {
		db = this.getWritableDatabase();
		db.execSQL("Delete from TrnOilPrice");
		db.close();
	}
	public String[] checkForCopraPrice() {

		String copraPrice = null;
		String coconutPrice = null;
		db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery("Select Copra_price,Coconut_Price from TrnNutsPrice", null);
		cursor.moveToFirst();
		int count = cursor.getCount();		
		if(count>0)
		{
			copraPrice = cursor.getString(cursor.getColumnIndex("Copra_price"));
			coconutPrice = cursor.getString(cursor.getColumnIndex("Coconut_Price"));

		}
		else
		{
			copraPrice = "0";
			coconutPrice = "0";
		}
		cursor.close();
		db.close();
		return new String[]{copraPrice,coconutPrice};
	}
	public int getUploadCountWithClosing() {
		db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery("Select A.FSPId from TrnAvg_Data A where A.UploadFlag = 'N'  Union Select B.FSPId from Farm_data B where B.UploadFlag = 'N'  Union Select C.FspId from TrnFSPLocation C where C.UploadFlag = 'N' Union Select D.FSP_ID from TrnKilometer D where D.UploadFlag = 'N' Union Select E.Loc_Code from TrnNutsPrice E where E.UploadFlag = 'N' Union Select F.FSPID from TrnOilPrice F where UploadFlag = 'N'", null);
		int count = cursor.getCount();
		cursor.close();
		db.close();
		return count; 
	}
}