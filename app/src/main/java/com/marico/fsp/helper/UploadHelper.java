package com.marico.fsp.helper;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.marico.fsp.util.UploadToServer;

public class UploadHelper extends DatabaseHelper
{

	DatabaseHelper databaseHelper;
	Context context;
	public UploadHelper(Context context)
	{
		super(context);
		this.context=context;
	}

	public int sendDataFormFormTrnAvg_Data(String url)
	{
		ArrayList<NameValuePair> arrlstNameValuePairs = new ArrayList<NameValuePair>();

		String col_1 = "";
		String col_2 = "";
		String col_3 = "";
		String col_4 = "";
		String col_5 = "";
		String col_6 = "";
		String col_7 = "";
		String col_8 = "";
		String col_9 = "";
		String col_10 = "";
		String col_11 = "";
		String col_12 = "";
		String col_13 = "";
		String col_14 = "";
		String col_15 = "";
		String col_16 = "";
		String col_17 = "";
		String col_18 = "";
		String col_19 = "";
		
		String col_20 = "";
		String col_21 = "";
		String col_22 = "";
		db=this.getWritableDatabase();
		Cursor cursor = null;
		String column_data= null;


		String column_Name =  "Entry_Date#"+"Round#"+"Farm_Code#"+"FSP_ID#"+"Matured_Bunches#"+"Button_Bunches#"+"Button_Nuts#"+"Harvested#"+"Lt_Plucking_Month#"+"Lt_Plucking_Year#"+"Number_Of_Nuts#"+"Nut_Stock#"+"Next_Plucking_Month#"+"Next_Plucking_Year#"+"Exp_Nuts#"+"Outlook#"+"Matured_Nuts#"+"TDate#"+"Last_2_Bunches#"+"SecondLastpluckingMonth#"+"SecondLastpluckingYear#"+"Fertilizer";
		Log.i("log", column_Name);
		arrlstNameValuePairs.add(new BasicNameValuePair("columnname",column_Name));
		cursor= db.rawQuery("select * from  TrnAvg_Data where UploadFlag='N'", null);
		int count=cursor.getCount();
		while(cursor.moveToNext())
		{
			col_1= cursor.getString(cursor.getColumnIndex("CurrentDate"));
			col_2= cursor.getString(cursor.getColumnIndex("Round"));
			col_3= cursor.getString(cursor.getColumnIndex("FarmCode"));
			col_4= cursor.getString(cursor.getColumnIndex("FSPId"));
			col_5= cursor.getString(cursor.getColumnIndex("MaturedBunches"));
			col_6= cursor.getString(cursor.getColumnIndex("ButtonBunches"));
			col_7= cursor.getString(cursor.getColumnIndex("ButtonNuts"));
			col_8= cursor.getString(cursor.getColumnIndex("Harvested"));
			col_9= cursor.getString(cursor.getColumnIndex("LastpluckingMonth"));
			col_10= cursor.getString(cursor.getColumnIndex("LastpluckingYear"));
			col_11= cursor.getString(cursor.getColumnIndex("NumberOfNuts"));
			col_12= cursor.getString(cursor.getColumnIndex("NutsStock"));
			col_13= cursor.getString(cursor.getColumnIndex("NextpluckingMonth"));
			col_14= cursor.getString(cursor.getColumnIndex("NextpluckingYear"));
			col_15= cursor.getString(cursor.getColumnIndex("ExpectedNuts"));
			col_16= cursor.getString(cursor.getColumnIndex("OutLookOfTree"));
			col_17= cursor.getString(cursor.getColumnIndex("MaturedNuts"));
			col_18= cursor.getString(cursor.getColumnIndex("TDate"));
			col_19 = cursor.getString(cursor.getColumnIndex("LastTwoBunches"));
			
			
			col_20= cursor.getString(cursor.getColumnIndex("SecondLastpluckingMonth"));
			col_21= cursor.getString(cursor.getColumnIndex("SecondLastpluckingYear"));
			col_22 = cursor.getString(cursor.getColumnIndex("Fertilizer"));

			/*if(col_5.equalsIgnoreCase(""))
			{
				col_5="0";
			}
			if(col_6.equalsIgnoreCase(""))
			{
				col_6="0";
			}
			if(col_17.equalsIgnoreCase(""))
			{
				col_17="0";
			}
			if(col_7.equalsIgnoreCase(""))
			{
				col_7="0";
			}
			if(col_11.equalsIgnoreCase(""))
			{
				col_11="0";
			}
			if(col_12.equalsIgnoreCase(""))
			{
				col_12="0";
			}
			if(col_15!=null)
			{
				if( col_15.equalsIgnoreCase("null")|| col_15.equalsIgnoreCase(""))
					col_15="0";

			}
			else
			{
				col_15="0";
			}
			 */



			column_data = col_1+"#"+col_2+"#"+col_3+"#"+col_4+"#"+col_5+"#"+col_6+"#"+col_7+"#"+col_8+"#"+col_9+"#"+col_10+"#"+col_11+"#"+col_12+"#"+col_13+"#"+col_14+"#"+col_15+"#"+col_16+"#"+col_17+"#"+col_18+"#"+col_19+"#"+col_20+"#"+col_21+"#"+col_22+"|";
			Log.i("log", column_data);
			arrlstNameValuePairs.add(new BasicNameValuePair("columndata",column_data));
		}
		if(count==0)
		{
			cursor.close();
			db.close();
			return 0;
		}
		cursor.close();
		db.close();

		UploadToServer uploadToServer = new UploadToServer();
		int code=uploadToServer.uploadToServer(arrlstNameValuePairs,url);
		if(code==1)
		{

			setFlagYforFormTrnAvg_Data();
		}
		else
		{
			count=code;
		}
		return count;
	}
	public int sendDataFormFarm_DataTable(String url)
	{
		ArrayList<NameValuePair> arrlstNameValuePairs = new ArrayList<NameValuePair>();
		String col_1 = "";
		String col_2 = "";
		String col_3 = "";
		String col_4 = "";
		String col_5 = "";
		String col_6 = "";
		String col_7 = "";
		String col_8 = "";
		String col_9 = "";
		
		db=this.getWritableDatabase();
		Cursor cursor = null;
		String column_data= null;
		String column_Name =  "TreeName#"+"Entry_Date#"+"Farm_Code#"+"FSP_ID#"+"Matured_Bunches#"+"Matured_Nuts#"+"Button_Bunches#"+"Button_Nuts#"+"Last_2_Bunches";
		Log.i("log", column_Name);
		arrlstNameValuePairs.add(new BasicNameValuePair("columnname",column_Name));
		cursor= db.rawQuery("select * from  Farm_Data where UploadFlag='N'", null);
		int count=cursor.getCount();
		while(cursor.moveToNext())
		{
			col_1= cursor.getString(cursor.getColumnIndex("TreeName"));

			col_2= cursor.getString(cursor.getColumnIndex("CurrentDate"));
			col_3= cursor.getString(cursor.getColumnIndex("FarmCode"));
			col_4= cursor.getString(cursor.getColumnIndex("FSPId"));
			col_5= cursor.getString(cursor.getColumnIndex("MaturedBunches"));
			col_6= cursor.getString(cursor.getColumnIndex("MaturedNuts"));
			col_7= cursor.getString(cursor.getColumnIndex("ButtonBunches"));
			col_8= cursor.getString(cursor.getColumnIndex("ButtonNuts"));
			col_9= cursor.getString(cursor.getColumnIndex("LastTwoBunches"));

			if(col_4.equalsIgnoreCase(""))
			{
				col_4="0";
			}

			column_data = col_1+"#"+col_2+"#"+col_3+"#"+col_4+"#"+col_5+"#"+col_6+"#"+col_7+"#"+col_8+"#"+col_9+"|";
			Log.i("log", column_data);
			arrlstNameValuePairs.add(new BasicNameValuePair("columndata",column_data));
		}
		if(count==0)
		{
			cursor.close();
			db.close();
			return 0;
		}
		cursor.close();
		db.close();
		UploadToServer uploadToServer = new UploadToServer();
		int code=uploadToServer.uploadToServer(arrlstNameValuePairs,url);
		if(code==1)
		{
			setFlagYforFormFarm_DataTable();
		}
		else
		{
			count=code;
		}
		return count;
	}
	public int sendDataFromTrnFSPLocation(String url)
	{
		ArrayList<NameValuePair> arrlstNameValuePairs = new ArrayList<NameValuePair>();
		String col_1 = "";
		String col_2 = "";
		String col_3 = "";
		String col_4 = "";
		String col_5 = "";
		String col_6 = "";
		String col_7 = "";
		db=this.getWritableDatabase();
		Cursor cursor = null;
		String column_data= null;
		String column_Name =  "FarmerId#"+"Lattitude#"+"Longitude#"+"FspId#"+"Accuracy#"+"Provider#"+"TrnDate";
		Log.i("log", column_Name);
		arrlstNameValuePairs.add(new BasicNameValuePair("columnname",column_Name));
		cursor= db.rawQuery("select * from  TrnFSPLocation where UploadFlag='N'", null);
		int count=cursor.getCount();
		while(cursor.moveToNext())
		{
			col_1= cursor.getString(cursor.getColumnIndex("FarmerId"));

			col_2= cursor.getString(cursor.getColumnIndex("Lattitude"));
			col_3= cursor.getString(cursor.getColumnIndex("Longitude"));
			col_4= cursor.getString(cursor.getColumnIndex("FspId"));
			col_5= cursor.getString(cursor.getColumnIndex("Accuracy"));

			col_6= cursor.getString(cursor.getColumnIndex("Provider"));

			col_7= cursor.getString(cursor.getColumnIndex("TrnDate"));

			if(col_4.equalsIgnoreCase(""))
			{
				col_4="0";
			}
			column_data = col_1+"#"+col_2+"#"+col_3+"#"+col_4+"#"+col_5+"#"+col_6+"#"+col_7+"|";
			Log.i("log", column_data);
			arrlstNameValuePairs.add(new BasicNameValuePair("columndata",column_data));
		}
		if(count==0)
		{
			cursor.close();
			db.close();
			return 0;
		}
		cursor.close();
		db.close();
		UploadToServer uploadToServer = new UploadToServer();
		int code=uploadToServer.uploadToServer(arrlstNameValuePairs,url);
		if(code==1)
		{
			setFlagYForTrnFSPLocation();
		}
		else
		{
			count=code;
		}
		return count;
	}
	public void setFlagYForTrnFSPLocation()
	{
		db=this.getWritableDatabase();
		ContentValues cv= new ContentValues();
		cv.put("UploadFlag", "Y");
		db.update("TrnFSPLocation", cv, "UploadFlag='N'", null); 	
		db.close();
	}
	public void setFlagYforFormFarm_DataTable()
	{
		db=this.getWritableDatabase();
		ContentValues cv= new ContentValues();
		cv.put("UploadFlag", "Y");
		db.update("Farm_Data", cv, "UploadFlag='N'", null); 	
		db.close();
	}
	public void setFlagYforFormTrnAvg_Data()
	{
		db=this.getWritableDatabase();
		ContentValues cv= new ContentValues();
		cv.put("UploadFlag", "Y");
		db.update("TrnAvg_Data", cv, "UploadFlag='N'", null); 	
		db.close();
	}
	public String getUserName() {
		db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery("Select * from MstUser", null);
		cursor.moveToFirst();
		String uid = "";
		int count = cursor.getCount();
		if(count!=0)
		{
			while(!cursor.isAfterLast())
			{
				uid = cursor.getString(cursor.getColumnIndex("User_ID"));
				cursor.moveToNext();
			}
		}
		else {
			uid = "NA";
		}
		cursor.close();
		db.close();
		return uid;
	}

	public int checkForClosing() 
	{
		int closing = 0;
		String closeString = "";
		db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery("Select Closing_KM from TrnKilometer where UploadFlag = 'N'", null);
		cursor.moveToFirst();
		int count = cursor.getCount();
		if(count>0)
		{
			closeString = cursor.getString(cursor.getColumnIndex("Closing_KM"));

			if(!closeString.equals("") || !closeString.equals("0"))
			{
				closing = Integer.parseInt(closeString);
			}
			else
			{
				closing = 0;
			}
		}
		cursor.close();
		db.close();
		return closing;
	}
	public int checkForOilPrice() 
	{
		int cnoPrice = 0;
		int palmPrice = 0;
		db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery("Select CNORate,PalmRate from TrnOilPrice where UploadFlag = 'N'", null);
		cursor.moveToFirst();
		int count = cursor.getCount();
		if(count>0)
		{
			cnoPrice = cursor.getInt(cursor.getColumnIndex("CNORate"));
			palmPrice = cursor.getInt(cursor.getColumnIndex("PalmRate"));						
		}
		cursor.close();
		db.close();
		if(cnoPrice>0 || palmPrice >0)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	
	
	public int checkForNutsPrice() 
	{
		double copraprice = 0, coconutPrice = 0;
		//String copra_price = "", coconut_Price = "";
		db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery("SELECT Copra_price, Coconut_Price FROM TrnNutsPrice where UploadFlag = 'N' ", null);
		cursor.moveToFirst();
		int count = cursor.getCount();
		if(count>0)
		{
			//copra_price = cursor.getString(cursor.getColumnIndex("Copra_price"));
			//coconut_Price = cursor.getString(cursor.getColumnIndex("Coconut_Price"));
			
			
			copraprice = cursor.getDouble(cursor.getColumnIndex("Copra_price"));
			coconutPrice = cursor.getDouble(cursor.getColumnIndex("Coconut_Price"));
			
			/*if(!copra_price.equals("") || !copra_price.equals("0") || !copra_price.equals("0.0"))
			{
				//closing = Integer.parseInt(closeString);
				//copraprice 
			}
			*/
		}
		cursor.close();
		db.close();
		
		if(copraprice>0 || coconutPrice >0)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	
	public int sendDataFromTrnKilometer(String url) 
	{
		ArrayList<NameValuePair> arrlstNameValuePairs = new ArrayList<NameValuePair>();
		String col_1 = "";
		String col_2 = "";
		String col_3 = "";
		String col_4 = "";
		String col_5 = "";
		String col_6 = "";
		String col_7 = "";
		String col_8 = "";
		db=this.getWritableDatabase();
		Cursor cursor = null;
		String column_data= null;
		String column_Name =  "Entry_Date#"+"FSP_ID#"+"Route_Code#"+"Opening_KM#"+"Closing_KM#"+"Total_KM#"+"Reason#"+"Daily_Allowance";
		Log.i("log", column_Name);
		arrlstNameValuePairs.add(new BasicNameValuePair("columnname",column_Name));
		cursor= db.rawQuery("select * from  TrnKilometer where UploadFlag='N'", null);
		int count=cursor.getCount();
		while(cursor.moveToNext())
		{
			col_1= cursor.getString(cursor.getColumnIndex("Entry_Date"));

			col_2= cursor.getString(cursor.getColumnIndex("FSP_ID"));
			col_3= cursor.getString(cursor.getColumnIndex("Route_Code"));
			col_4= cursor.getString(cursor.getColumnIndex("Opening_KM"));
			col_5= cursor.getString(cursor.getColumnIndex("Closing_KM"));

			col_6= cursor.getString(cursor.getColumnIndex("Total_KM"));

			col_7= cursor.getString(cursor.getColumnIndex("Reason"));
			col_8= cursor.getString(cursor.getColumnIndex("Daily_Allowance"));

			column_data = col_1+"#"+col_2+"#"+col_3+"#"+col_4+"#"+col_5+"#"+col_6+"#"+col_7+"#"+col_8+"|";
			Log.i("log", column_data);
			arrlstNameValuePairs.add(new BasicNameValuePair("columndata",column_data));
		}
		if(count==0)
		{
			cursor.close();
			db.close();
			return 0;
		}
		cursor.close();
		db.close();
		UploadToServer uploadToServer = new UploadToServer();
		int code=uploadToServer.uploadToServer(arrlstNameValuePairs,url);
		if(code==1)
		{
			setFlagYForTrnKilometer();
		}
		else
		{
			count=code;
		}
		return count;
	}

	public int sendDataFromTrnNutsPrice(String url) 
	{
		ArrayList<NameValuePair> arrlstNameValuePairs = new ArrayList<NameValuePair>();
		String col_1 = "";
		String col_2 = "";
		String col_3 = "";
		String col_4 = "";
		db=this.getWritableDatabase();
		Cursor cursor = null;
		String column_data= null;
		String column_Name =  "Entry_Date#"+"Copra_price#"+"Coconut_Price#"+"Loc_Code";
		Log.i("log", column_Name);
		arrlstNameValuePairs.add(new BasicNameValuePair("columnname",column_Name));
		cursor= db.rawQuery("select * from  TrnNutsPrice where UploadFlag='N'", null);
		int count=cursor.getCount();
		while(cursor.moveToNext())
		{
			col_1= cursor.getString(cursor.getColumnIndex("Entry_Date"));
			col_2= cursor.getString(cursor.getColumnIndex("Copra_price"));
			col_3= cursor.getString(cursor.getColumnIndex("Coconut_Price"));
			col_4= cursor.getString(cursor.getColumnIndex("Loc_Code"));
			
			column_data = col_1+"#"+col_2+"#"+col_3+"#"+col_4+"|";
			Log.i("Log", column_data);
			arrlstNameValuePairs.add(new BasicNameValuePair("columndata",column_data));
		}
		if(count==0)
		{
			cursor.close();
			db.close();
			return 0;
		}
		cursor.close();
		db.close();
		UploadToServer uploadToServer = new UploadToServer();
		int code=uploadToServer.uploadToServer(arrlstNameValuePairs,url);
		if(code==1)
		{
			setFlagYForTrnNutsPrice();
		}
		else
		{
			count=code;
		}
		return count;
	}

	private void setFlagYForTrnKilometer() 
	{
		db=this.getWritableDatabase();
		ContentValues cv= new ContentValues();
		cv.put("UploadFlag", "Y");
		db.update("TrnKilometer", cv, "UploadFlag='N'", null); 	
		db.close();
	}
	private void setFlagYForTrnNutsPrice() 
	{
		db=this.getWritableDatabase();
		ContentValues cv= new ContentValues();
		cv.put("UploadFlag", "Y");
		db.update("TrnNutsPrice", cv, "UploadFlag='N'", null); 	
		db.close();
	}

	public int sendDataFromTrnOilPrice(String url) {
		ArrayList<NameValuePair> arrlstNameValuePairs = new ArrayList<NameValuePair>();
		String col_2 = "";
		String col_3 = "";
		String col_4 = "";
		String col_5 = "";
		String col_6 = "";
		db=this.getWritableDatabase();
		Cursor cursor = null;
		String column_data= null;
		String column_Name =  "FSPID#"+"OilPriceDate#"+"CNORate#"+"PalmRate#"+"CreatedBy";
		Log.i("Log", column_Name);
		arrlstNameValuePairs.add(new BasicNameValuePair("columnname",column_Name));
		cursor= db.rawQuery("select * from  TrnOilPrice where UploadFlag='N'", null);
		int count=cursor.getCount();
		while(cursor.moveToNext())
		{
			//col_1= cursor.getString(cursor.getColumnIndex("OilPriceID"));

			col_2= cursor.getString(cursor.getColumnIndex("FSPID"));
			col_3= cursor.getString(cursor.getColumnIndex("OilPriceDate"));
			col_4= cursor.getString(cursor.getColumnIndex("CNORate"));
			col_5= cursor.getString(cursor.getColumnIndex("PalmRate"));
			col_6= cursor.getString(cursor.getColumnIndex("CreatedBy"));
			column_data = col_2+"#"+col_3+"#"+col_4+"#"+col_5+"#"+col_6+"|";
			Log.i("Log", column_data);
			arrlstNameValuePairs.add(new BasicNameValuePair("columndata",column_data));
		}
		if(count==0)
		{
			cursor.close();
			db.close();
			return 0;
		}
		cursor.close();
		db.close();
		UploadToServer uploadToServer = new UploadToServer();
		int code=uploadToServer.uploadToServer(arrlstNameValuePairs,url);
		if(code==1)
		{
			setFlagYForTrnOilPrice();
		}
		else
		{
			count=code;
		}
		return count;
	}

	private void setFlagYForTrnOilPrice() 
	{
		db=this.getWritableDatabase();
		ContentValues cv= new ContentValues();
		cv.put("UploadFlag", "Y");
		db.update("TrnOilPrice", cv, "UploadFlag='N'", null); 	
		db.close();
	}
}