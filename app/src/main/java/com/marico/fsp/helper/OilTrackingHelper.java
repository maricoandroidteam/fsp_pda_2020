package com.marico.fsp.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

public class OilTrackingHelper extends DatabaseHelper{

	public OilTrackingHelper(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	} 

	public boolean chechforTrnOils() 
	{
		db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery("Select * from TrnOilPrice", null);
		cursor.moveToFirst();
		int count = cursor.getCount();
		cursor.close();
		db.close();

		if(count>0)
		{
			return true;
		}
		return false;

	}
	public String[] getTrnOils_Data() 
	{
		// TODO Auto-generated method stub
		db = this.getReadableDatabase();
		Cursor cursor = null;		
		String coconut = "0",palm= "0";

		cursor= db.rawQuery("Select * from TrnOilPrice",null);
		while(cursor.moveToNext())
		{
			coconut = cursor.getString(cursor.getColumnIndex("CNORate"));
			palm = cursor.getString(cursor.getColumnIndex("PalmRate"));			
		}
		cursor.close();
		db.close();
		return new String[]{coconut,palm};
	}
	private String getUserName() {
		// TODO Auto-generated method stub
		db = this.getReadableDatabase();
		String uName = "";
		Cursor cursor = null;
		cursor = db.rawQuery("Select User_ID from mstUser", null);
		cursor.moveToFirst();
		while(!cursor.isAfterLast())
		{
			uName = cursor.getString(cursor.getColumnIndex("User_ID"));
			cursor.moveToNext();
		}
		cursor.close();
		db.close();
		return uName;
	}
	
	public void saveInTrnOilPrice(int cnoRate, int palmRate, boolean hasData) 
	{
		String fspId = getUserName();
		db = this.getWritableDatabase();
		ContentValues cv = new ContentValues();
		//cv.put(key, value)
		cv.put("FSPId", fspId);
		cv.put("CNORate", String.valueOf(cnoRate));
		cv.put("PalmRate", String.valueOf(palmRate));
		cv.put("CreatedBy",fspId);
		if(hasData)
		{
			//db.update("TrnKilometer", cv, "FSP_ID = ? ", new String[]{fspId});
			db.execSQL("Delete from TrnOilPrice");
		}
			db.insert("TrnOilPrice", String.valueOf(cnoRate), cv);	
		db.close();
	}
	
}
